/**
 * This packaged is about manipulating single/double-linked lists and using them to show certain properties
 * or achieve certain goals.
 */
/**
 * @author YValeri
 *
 */
package LinkedList;