package LinkedList;

import java.util.ArrayList;

/*
 * Given k sorted singly linked lists, write a function to merge all the lists into one sorted singly linked list.
 */

public class KListMerger {
	public static void main(String[] args) {
		ArrayList<Integer> l1 = new ArrayList<>();
		l1.add(1);
		l1.add(4);
		l1.add(6);
		l1.add(12);
		ArrayList<Integer> l2 = new ArrayList<>();
		l2.add(2);
		l2.add(7);
		l2.add(9);
		l2.add(11);
		ArrayList<Integer> l3 = new ArrayList<>();
		l3.add(4);
		l3.add(8);
		l3.add(15);
		l3.add(22);
		ArrayList<Integer> l4 = new ArrayList<>();
		l4.add(9);
		l4.add(10);
		l4.add(18);
		l4.add(27);
		ArrayList<Integer> l5 = new ArrayList<>();
		l5.add(1);
		l5.add(2);
		l5.add(3);
		l5.add(4);
		
		System.out.println(merger(l1, l2, l3, l4, l5));
	}
	
	@SafeVarargs
	public static ArrayList<Integer> merger(ArrayList<Integer> ...lists){
		int total = 0;
		for(int i = 0; i < lists.length; ++i)
			total += lists[i].size();
		int[] indexes = new int[lists.length];
		ArrayList<Integer> res = new ArrayList<>();
		for(int i = 0; i < total; ++i) {
			int min = Integer.MAX_VALUE;
			int index = 0;
			for(int j = 0; j < indexes.length; ++j) {
				if(indexes[j] < lists[j].size() && lists[j].get(indexes[j]) < min) {
					min = lists[j].get(indexes[j]);
					index = j;
				}
			}
			indexes[index]++;
			res.add(min);
		}
		return res;
	}
}
