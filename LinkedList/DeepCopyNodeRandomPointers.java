package LinkedList;

import java.util.HashMap;

public class DeepCopyNodeRandomPointers {
	public static void main(String[] args) {
		Node head = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(5);
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		head.rnd = n3;
		n2.rnd = n4;
		n3.rnd = head;
		n4.rnd = n2;
		n5.rnd = n5;

		System.out.println("Original :");
		for(Node h = deepCopy(head); h != null; h = h.next) 
			System.out.println(h.iValue+", "+h.rnd.iValue);
		
		System.out.println("\nCopy : ");
		
		for(Node h = head; h != null; h = h.next)
			System.out.println(h.iValue+", "+h.rnd.iValue);
	}
	
	public static Node deepCopy(Node head) {
		Node nh = new Node();
		Node keeper = nh;
		HashMap<Node, Node> map = new HashMap<>();
		for(Node n = head; n != null; n = n.next) {
			map.put(n, nh);
			nh.iValue = n.iValue;
			if(n.next != null) {
				nh.next = new Node();
				nh = nh.next;
			}
		}
		nh = keeper;
		for(Node n = head; n != null; n = n.next, nh = nh.next) {
			nh.rnd = map.get(n.rnd);
		}
		return keeper;
	}
}
