package LinkedList;

import java.util.HashMap;

/*Given two singly linked lists that intersect at some point, find the intersecting node. The lists are non-cyclical.

For example, given A = 3 -> 7 -> 8 -> 10 and B = 99 -> 1 -> 8 -> 10, return the node with value 8.

In this example, assume nodes with the same value are the exact same node objects.

Do this in O(M + N) time (where M and N are the lengths of the lists) and constant space.
 */

public class ListIntersection {
	static HashMap<Integer, Node> m = new HashMap<>();
	
	public static void main(String[] args) {
		Node intersect = new Node(8, null);
		Node A = new Node(0, new Node(7, intersect));
		Node C = new Node(12, new Node(4, A));
		Node B = new Node(99, new Node(7, intersect));
		C.printList();
		B.printList();
		
		long lStartTime = System.nanoTime();
		addMap(C);
		int i = intersect(B).iValue;
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(i);
	}
	
	static void addMap(Node n) {
		while(n != null) {
			m.put(n.iValue, n);
			n = n.next;
		}
	}
	
	static Node intersect(Node n) {
		while(n != null) {
			Node result = m.get(n.iValue);
			if(result != null)
				return result;
			n = n.next;
		}
		return  null;
	}
}
