package LinkedList;

/*
 * Given the head of a singly linked list, reverse it in-place.
 */

public class LinkedListReversal {
	public static void main(String[] args) {
		Node head = new Node(0, null);
		for(int i = 1; i < 10; ++i)
			head.append(i);
		System.out.println(head);
		System.out.println(reversal(head));
	}
	
	public static Node reversal(Node head){
		if(head.next != null) {
			Node n = reversal(head.next);
			Node newHead = n;
			while(newHead.next != null)
				newHead = newHead.next;
			newHead.next = head;
			head.next = null;
			return n;
		}else {
			return head;
		}
	}
}
