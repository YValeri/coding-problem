package LinkedList;

/*
 * Given the head of a singly linked list, swap every two nodes and return its head.
 * 
 * For example, given 1 -> 2 -> 3 -> 4, return 2 -> 1 -> 4 -> 3.
 */

public class SwappingNodes {
	public static void main(String[] args) {
		Node head = new Node(1);
		head.next = new Node(2);
		head.next.next = new Node(3);
		head.next.next.next = new Node(4);
		head.next.next.next.next = new Node(5);
		head.printList();
		System.out.println();
		head = swap(head);
		head.printList();
		
	}

	public static Node swap(Node head) {
		//If head or head.next is null, we return head because there is nothing to swap
		if (head == null || head.next == null)
			return head;
		
		//temp is used to keep the node that will become head of the current sublist
		Node temp = head.next;
		
		//Change the next of head to the head of the sublist starting from the next of the next of the current head
		head.next = swap(head.next.next);
		
		//We swap the head and temp, so the latter's next is the old head
		temp.next = head;
		
		//Return the new head of the current sublist
		return temp;
	}
}
