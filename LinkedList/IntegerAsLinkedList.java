package LinkedList;

/*
 * Let's represent an integer in a linked list format by having each node represent a digit in the number. 
 * The nodes make up the number in reversed order.
 * 
 * For example, the following linked list:

1 -> 2 -> 3 -> 4 -> 5

 * is the number 54321.
 * 
 * Given two linked lists in this format, return their sum in the same linked list format.
 * 
 * For example, given

9 -> 9

5 -> 2

 * return 124 (99 + 25) as:

4 -> 2 -> 1

 */

public class IntegerAsLinkedList {
	public static void main(String[] args) {
		Node n1 = new Node(9);
		n1.next = new Node(9);

		Node n2 = new Node(5);
		n2.next = new Node(2);
		
		Node n = add(n1, n2, false);
		while(n != null) {
			System.out.print(n.iValue+", ");
			n = n.next;
		}
	}

	public static Node add(Node n1, Node n2, boolean carry) {
		if(n1 == null && n2 == null)
			if(carry) 
				return new Node(1);
			else return null;
		int t = ((n1 == null)?0:n1.iValue) + ((n2 == null)?0:n2.iValue) + ((carry)?1:0);
		Node n = new Node(t%10);
		n.next = add((n1 == null)?n1:n1.next, (n2 == null)?n2:n2.next, t >= 10);
		return n;
	}
}
