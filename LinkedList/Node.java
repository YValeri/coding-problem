package LinkedList;

/*
 * See "SwappingNodes" and "LinkedListReversal" problems
 */

public class Node {
	String sValue;
	Integer iValue;
	Node next;
	Node rnd;
	int count = 0;
	
	Node(){
		this.next = null;
		this.iValue = null;
		this.rnd= null;
		this.sValue = "";
	}
	
	Node(int val){
		this.next = null;
		this.iValue = val;
		this.rnd= null;
		this.sValue = "";
	}
	
	Node(String s){
		this.next = null;
		this.iValue = 0;
		this.rnd= null;
		this.sValue = s;
	}
	
	Node(int val, Node next){
		this.next = next;
		this.iValue = val;
		this.rnd= null;
		this.sValue = "";
	}
	
	public void append(int i) {
		if(next != null)
			next.append(i);
		else
			next = new Node(i, null);
	}
	
	public void append(Node n) {
		if(next != null)
			next.append(n);
		else
			next = n;
	}
	
	public String toString() {
		if(next != null)
			return String.valueOf(iValue)+ ", " + next.toString();
		else
			return String.valueOf(iValue);
	}
	
	void printList() {
		System.out.print(this.iValue);
		if(this.next != null) {
			System.out.print(" -> ");
			this.next.printList();
		}else System.out.println();
	}
}













