package LinkedList;

import java.util.LinkedList;

/*
 * Determine whether a doubly linked list is a palindrome.
 * What if it’s singly linked?
 * 
 * For example, 1 -> 4 -> 3 -> 4 -> 1 returns true while 1 -> 4 returns false.
 */

public class ListPalindrome {
	public static void main(String[] args) {
		LinkedList<Integer> l = new LinkedList<>();
		l.addFirst(1);
		l.addFirst(4);
		l.addFirst(3);
		l.addFirst(4);
		l.addFirst(1);
		
		LinkedList<Integer> l2 = new LinkedList<>();
		l2.addFirst(1);
		l2.addFirst(4);
		l2.addFirst(4);
		l2.addFirst(1);
		

		LinkedList<Integer> l3 = new LinkedList<>();
		l3.addFirst(1);
		l3.addFirst(4);
		

		LinkedList<Integer> l4 = new LinkedList<>();
		l4.addFirst(1);
		l4.addFirst(4);
		l4.addFirst(5);
		
		System.out.println(check(l));
		System.out.println(check(l2));
		System.out.println(check(l3));
		System.out.println(check(l4));
	}
	
	public static boolean check(LinkedList<Integer> l) {
		int[] arr = new int[l.size()/2];
		int pos = 0, length = l.size();
		for(Integer i : l) {
			if(pos < length/2)
				arr[length/2 - pos - 1] = i;
			else if(!(length%2 == 1 && pos == length/2))
				if(i != arr[pos - length/2 - (length%2)])
					return false;
			
			pos++;
		}
		return true;
	}
}
