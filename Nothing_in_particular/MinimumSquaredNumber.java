package Nothing_in_particular;

/*
 * Given a positive integer n, find the smallest number of squared integers which sum to n.
 * 
 * For example, given n = 13, return 2 since 13 = 3^2 + 2^2 = 9 + 4.
 * 
 * Given n = 27, return 3 since 27 = 3^2 + 3^2 + 3^2 = 9 + 9 + 9.
 */

public class MinimumSquaredNumber {
	public static void main(String[] args) {
		int n = 33;
		System.out.println(square(n));
	}

	//We do all the integers which squared are less than n, and recall the method each time
	public static int square(int n) {
		//If n <= 3, return n because you have to do n * 1 to get n, i.e. if n = 3, we add 3 because it is 1^2 + 1^2 + 1^2
		if (n <= 3)
			return n;

		int min = n;
		
		//For all integers which squared are less than n
		for (int i = 1; i * i <= n; ++i) 
			//min = the minimum what we already found and the result of square(n - i^2) + 1
			min = Math.min(min, square(n - i * i) + 1);
		
		return min;

	}
}
