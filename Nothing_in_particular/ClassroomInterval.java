package Nothing_in_particular;

import java.util.ArrayList;

/*
 * Given an array of time intervals (start, end) for classroom lectures (possibly overlapping), find the minimum number of rooms required.
 * For example, given [(30, 75), (0, 50), (60, 150)], you should return 2.
 */

public class ClassroomInterval {
	public static void main(String[] args) {
		ArrayList<Pair<Integer, Integer>> a = new ArrayList<Pair<Integer, Integer>>();
		a.add(new Pair<Integer, Integer>(30, 75));
		a.add(new Pair<Integer, Integer>(0, 50));
		a.add(new Pair<Integer, Integer>(60, 150));
		a.add(new Pair<Integer, Integer>(40, 70));
		a.add(new Pair<Integer, Integer>(80, 90));
		
		long lStartTime = System.nanoTime();
		int i = minimumRooms(a);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(i);
	}
	
	static int minimumRooms(ArrayList<Pair<Integer, Integer>> a) {
		ArrayList<Pair<Integer, Integer>> b = new ArrayList<Pair<Integer, Integer>>();
		int cpt = 0;
		while(a.size() != 0) {
			Pair<Integer, Integer> p = getMin(a);
			do{
				b.add(p);
				p = nextList(a, p.second);
			}while(p != null);
			a.removeAll(b);
			cpt++;
		}
		return cpt;
	}
	
	static Pair<Integer, Integer> getMin(ArrayList<Pair<Integer, Integer>> a){
		int min = 999999;
		Pair<Integer, Integer> res = null;
		for(Pair<Integer, Integer> p : a) {
			if(p.first <= min) {
				min = p.first;
				res = p;
			}
		}
		return res;
	}
	
	static Pair<Integer, Integer> nextList(ArrayList<Pair<Integer, Integer>> a, int min) {
		int max = 99999;
		Pair<Integer, Integer> res = null;
		for(Pair<Integer, Integer> p : a) {
			if(p.first >= min && p.first <= max) {
				max = p.first;
				res = p;
			}
		}
		return res;
	}
}
