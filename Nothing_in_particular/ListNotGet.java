package Nothing_in_particular;

import java.util.ArrayList;
import java.util.Random;

/*
 * Given an integer n and a list of integers l, 
 * write a function that randomly generates a 
 * number from 0 to n-1 that isn't in l (uniform).
 */

public class ListNotGet {
	public static void main(String[] args) {
		ArrayList<Integer> l = new ArrayList<>();
		for(int i = 0; i < 60; ++i) {
			l.add(i*2);
			l.add(i*3);
		}
		System.out.println(get(l, 60));
	}
	
	public static int get(ArrayList<Integer> l, int n) {
		int[] tab = new int[n];
		for (int i = 0; i < tab.length; i++) 
			tab[i] = i;
		
		for(Integer i : l)
			if(i < n)
				tab[i] = -1;
		
		ArrayList<Integer> missing = new ArrayList<>();
		for (int i = 0; i < tab.length; i++) 
			if(tab[i] != -1)
				missing.add(i);
		
		if(missing.size() == l.size())
			return -1;
		
		Random r = new Random(System.nanoTime());
		int index = r.nextInt(missing.size());
		return missing.get(index);
	}
}
