package Nothing_in_particular;

import java.util.ArrayList;

/*
 * Gray code is a binary code where each successive value differ in only one bit, 
 * as well as when wrapping around. 
 * Gray code is common in hardware so that we don't see temporary spurious values during transitions.
 * 
 * Given a number of bits n, generate a possible gray code for it.
 * 
 * For example, for n = 2, one gray code would be [00, 01, 11, 10].
 */

public class GrayCodeGenerator {
	public static void main(String[] args) {
		int n = 4;
		ArrayList<String> l = generator(n);
		System.out.println(l);
	}
	
	public static ArrayList<String> generator(int n){
		//Basic case, if the we want to generate the 2-bit gray code, it is the following list : "00 01 11 10"
		if(n == 2) {
			//So we create that list, fill it, and return it
			ArrayList<String> res = new ArrayList<>();
			res.add("00");
			res.add("01");
			res.add("11");
			res.add("10");
			return res;
		}else {
			//temp is the list of (n-1)-bit gray code
			ArrayList<String> temp = generator(n-1);
			ArrayList<String> res = new ArrayList<>();
			
			//For all (n-1)-bit gray codes, we take that code, add "0" before it, and add that to the result list
			for(int i = 0; i < temp.size(); ++i)
				res.add("0"+temp.get(i));
			
			//We do the same, but we traverse the list in reverse order, and add "1" before each code
			for(int i = temp.size()-1; i >= 0; --i)
				res.add("1"+temp.get(i));
			
			//Return the n-bit gray code list
			return res;
		}
	}
}
