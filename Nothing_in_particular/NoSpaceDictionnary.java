package Nothing_in_particular;

import java.util.HashMap;
import java.util.Map;

/*
 * Given a dictionary of words and a string made up of those words (no spaces), return the original sentence in a list. 
 * If there is more than one possible reconstruction, return any of them. 
 * If there is no possible reconstruction, then return null.
 * 
 * For example, given the set of words 'quick', 'brown', 'the', 'fox', and the string "thequickbrownfox", 
 * you should return ['the', 'quick', 'brown', 'fox'].
 * 
 * Given the set of words 'bed', 'bath', 'bedbath', 'and', 'beyond', and the string "bedbathandbeyond", 
 * return either ['bed', 'bath', 'and', 'beyond] or ['bedbath', 'and', 'beyond'].
 */

public class NoSpaceDictionnary {
	static Map<String, String> dictionary = new HashMap<String, String>();
	
	public static void main(String[] args) {
		dictionary.put("quick", "quick");
		dictionary.put("brown", "brown");
		dictionary.put("the", "the");
		dictionary.put("fox", "fox");
		dictionary.put("bed", "bed");
		dictionary.put("bath", "bath");
		dictionary.put("bedbath", "bedbath");
		dictionary.put("and", "and");
		dictionary.put("beyond", "beyond");
		String s = "bedbathandbeyond";
		
		long lStartTime = System.nanoTime();
		String result = originalSentence(s);
	    long lEndTime = System.nanoTime();
	    long output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(result);
	}
	
	static String originalSentence(String s) {
		String result = "", inter = "";
		for(int i = 0; i < s.length(); i++){
			inter += s.charAt(i);
			if(dictionary.get(inter) != null) {
				result += inter + " ";
				inter = "";
			}
		}
		return (inter.length() != 0)?null:result;
	}
}
