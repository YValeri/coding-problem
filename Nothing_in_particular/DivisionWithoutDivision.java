package Nothing_in_particular;

/*
 * Implement division of two positive integers without using the division, 
 * multiplication, or modulus operators. 
 * Return the quotient as an integer, ignoring the remainder.
 */

public class DivisionWithoutDivision {
	public static void main(String[] args) {
		System.out.println(divide(8, 4));
		System.out.println(divide(7, 5));
		System.out.println(divide(12, 2));
		System.out.println(divide(844, 9));
		System.out.println(divide(94257, 5));
	}
	
	public static int divide(int b, int a) {
		if(a > b)
			return 0;
		int cpt = 0;
		while(b >= a) {
			b -= a;
			cpt++;
		}
		return cpt;
	}
}
