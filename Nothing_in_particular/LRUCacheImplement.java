package Nothing_in_particular;

/*
 * Implement an LRU (Least Recently Used) cache. 
 * It should be able to be initialized with a cache size n, and contain the following methods:
 * 
 * set(key, value): sets key to value. 
 * If there are already n items in the cache and we are adding a new item, then it should also remove the least recently used item.
 * 
 * get(key): gets the value at key. If no such key exists, return null.
 * 
 * Each operation should run in O(1) time.
 */

public class LRUCacheImplement<K, V> {
	public static void main(String[] args) {
		int n = 5;
		LRUCache<String, Integer> cache = new LRUCache<>(n);
		long lStartTime, lEndTime, output;
		
		for(int i = 0; i < (n*2 + 3); ++i) {
			lStartTime = System.nanoTime();
			cache.set(String.valueOf(i), i+1);
	        lEndTime = System.nanoTime();
	        output = lEndTime - lStartTime;
	        System.out.println("Elapsed time in milliseconds: " + output);
	        cache.print();
		}
        
		lStartTime = System.nanoTime();
		int res = cache.get("8");
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("\n"+res);
        cache.print();
        
        System.out.println();
        
        lStartTime = System.nanoTime();
		cache.set(String.valueOf(88), 88);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        cache.print();
	}
}
