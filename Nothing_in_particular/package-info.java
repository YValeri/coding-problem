/**
 * This package isn't about a single data structure in particular.
 * The algorithms here use multiple structures or not any to achieve certain goals that
 * are not related to the structures in themselves.
 */
/**
 * @author YValeri
 *
 */
package Nothing_in_particular;