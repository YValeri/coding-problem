package Nothing_in_particular;

import java.util.ArrayList;

/*
 * Given a list of points, a central point, and an integer k, 
 * find the nearest k points from the central point.
 * 
 * For example, given the list of points [(0, 0), (5, 4), (3, 1)], 
 * the central point (1, 2), and k = 2, return [(0, 0), (3, 1)].
 */

public class KCentralPoints {
	public static void main(String[] args) {
		int k = 2;
		Pair<Integer, Integer> p = new Pair<Integer, Integer>(1, 2);
		ArrayList<Pair<Integer, Integer>> l = new ArrayList<>();
		l.add(new Pair<Integer, Integer>(-1, -1));
		l.add(new Pair<Integer, Integer>(3, 1));
		l.add(new Pair<Integer, Integer>(0, 0));
		l.add(new Pair<Integer, Integer>(5, 4));
		l.add(new Pair<Integer, Integer>(1, 1));
		
		System.out.println(central(l, k, p));
	}
	
	public static ArrayList<Pair<Integer, Integer>> central(ArrayList<Pair<Integer, Integer>> l, int k, Pair<Integer, Integer> p){
		//If we want a number of points larger than the ones we can pick from, just return the list
		if(k >= l.size())
			return l;
		
		ArrayList<Pair<Integer, Integer>> res = new ArrayList<>();
		
		//We do k rounds of search
		for(int i = 0; i < k; ++i) {
			
			//Get the index of the minimum distance between of point of l and the central point p
			int min = Integer.MAX_VALUE, index = 0;
			for(int j = 0; j < l.size(); ++j) {
				Pair<Integer, Integer> temp = l.get(j);
				
				int diff = Math.max(temp.first, p.first) - Math.min(temp.first, p.first) + 
								Math.max(temp.second, p.second) - Math.min(temp.second, p.second);
				if(diff < min) {
					min = diff;
					index = j;
				}
			}
			
			/*
			 * Since we didn't change the indexes and positions of points in the two lists,
			 * we just add the point at index index in l to the result list,
			 * and delete that point in the list l
			 */
			res.add(l.get(index));
			l.remove(index);
		}
		
		return res;
	}
}
