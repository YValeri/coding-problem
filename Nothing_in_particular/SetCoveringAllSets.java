package Nothing_in_particular;

import java.util.ArrayList;

/*
 * Given a set of closed intervals, find the smallest set of numbers that covers all the intervals. 
 * If there are multiple smallest sets, return any of them.
 * 
 * For example, given the intervals [0, 3], [2, 6], [3, 4], [6, 9], 
 * one set of numbers that covers all these intervals is {3, 6}.
 */

public class SetCoveringAllSets {
	public static void main(String[] args) {
		ArrayList<Pair<Integer, Integer>> intervals = new ArrayList<>();
		intervals.add(new Pair<Integer, Integer>(0, 3));
		intervals.add(new Pair<Integer, Integer>(2, 6));
		intervals.add(new Pair<Integer, Integer>(3, 4));
		intervals.add(new Pair<Integer, Integer>(6, 9));
		System.out.println(overlap(intervals));
	}

	public static ArrayList<Integer> overlap(ArrayList<Pair<Integer, Integer>> intervals) {
		ArrayList<Integer> res = new ArrayList<>();
		for(Pair<Integer, Integer> p : intervals) {
			boolean contained = false;
			for(Integer i : res) {
				if(p.first <= i && i <= p.second) {
					contained = true;
					break;
				}
			}
			if(contained) {
				ArrayList<Integer> temp = new ArrayList<>(res);
				for(Integer i : res) 
					if(!(p.first <= i && i <= p.second)) 
						temp.remove(i);
				res = temp;
			}else {
				for(int i = p.first; i <= p.second; ++i)
					res.add(i);
			}
		}
		return verify(intervals, res);
	}

	public static ArrayList<Integer> verify(ArrayList<Pair<Integer, Integer>> intervals, ArrayList<Integer> res) {
		boolean[] inRes = new boolean[res.size()];
		for(Pair<Integer, Integer> p : intervals) {
			for(int i = 0; i < res.size(); ++i) {
				int temp = res.get(i);
				if(p.first <= temp && temp <= p.second) {
					inRes[i] = true;
					break;
				}
			}
		}
		ArrayList<Integer> finalres = new ArrayList<>();
		for(int i = 0; i < res.size(); ++i) 
			if(inRes[i])
				finalres.add(res.get(i));
		return finalres;
	}
}
