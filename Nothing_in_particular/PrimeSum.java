package Nothing_in_particular;

/*
 * Given an even number (greater than 2), return two prime numbers whose sum will be equal to the given number.
 * 
 * A solution will always exist. See Goldbach’s conjecture.
 * 
 * Example:

Input: 4
Output: 2 + 2 = 4

 * If there are more than one solution possible, return the lexicographically smaller solution.
 * 
 * If [a, b] is one solution with a <= b, and [c, d] is another solution with c <= d, then

[a, b] < [c, d]

 * If a < c OR a==c AND b < d.
 */

public class PrimeSum {
	public static void main(String[] args) {
		for(int i = 4; i <= 150; i += 2)
			System.out.println(i+" is equal to the sum of the two primes : "+sum(i));
	}

	public static boolean[] Eratosthenes(int n) {
		boolean[] isPrime = new boolean[n];
		for (int i = 2; i < n; ++i)
			isPrime[i] = true;
		for (int i = 2; i * i < n; ++i) {
			if (isPrime[i] == true) {
				for (int j = i * 2; j < n; j += i)
					isPrime[j] = false;
			}
		}
		return isPrime;
	}

	public static Pair<Integer, Integer> sum(int n) {
		boolean[] isPrime = Eratosthenes(n);
		for(int i = 2; i < n; ++i) 
			if(isPrime[i] && isPrime[n-i])
				return new Pair<Integer, Integer>(i, n-i);
		
		return null;
	}
}
