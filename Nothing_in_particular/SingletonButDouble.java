package Nothing_in_particular;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Implement the singleton pattern with a twist. First, instead of storing one instance, store two instances. 
 * And in every even call of getInstance(), return the first instance 
 * and in every odd call of getInstance(), return the second instance.
 */

public class SingletonButDouble {

	public static void main(String[] args) {
		SingletonButDouble singleton1 = SingletonButDouble.getInstance();
		SingletonButDouble singleton2 = SingletonButDouble.getInstance();
		SingletonButDouble singleton3 = SingletonButDouble.getInstance();
		SingletonButDouble singleton4 = SingletonButDouble.getInstance();

		singleton2.show();
		singleton3.show();
		singleton1.show();
		singleton4.show();

	}

	private static Map<Turn, SingletonButDouble> turns;

	private static Turn turn;
	private String s;

	public enum Turn {

		ZERO, ONE;

	}

	static {
		turns = new ConcurrentHashMap<>();
		turns.put(Turn.ZERO, new SingletonButDouble("0"));
		turns.put(Turn.ONE, new SingletonButDouble("1"));
	}

	private SingletonButDouble(String s) {
		this.s= s;
	}

	public static SingletonButDouble getInstance() {
		if (turn == Turn.ZERO) {
			turn = Turn.ONE;
			return turns.get(Turn.ZERO);
		} else {
			turn = Turn.ZERO;
			return turns.get(Turn.ONE);

		}
	}

	public void show() {

		System.out.println("My number is " + s);

	}
}
