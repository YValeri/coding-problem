package Nothing_in_particular;

import java.util.ArrayList;

/*
 * Given a list of possibly overlapping intervals, 
 * return a new list of intervals where all overlapping intervals have been merged.
 * 
 * The input list is not necessarily ordered in any way.
 * 
 * For example, given [(1, 3), (5, 8), (4, 10), (20, 25)], 
 * you should return [(1, 3), (4, 10), (20, 25)].
 */

public class OverlappingIntervals {
	public static void main(String[] args) {
		ArrayList<Pair<Integer, Integer>> l = new ArrayList<>();
		l.add(new Pair<Integer, Integer>(1, 3));
		l.add(new Pair<Integer, Integer>(5, 8));
		l.add(new Pair<Integer, Integer>(4, 10));
		l.add(new Pair<Integer, Integer>(20, 25));
		l.add(new Pair<Integer, Integer>(7, 12));
		l.add(new Pair<Integer, Integer>(11, 15));
		l.add(new Pair<Integer, Integer>(-7, 0));
		l.add(new Pair<Integer, Integer>(11, 22));
		l.add(new Pair<Integer, Integer>(11, 22));
		l.add(new Pair<Integer, Integer>(4, 25));
		
		ArrayList<Pair<Integer, Integer>> res = overlap(l);
		for(Pair<Integer, Integer> p : res) {
			System.out.println(p.first+", "+p.second);
		}
	}
	
	public static ArrayList<Pair<Integer, Integer>> overlap(ArrayList<Pair<Integer, Integer>> list){
		ArrayList<Pair<Integer, Integer>> res = new ArrayList<>();
		ArrayList<Pair<Integer, Integer>> toRemove = new ArrayList<>();
		for(int i = 0; i < list.size()-1;) {
			int min = list.get(i).first, max = list.get(i).second;
			for(int j = i+1; j < list.size(); ++j) {
				Pair<Integer, Integer> curL = list.get(j);
				if(curL.first < min && curL.second > min) {
					min = curL.first;
					j = i;
					toRemove.add(curL);
				}
				if(curL.first < max && curL.second > max) {
					max = curL.second;
					j = i;
					toRemove.add(curL);
				}
				if(min <= curL.first && curL.second <= max) 
					toRemove.add(curL);
			}
			toRemove.add(list.get(i));
			list.removeAll(toRemove);
			toRemove.clear();
			res.add(new Pair<Integer, Integer>(min, max));
		}
		res.addAll(list);
		return res;
	}
}
