package Nothing_in_particular;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/*
 * Given an iterator with methods next() and hasNext(), create a wrapper iterator, 
 * PeekableInterface, which also implements peek(). 
 * peek shows the next element that would be returned on next().
 * 
 * Here is the interface:

class PeekableInterface(object):
    def __init__(self, iterator):
        pass

    def peek(self):
        pass

    def next(self):
        pass

    def hasNext(self):
        pass

 */

public class PeekableInterface<E> implements Iterator<E> {

	// Good driver function taken online
	public static void main(String[] args) {
		// Hashtable instance used for Iterator Example in Java
		Hashtable<Integer, String> stockTable = new Hashtable<Integer, String>();

		// Populating Hashtable instance with sample values
		stockTable.put(1, "Two");
		stockTable.put(2, "One");
		stockTable.put(4, "Four");
		stockTable.put(3, "Three");

		// Getting Set of keys for Iteration
		Set<Entry<Integer, String>> stockSet = stockTable.entrySet();

		// Making use of Iterator to loop Map in Java, here Map implementation is
		// Hashtable
		PeekableInterface<Entry<Integer, String>> i = new PeekableInterface<>(stockSet.iterator());
		System.out.println("Iterating over Hashtable in Java");

		// Iterator begins
		while (i.hasNext()) {
			Entry<Integer, String> m = i.next();
			int key = m.getKey();
			String value = m.getValue();
			System.out.println("Key :" + key + "  value :" + value);
		}
		System.out.println("Iteration over Hashtable finished");
	}

	/*
	 * We keep the iterator and an element, which will be used for the peek
	 */
	public Iterator<E> iter;
	public E nextElem;

	public PeekableInterface(Iterator<E> iter) {
		this.iter = iter;
	}

	/*
	 * If the iterator has a next element, we return it, without taking it out of it
	 * else, return null
	 */
	public E peek() {
		if (hasNext())
			return nextElem;
		return null;
	}

	/*
	 * If the next element if already set, return true. Else we try to get it from
	 * the iterator and return if we got it or not
	 */
	@Override
	public boolean hasNext() {
		if (nextElem != null)
			return true;

		if (iter.hasNext()) {
			nextElem = iter.next();
			return true;
		}

		return false;
	}

	/*
	 * Return the next element no matter what. Since using an iterator implies that
	 * we use "hasNext", we know the next element is set (null or not) so we don't
	 * need to actually retrieve it
	 */
	@Override
	public E next() {
		E temp = nextElem;
		nextElem = null;
		return temp;
	}
}
