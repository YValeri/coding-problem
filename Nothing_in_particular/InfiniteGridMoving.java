package Nothing_in_particular;

import java.util.ArrayList;

/*
 * You are in an infinite 2D grid where you can move in any of the 8 directions:

 (x,y) to
    (x+1, y),
    (x - 1, y),
    (x, y+1),
    (x, y-1),
    (x-1, y-1),
    (x+1,y+1),
    (x-1,y+1),
    (x+1,y-1)

 * You are given a sequence of points and the order in which you need to cover the points. 
 * Give the minimum number of steps in which you can achieve it. You start from the first point.
 * 
 * Example:

Input: [(0, 0), (1, 1), (1, 2)]
Output: 2

 * It takes 1 step to move from (0, 0) to (1, 1). It takes one more step to move from (1, 1) to (1, 2).
 */

public class InfiniteGridMoving {
	public static void main(String[] args) {
		ArrayList<Pair<Integer, Integer>> movements = new ArrayList<>();
		movements.add(new Pair<Integer, Integer>(0, 0));
		movements.add(new Pair<Integer, Integer>(1, 1));
		movements.add(new Pair<Integer, Integer>(1, 2));
		movements.add(new Pair<Integer, Integer>(0, 4));
		movements.add(new Pair<Integer, Integer>(-1, -5));
		movements.add(new Pair<Integer, Integer>(2, 1));
		movements.add(new Pair<Integer, Integer>(0, 0));
		movements.add(new Pair<Integer, Integer>(1, 1));
		movements.add(new Pair<Integer, Integer>(1, 2));
		movements.add(new Pair<Integer, Integer>(0, 4));
		movements.add(new Pair<Integer, Integer>(-1, -5));
		movements.add(new Pair<Integer, Integer>(2, 1));
		System.out.println(calculateSteps(movements));
	}
	
	public static int calculateSteps(ArrayList<Pair<Integer, Integer>> movements) {
		int res = 0;
		for(int i = 0; i < movements.size()-1; ++i) {
			Pair<Integer, Integer> cur = movements.get(i);
			Pair<Integer, Integer> target = movements.get(i+1);
			res += Math.max(Math.abs(cur.first - target.first), Math.abs(cur.second - target.second));
		}
		return res;
	}
}







