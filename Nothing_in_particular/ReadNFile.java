package Nothing_in_particular;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/*
 * Using a read7() method that returns 7 characters from a file, 
 * implement readN(n) which reads n characters.
 * 
 * For example, given a file with the content “Hello world”, 
 * three read7() returns “Hello w”, “orld” and then “”.
 */

public class ReadNFile {
	static int count = 0;
	static String file = "D:\\Eclipse\\eclipse-workspace\\Random_Algo\\src\\Coding_problems\\test.txt";
	
	public static void main(String[] args) throws Exception {
		String s = readN(22);
		System.out.println(s);
	}

	public static String read7() throws IOException {
		Path logPath = Paths.get(file);
        FileChannel channel = FileChannel.open(logPath, StandardOpenOption.READ);
        ByteBuffer buffer = ByteBuffer.allocate(7);

        if(channel.size() <= count)
        	return null;
        channel.read(buffer, count);
        count += 7;
        String s = new String(buffer.array());
		return s;
	}

	public static String readN(int n) throws IOException {
		String res = "";
		int i = 0;
		for(; (i+7) < n; i += 7) {
			String temp = read7();
			if(temp == null)
				return res;
			res += temp;
		}
		String temp = read7();
		if(temp == null)
			return res;
		temp = temp.substring(0, n-i);
		res += temp;
		return res;
	}
}
