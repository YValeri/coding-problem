package Nothing_in_particular;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * Given an unordered list of flights taken by someone, 
 * each represented as (origin, destination) pairs, 
 * and a starting airport, compute the person's itinerary. 
 * If no such itinerary exists, return null. 
 * If there are multiple possible itineraries, 
 * return the lexicographically smallest one. 
 * All flights must be used in the itinerary.
 * 
 * For example, given the list of flights [('SFO', 'HKO'), 
 * ('YYZ', 'SFO'), ('YUL', 'YYZ'), ('HKO', 'ORD')] and starting airport 
 * 'YUL', you should return the list ['YUL', 'YYZ', 'SFO', 'HKO', 'ORD'].
 * 
 * Given the list of flights [('SFO', 'COM'), ('COM', 'YYZ')] 
 * and starting airport 'COM', you should return null.
 * 
 * Given the list of flights [('A', 'B'), ('A', 'C'), ('B', 'C'), ('C', 'A')] 
 * and starting airport 'A', you should return the list ['A', 'B', 'C', 'A', 'C']
 * even though ['A', 'C', 'A', 'B', 'C'] is also a valid itinerary. 
 *  However, the first one is lexicographically smaller.
 */

public class ItineraryFlights {
	public static void main(String[] args) {
		ArrayList<Pair<String, String>> l = new ArrayList<>();
		long lStartTime, lEndTime, output;
		ArrayList<String> res;
		
		l.add(new Pair<String, String>("SFO", "HKO"));
		l.add(new Pair<String, String>("YYZ", "SFO"));
		l.add(new Pair<String, String>("YUL", "YYZ"));
		l.add(new Pair<String, String>("HKO", "ORD"));
		
		lStartTime = System.nanoTime();
		res = itinerary(l, "YUL");
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
        
        l = new ArrayList<>();
        l.add(new Pair<String, String>("SFO", "COM"));
		l.add(new Pair<String, String>("COM", "YYZ"));
		
		lStartTime = System.nanoTime();
		res = itinerary(l, "COM");
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
        
        l = new ArrayList<>();
		l.add(new Pair<String, String>("A", "C"));
        l.add(new Pair<String, String>("A", "B"));
		l.add(new Pair<String, String>("B", "C"));
		l.add(new Pair<String, String>("C", "A"));
		
		lStartTime = System.nanoTime();
		res = itinerary(l, "A");
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
	}
	
	public static ArrayList<String> itinerary(ArrayList<Pair<String, String>> l, String start){
		HashMap<String, String> map = new HashMap<>(l.size());
		for(Pair<String, String> p : l) {
			map.put(p.first, p.second);
		}
		String next = start;
		String prev = start;
		ArrayList<String> res = new ArrayList<>();
		res.add(start);
		while( (next = map.get(next)) != null ) {
			map.remove(prev);
			prev = next;
			res.add(next);
		}
		if(map.size() != 0)
			return null;
		else 
			return res;
		
	}
}
