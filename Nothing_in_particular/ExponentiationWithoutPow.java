package Nothing_in_particular;

/*
 * Implement integer exponentiation. 
 * That is, implement the pow(x, y) function, where x and y are integers and returns x^y.
 * 
 * Do this faster than the naive method of repeated multiplication.
 * 
 * For example, pow(2, 10) should return 1024.
 */

public class ExponentiationWithoutPow {
	public static void main(String[] args) {
		long lStartTime, lEndTime, output;
		double res;
		lStartTime = System.nanoTime();
		res = basicPow(2, 10);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(res);
	    
		lStartTime = System.nanoTime();
		res = basicPow(2, -2);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(res);
	    
		lStartTime = System.nanoTime();
		res = basicPow(-3, 3);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(res);
	    
	    lStartTime = System.nanoTime();
		res = basicPow(-3, 4);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(res);
	    
	    lStartTime = System.nanoTime();
		res = pow(2, 10);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(res);
	    
		lStartTime = System.nanoTime();
		res = pow(2, -2);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(res);
	    
		lStartTime = System.nanoTime();
		res = pow(-3, 3);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(res);
	    
	    lStartTime = System.nanoTime();
		res = pow(-3, 4);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
	    System.out.println(res);
	}
	
	public static double pow(int x, int n) {
		if(n == 1)
			return x;
		else if(n > 0) {
			if(n%2 == 0)
				return pow(x*x, n/2);
			else
				return x * pow(x*x, (n-1)/2);
		}else
			return 1 / pow(x, n*(-1));
	}
	
	public static double basicPow(int x, int y) {
		double res = 1;
		if(y >= 0) 
			for(int i = 0; i < y; ++i) 
				res *= x;
		else
			return 1 / basicPow(x, Math.abs(y));
		return res;
	}
}
