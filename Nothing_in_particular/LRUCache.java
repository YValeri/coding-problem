package Nothing_in_particular;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * See "LRUCacheImplement" problem.
 */

public class LRUCache<K, V> {
	HashMap<K, V> cache;
	ArrayList<K> LRU;
	int size;
	
	public LRUCache(int size) {
		this.size = size;
		cache = new HashMap<>(size);
		LRU = new ArrayList<>(size);
	}
	
	public void set(K key, V value) {
		if(cache.size() == size) {
			cache.remove(LRU.get(0));
			cache.put(key, value);
			LRU.remove(0);
			LRU.add(key);
		}else {
			cache.put(key, value);
			LRU.add(key);
		}
	}
	
	public V get(K key) {
		V res = cache.get(key);
		if(res == null)
			return null;
		LRU.remove(key);
		LRU.add(key);
		return res;
	}
	
	public void print() {
		System.out.println(cache);
		System.out.println(LRU);
	}
}
