package Nothing_in_particular;

/*
 * Given an unsigned 8-bit integer, swap its even and odd bits. 
 * The 1st and 2nd bit should be swapped, the 3rd and 4th bit should be swapped, and so on.
 * 
 * For example, 10101010 should be 01010101. 11100010 should be 11010001.
 * 
 * Bonus: Can you do this in one line?
 */

public class EvenOddBitSwapping {
	public static void main(String[] args) {
		System.out.println(swap(23));
		System.out.println(swap(52));
		System.out.println(swap(57894));
	}

	public static int swap(int n) {
		return (n & 0xAAAAAAAA) >> 1 | (n & 0x55555555) << 1;
	}
}
