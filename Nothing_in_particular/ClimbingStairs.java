package Nothing_in_particular;

/*
 * There exists a staircase with N steps, and you can climb up either 1 or 2 steps at a time. 
 * Given N, write a function that returns the number of unique ways you can climb the staircase. 
 * The order of the steps matters.
 * 
 * For example, if N is 4, then there are 5 unique ways:

    1, 1, 1, 1
    2, 1, 1
    1, 2, 1
    1, 1, 2
    2, 2
    
 * What if, instead of being able to climb 1 or 2 steps at a time, 
 * you could climb any number from a set of positive integers X? 
 * For example, if X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time.
 */

public class ClimbingStairs {
	public static void main(String[] args) {
		int N = 4;
		
		long lStartTime = System.nanoTime();
		int n = climb12(N);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(n);
        
        int[] set = {1,3,5};
        lStartTime = System.nanoTime();
		n = staircase(6, set);
		lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(n);
	}
	
	static int climb12(int N) {
		int a = 1, b = 1;
		for(int i = N; i > 1; i--) {
			int temp = a;
			a = b;
			b = temp + b;
		}
		
		return Math.max(a, b);
	}
	
	static int staircase(int N, int[] X) {
		int[] cache = new int[N];
		cache[0] = 1;
		for(int i = 1; i < N; i++) {
			int somme = 0;
			for(int j = 0; j < X.length; j++) {
				if(i-j >= 0)
					somme += cache[i-j];
			}
			cache[i] += somme;
		}
		return cache[cache.length-1];
	}
}
