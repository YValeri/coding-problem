package Nothing_in_particular;

/*
 * The area of a circle is defined as πr^2. 
 * Estimate π to 3 decimal places using a Monte Carlo method.
 * 
 * Hint: The basic equation of a circle is x2 + y2 = r2.
 */

public class CircleArea {
	public static void main(String[] args) {
		long lStartTime = System.nanoTime();
		double n = monteCarlo();
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(n);
	}
	
	static double monteCarlo() {
		double circle_points = 0, square_points = 0;
		double x, y;
		for(int i  = 0; i < 500; i++) {
			x = Math.random();
			y = Math.random();
			double d = x*x + y*y;
			if(d <= 1) 
				circle_points++;
			square_points++;
		}
		return 4*(circle_points/square_points);
	}
}
