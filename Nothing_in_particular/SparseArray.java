package Nothing_in_particular;

import java.util.ArrayList;

/*
 * You have a large array with most of the elements as zero.
 * 
 * Use a more space-efficient data structure, SparseArray, that implements the same interface:

    init(arr, size): initialize with the original large array and size.
    set(i, val): updates index at i with val.
    get(i): gets the value at index i.

 */

public class SparseArray {
	public static void main(String[] args) {
		int[] arr = { 0, 0, 0, 0, 1, 0, 0, 0, 0, 5, 0, 0, 0, 0, 8, 0, 0, 0, 0, 4, 0, 0, 0, 4, 0, 0, 0, 0, 6, 0, 0, 8, 0,
				0, 0, 4, 0, 0, 1, 0, 0, -5 };
		SparseArray sp = new SparseArray(arr, arr.length);
		System.out.println(sp);
		sp.set(1, 7);
		System.out.println(sp);
		sp.set(4, 11);
		System.out.println(sp);
		System.out.println(sp.get(0));
		System.out.println(sp.get(4));
		System.out.println(sp.get(7));
	}
	
	static ArrayList<Pair<Integer, Integer>> sparse;
	static int size;
	
	public SparseArray(int[] arr, int s) {
		size = s;
		sparse = new ArrayList<>();
		for(int i = 0; i < s; ++i)
			if(arr[i] != 0)
				sparse.add(new Pair<Integer, Integer>(i, arr[i]));
	}
	
	public void set(int index, int val) {
		for(int i = 0; i < sparse.size(); ++i) 
			if(sparse.get(i).first == index) {
				sparse.set(i, new Pair<Integer, Integer>(index, val));
				return;
			}
		sparse.add(new Pair<Integer, Integer>(index, val));
	}
	
	public int get(int index) {
		for(int i = 0; i < sparse.size(); ++i) 
			if(sparse.get(i).first == index)
				return sparse.get(i).second;
		return 0;
	}
	
	public String toString() {
		String s = "[";
		for(int i = 0; i < sparse.size()-1; ++i) 
			s += sparse.get(i) + ", ";
		return s + sparse.get(sparse.size()-1) + "]";
	}
}
