package Nothing_in_particular;

/*
 * Find the minimum number of coins required to make n cents.
 * 
 * You can use standard American denominations, that is, 1¢, 5¢, 10¢, and 25¢.
 * 
 * For example, given n = 16, return 3 since we can make it with a 10¢, a 5¢, and a 1¢.
 */

public class NumberOfCoins {
	public static void main(String[] args) {
		int n = 16;
		System.out.println(amount(n));
		n = 17;
		System.out.println(amount(n));
		n = 22;
		System.out.println(amount(n));
		n = 26;
		System.out.println(amount(n));
		n = 107;
		System.out.println(amount(n));
	}

	public static int amount(int n) {
		int cpt = 0, temp, val = 25;

		while (n != 0) {
			// temp = how many coins of val value can fit into current n cents
			temp = n / val;
			// cpt = previous amount + that new amount of coins
			cpt += temp;
			// n = old cents - (the new amount * the value of a coin)
			n -= temp * val;
			// Update the value of a coin as stated by the problem
			if (val == 25) val = 10;
			else if (val == 10)	val = 5;
			else val = 1;
		}

		return cpt;
	}
}
