package Nothing_in_particular;

/*
 * Given a function f, and N return a debounced f of N milliseconds.
 * 
 * That is, as long as the debounced f continues to be invoked, f itself will not be called for N milliseconds.
 */

public class DebouncedFunction {
	public static void main(String[] args) {
		try {
			delayed();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void delayed() throws InterruptedException {
		long l = System.currentTimeMillis();
		System.out.println("start of delayed at time "+l);
        MyRunnable myRunnable = new MyRunnable(3 * 1000L);
		Thread thread = new Thread(myRunnable);
		thread.start();
		System.out.println("Reset after "+(System.currentTimeMillis() - l)+" milliseconds have passed.");
		myRunnable.reset();
		System.out.println("Sleep of 1 sec after "+(System.currentTimeMillis() - l)+" milliseconds have passed.");
		Thread.sleep(1000);
		System.out.println("Reset after "+(System.currentTimeMillis() - l)+" milliseconds have passed.");
		myRunnable.reset();
		System.out.println("Sleep of 2 sec after "+(System.currentTimeMillis() - l)+" milliseconds have passed.");
		Thread.sleep(2000);
		System.out.println("Reset after "+(System.currentTimeMillis() - l)+" milliseconds have passed.");
		myRunnable.reset();
		System.out.println("Sleep of 4 sec after "+(System.currentTimeMillis() - l)+" milliseconds have passed.");
		Thread.sleep(4000);
		System.out.println("End of program after "+(System.currentTimeMillis() - l)+" milliseconds have passed.");
	}
}
