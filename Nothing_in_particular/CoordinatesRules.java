package Nothing_in_particular;

import java.util.ArrayList;

/*
 * A rule looks like this:

A NE B

 * This means this means point A is located northeast of point B.

A SW C

 * means that point A is southwest of C.

 * Given a list of rules, check if the sum of the rules validate. For example:

A N B
B NE C
C N A

 * does not validate, since A cannot be both north and south of C.

A NW B
B SE A

 * is considered valid.
 */

public class CoordinatesRules {
	public static void main(String[] args) {
		ArrayList<String> l = new ArrayList<>();
		l.add("A NW B");
		l.add("B SE A");
		System.out.println(l);
		System.out.println(valid(l));
	}
	
	public static boolean valid(ArrayList<String> l) {
		int tempI = l.size() / 2;
		int tempJ = l.size() / 2;
		char[][] marqued = new char[l.size()+1][l.size()+1];
		for (int i = 0; i < marqued.length; i++) {
			for (int j = 0; j < marqued.length; j++) {
				marqued[i][j] = '0';
			}
		}
		for(String s : l) {
			marqued[tempI][tempJ] = s.charAt(s.length()-1);
			String temp = s.substring(2, 4);
			if(temp.contains(" ")) {
				switch(s.charAt(2)) {
				case 'N':
					if(marqued[tempI-1][tempJ] != '0')
						return false;
					marqued[tempI-1][tempJ] = s.charAt(0);
					tempI--;
					break;
				case 'S':
					if(marqued[tempI+1][tempJ] != '0')
						return false;
					marqued[tempI+1][tempJ] = s.charAt(0);
					tempI++;
					break;
				case 'W':
					if(marqued[tempI][tempJ-1] != '0')
						return false;
					marqued[tempI][tempJ-1] = s.charAt(0);
					tempJ--;
					break;
				case 'E':
					if(marqued[tempI][tempJ+1] != '0')
						return false;
					marqued[tempI][tempJ+1] = s.charAt(0);
					tempJ++;
					break;
				default:
						return false;
				}
			}else {
				switch(temp) {
				case "NW":
					if(marqued[tempI-1][tempJ-1] != '0')
						return false;
					marqued[tempI-1][tempJ-1] = s.charAt(0);
					tempI--;
					tempJ--;
					break;
				case "NE":
					if(marqued[tempI-1][tempJ+1] != '0')
						return false;
					marqued[tempI-1][tempJ+1] = s.charAt(0);
					tempI--;
					tempJ++;
					break;
				case "SW":
					if(marqued[tempI+1][tempJ-1] != '0')
						return false;
					marqued[tempI+1][tempJ-1] = s.charAt(0);
					tempI++;
					tempJ--;
					break;
				case "SE":
					if(marqued[tempI+1][tempJ+1] != '0')
						return false;
					marqued[tempI+1][tempJ+1] = s.charAt(0);
					tempI++;
					tempJ++;
					break;
				default:
					return false;
				}
			}
		}
		return true;
	}
}
