package Nothing_in_particular;

import java.util.Random;

/*
 * Using a function rand5() that returns an integer from 1 to 5 (inclusive) with uniform probability, 
 * implement a function rand7() that returns an integer from 1 to 7 (inclusive).
 */

public class Rand7 {
	public static void main(String[] args) {
		for(int i = rand7(), j = 0; j < 20; i = rand7(), ++j)
			System.out.println(i);
	}
	
	public static int rand7() {
		int i;
		do
		{
		  i = 5 * (rand5() - 1) + rand5();  // i is now uniformly random between 1 and 25
		} while(i > 21);
		// i is now uniformly random between 1 and 21
		return i % 7 + 1;  // result is now uniformly random between 1 and 7
	}
	
	public static int rand5() {
		Random r = new Random();
		return r.nextInt(5)+1;
	}
}
