package Nothing_in_particular;

/*
 * Given a real number n, find the square root of n. 
 * For example, given n = 9, return 3.
 */

public class SquareRoot {
	public static void main(String[] args) {
		System.out.println(sqr(12.17));
	}

	public static String sqr(double n) {
		if(n == 0)
			return "0";
		
		boolean isPositiveNumber = (n > 0);
		if(n < 0)
			n = -n;
		double g1;
		double squareRoot = n / 2;
		do {
			g1 = squareRoot;
			squareRoot = (g1 + (n / g1)) / 2;
		} while ((g1 - squareRoot) != 0);

		String res = "+-"+squareRoot;
		if(!isPositiveNumber)
			return res+"i";
		return res;
	}
}
