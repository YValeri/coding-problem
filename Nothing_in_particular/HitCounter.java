package Nothing_in_particular;

import java.sql.Timestamp;
import java.util.ArrayList;

/*
 * Design and implement a HitCounter class that keeps track of requests (or hits). 
 * It should support the following operations:

    record(timestamp): records a hit that happened at timestamp
    total(): returns the total number of hits recorded
    range(lower, upper): returns the number of hits that occurred between timestamps lower and upper (inclusive)

 * Follow-up: What if our system has limited memory?
 */

public class HitCounter {
	static ArrayList<Timestamp> l = new ArrayList<>();
	
	public static void main(String[] args) throws InterruptedException {
        Timestamp t1 = new Timestamp(System.currentTimeMillis());
		Thread.sleep(300);
        Timestamp t2 = new Timestamp(System.currentTimeMillis());
		Thread.sleep(200);
        Timestamp t3 = new Timestamp(System.currentTimeMillis());
		Thread.sleep(100);
        Timestamp t4 = new Timestamp(System.currentTimeMillis());
		Thread.sleep(400);
        Timestamp t5 = new Timestamp(System.currentTimeMillis());
		Thread.sleep(700);
        Timestamp t6 = new Timestamp(System.currentTimeMillis());
		Thread.sleep(1000);
		record(t1);
		record(t2);
		record(t3);
		record(t4);
		record(t5);
		record(t6);
		System.out.println(total());
		System.out.println(range(t3, t6));
        
	}
	
	public static void record(Timestamp t) {
		l.add(t);
	}
	
	public static int total() {
		return l.size();
	}
	
	public static int range(Timestamp lower, Timestamp upper) {
		int cpt = 0;
		for(Timestamp t : l)
			if(t.compareTo(lower) >= 0 && t.compareTo(upper) <= 0)
				cpt++;
		return cpt;
	}
}
