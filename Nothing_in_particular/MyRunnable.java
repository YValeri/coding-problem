package Nothing_in_particular;

/*
 * See "DebouncedFunction" problem.
 */

public class MyRunnable implements Runnable {
	private long N;
	private long time;

	public MyRunnable(long N) {
		this.N = N;
		time = System.currentTimeMillis();
	}

	public synchronized void reset() {
		System.out.println("time to reset the function.");
		time = System.currentTimeMillis();
	}

	@Override
	public void run() {
		System.out.println();
		while ((System.currentTimeMillis() - time) < N)
			System.out.println("Thread time = "+(System.currentTimeMillis() - time));;
		System.out.println("Mission failed !");
	}

}
