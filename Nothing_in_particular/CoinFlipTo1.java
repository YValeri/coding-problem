package Nothing_in_particular;

/*
 * You have 100 fair coins and you flip them all at the same time. 
 * Any that come up tails you set aside. The ones that come up heads you flip again. 
 * How many rounds do you expect to play before only one coin remains?
 * 
 * Write a function that, given n, returns the number of rounds you'd expect to play until one coin remains.
 */

public class CoinFlipTo1 {
	public static void main(String[] args) {
		int nb = 255;
		System.out.println(average(nb));
	}

	public static int average(int nb) {
		int cpt = 0;
		while(nb != 1) {
			cpt++;
			nb = nb >> 1;
		}
		return cpt;
	}
}
