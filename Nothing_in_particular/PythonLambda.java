package Nothing_in_particular;

/*
 * What does the below code snippet print out? How can we fix the anonymous functions to behave as we'd expect?

functions = []
for i in range(10):
    functions.append(lambda : i)

for f in functions:
    print(f())

 */

public class PythonLambda {
	/*
	 * functions = [] 
	 * for i in range(10): 
	 *     	functions.append(lambda i : i)
	 * 
	 * for i, f in enumerate(functions): 
	 * 		print(f(i))
	 */
}
