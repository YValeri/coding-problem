package Nothing_in_particular;

import java.util.Random;

/*
 * Assume you have access to a function toss_biased() which returns 0 or 1 with a probability 
 * that's not 50-50 (but also not 0-100 or 100-0). You do not know the bias of the coin.
 * 
 * Write a function to simulate an unbiased coin toss.
 */

public class BiaisedCoinToss {
	static int i = 0;
	public static void main(String[] args) {
		double total = 0;
		for(int i = 0; i < 1000; ++i)
			total += toin_coss();
		System.out.println(total/1000);
	}
	
	public static double toin_coss() {
		int t1, t2;
		do {
			t1 = toss_biaised();
			t2 = toss_biaised();
		}while(t1 != t2);
		return t1;
	}
	
	public static int toss_biaised() {
		return ((new Random()).nextInt(10) < 6)?0:1;
	}
}
