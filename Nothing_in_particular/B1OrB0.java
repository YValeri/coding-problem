package Nothing_in_particular;

/*
 * Given three 32-bit integers x, y, and b, return x if b is 1 and y if b is 0, 
 * using only mathematical or bit operations. 
 * You can assume b can only be 1 or 0.
 */

public class B1OrB0 {
	public static void main(String[] args) {
		int x = 3, y = 5, b = 1;
		System.out.println(check(x, y, b));
		System.out.println(check(x, y, 0));
	}
	
	public static int check(int x, int y, int b) {
		if((b & 1) == 1)
			return x;
		else
			return y;
	}
}
