package Nothing_in_particular;

import java.util.Random;

/*
 * You are given n numbers as well as n probabilities that sum up to 1. 
 * Write a function to generate one of the numbers with its corresponding probability.
 * 
 * For example, given the numbers [1, 2, 3, 4] and probabilities [0.1, 0.5, 0.2, 0.2], 
 * your function should return 1 10% of the time, 2 50% of the time, and 3 and 4 20% of the time.
 * 
 * You can generate random numbers between 0 and 1 uniformly.
 */

public class CumulativeSum {
	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5,6};
		double[] prob = {0.1,0.2,0.3,0.1,0.1,0.2};
		System.out.println(cumSum(arr, prob));
	}
	
	//Simple cumulative sum over the probabilistic array
	public static int cumSum(int[] arr, double[] prob) {
		
		//Generate the random between 0 and 1
		Random r = new Random();
		double temp = r.nextDouble();
		
		//Traverse the probalities array
		for(int i = 0; i < prob.length; ++i)
			//if temp < the probability at a certain point, return it
			if(temp < prob[i])
				return arr[i];
			else //else, take out this probability from the generated number
				temp -= prob[i];
		return arr[prob.length - 1];
	}
}
