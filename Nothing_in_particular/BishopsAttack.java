package Nothing_in_particular;

/*
 * On our special chessboard, two bishops attack each other if they share the same diagonal. 
 * This includes bishops that have another bishop located between them, 
 * i.e. bishops can attack through pieces.
 * 
 * You are given N bishops, represented as (row, column) tuples on a M by M chessboard. 
 * Write a function to count the number of pairs of bishops that attack each other. 
 * The ordering of the pair doesn't matter: (1, 2) is considered the same as (2, 1).
 * 
 * For example, given M = 5 and the list of bishops:

    (0, 0)
    (1, 2)
    (2, 2)
    (4, 0)

 * The board would look like this:

[b 0 0 0 0]
[0 0 b 0 0]
[0 0 b 0 0]
[0 0 0 0 0]
[b 0 0 0 0]

 * You should return 2, since bishops 1 and 3 attack each other, as well as bishops 3 and 4.
 */

import java.util.ArrayList;

public class BishopsAttack {
	public static void main(String[] args) {
		ArrayList<Pair<Integer, Integer>> bishops = new ArrayList<>();
		bishops.add(new Pair<Integer,Integer>(0, 0));
		bishops.add(new Pair<Integer,Integer>(1, 2));
		bishops.add(new Pair<Integer,Integer>(2, 2));
		bishops.add(new Pair<Integer,Integer>(4, 0));
		bishops.add(new Pair<Integer,Integer>(3, 3));
		bishops.add(new Pair<Integer,Integer>(2, 3));
		
		Pair<Integer,Integer> temp = new Pair<Integer,Integer>(0, 0);
		if(bishops.contains(temp))
			System.out.println("slt");
		int res = countPairs(bishops, 5);
		System.out.println(res);
	}
	
	public static int countPairs(ArrayList<Pair<Integer, Integer>> bishops, int M) {
		int res = 0;
		while(bishops.size() != 0) {
			Pair<Integer, Integer> p = bishops.get(0);
			System.out.println("p.first = "+p.first+", p.second = "+p.second);
			res += fourWays(p, bishops, M);
			bishops.remove(p);
		}
		return res;
	}
	
	public static boolean contient(int i, int j, ArrayList<Pair<Integer, Integer>> bishops) {
		for(Pair<Integer, Integer> p : bishops) {
			if(p.first == i && p.second == j)
				return true;
		}
		return false;
	}
	
	public static int fourWays(Pair<Integer, Integer> cur, ArrayList<Pair<Integer, Integer>> bishops, int M) {
		int res = 0;
		for(int i = 1; i < M; ++i) {
			if(cur.first-i >= 0 && cur.second-i >= 0) 
				if(contient(cur.first-i, cur.second-i, bishops))
					res++;
			
			if(cur.first-i >= 0 && cur.second+i < M) 
				if(contient(cur.first-i, cur.second+i, bishops))
					res++;
			
			if(cur.first+i < M && cur.second-i >= 0) 
				if(contient(cur.first+i, cur.second-i, bishops))
					res++;
			
			if(cur.first+i < M && cur.second+i < M) 
				if(contient(cur.first+i, cur.second+i, bishops))
					res++;
		}
		return res;
	}
}
