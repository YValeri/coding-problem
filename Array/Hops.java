package Array;

/*
 * Given an integer list where each number represents the number of hops you can make, 
 * determine whether you can reach to the last index starting at index 0.
 * 
 * For example, [2, 0, 1, 0] returns true while [1, 1, 0, 1] returns false.
 */

public class Hops {
	public static void main(String[] args) {
		int[] arr = { 3, 2, 1, 1, 0 };
		System.out.println(hophop(arr));
	}

	public static boolean hophop(int[] arr) {
		// If the first integer is 0, we cannot jump anywhere
		// So we can't get to the end
		if (arr[0] == 0)
			return false;

		int maxReach = arr[0];
		int step = arr[0];

		// We traverse the array
		for (int i = 1; i < arr.length; i++) {
			System.out.println("i = " + i + ", arr[i] = " + arr[i] + ", max = " + maxReach + ", step = " + step);
			// If we got to the end, we return true
			if (i == arr.length - 1)
				return true;

			// maxReach = maximum(maximum position we can reach, current pos + where we can
			// jump)
			maxReach = Math.max(maxReach, i + arr[i]);

			System.out.println("max = " + maxReach);

			step--;

			// If we did the maximum number of jumps we could do when starting at a certain
			// point
			if (step == 0) {
				// If we got the maximum position we could reach, return false
				if (i >= maxReach)
					return false;
				// else we got to the furthest position we can currently get, minus the current
				// position
				step = maxReach - i;
			}

			System.out.println("step = " + step);
		}
		return false;
	}
}
