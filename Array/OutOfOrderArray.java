package Array;

/*
 * We can determine how "out of order" an array A is by counting the number of inversions it has. 
 * Two elements A[i] and A[j] form an inversion if A[i] > A[j] but i < j. 
 * That is, a smaller element appears after a larger element.
 * 
 * Given an array, count the number of inversions it has. Do this faster than O(N^2) time.
 * 
 * You may assume each element in the array is distinct.
 * 
 * For example, a sorted list has zero inversions. 
 * The array [2, 4, 1, 3, 5] has three inversions: (2, 1), (4, 1), and (4, 3). 
 * The array [5, 4, 3, 2, 1] has ten inversions: every distinct pair forms an inversion.
 */

public class OutOfOrderArray {

	public static void main(String[] args) {
		int[] tab = { 5, 4, 3, 2, 1 };
		long lStartTime;
		long lEndTime;
		long output;
		int res;

		lStartTime = System.nanoTime();
		res = mergeSort(tab, tab.length);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(res);
	}

	public static int mergeSort(int arr[], int array_size) {
		int temp[] = new int[array_size];
		return mergeSortInter(arr, temp, 0, array_size - 1);
	}

	public static int mergeSortInter(int arr[], int temp[], int left, int right) {
		int mid, invCount = 0;
		if (right > left) {
			mid = (right + left) / 2;

			// Call mergeSortInter of the left and right part of the array, and add the
			// result to invCount
			invCount = mergeSortInter(arr, temp, left, mid);
			invCount += mergeSortInter(arr, temp, mid + 1, right);

			// We merge the two parts, and increments invCount again
			invCount += merge(arr, temp, left, mid + 1, right);
		}
		return invCount;
	}

	public static int merge(int arr[], int temp[], int left, int mid, int right) {
		int i, j, k;
		int invCount = 0;

		i = left; // i is index for left subarray
		j = mid; // j is index for right subarray
		k = left; // k is index for resultant merged subarray
		
		// While we didn't go through at least one of the subarrays
		while ((i <= mid - 1) && (j <= right)) {
			// If the current element in the left subarray is lower
			// than the current element in the right subbaray
			if (arr[i] <= arr[j]) {
				//set temp[k] to arr[i], and increment both variables
				temp[k++] = arr[i++];
			} else {
				//set temp[k] to arr[j], and increment both variables
				temp[k++] = arr[j++];

				//If the element in the right subarray was greater than the one in the left
				//we increment invCount by mid-i
				invCount += (mid - i);
			}
		}

		//Copy the potential remaining values in temp
		while (i <= mid - 1)
			temp[k++] = arr[i++];

		//Copy the potential remaining values in temp
		while (j <= right)
			temp[k++] = arr[j++];

		//Copy all the values in temp to arr
		for (i = left; i <= right; i++)
			arr[i] = temp[i];

		return invCount;
	}
}
