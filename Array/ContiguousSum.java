package Array;

import java.util.ArrayList;

/*
 * Given a list of integers and a number K, return which contiguous elements of the list sum to K.
 * 
 * For example, if the list is [1, 2, 3, 4, 5] and K is 9, then it should return [2, 3, 4].
 */

public class ContiguousSum {
	public static void main(String[] args) {
		int[] arr = { 1, 2, 4, 6, 7, 7, 8, -12, 4, -8, 10, 57, 5, 7, 5, 8, 2, 4, 7 };
		int K = 9;

		long lStartTime = System.nanoTime();
		ArrayList<Integer> l = list(arr, K);
		long lEndTime = System.nanoTime();
		long output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(l);

		lStartTime = System.nanoTime();
		l = list2(arr, K);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(l);
	}

	// The simple way is to do double for loop, and check every contiguous sequence
	// inferior to K
	public static ArrayList<Integer> list(int[] arr, int K) {
		for (int i = 0; i < arr.length; ++i) {
			ArrayList<Integer> res = new ArrayList<>();
			int val = 0;
			for (int j = i; j < arr.length && val <= K; ++j) {
				val += arr[j];
				res.add(arr[j]);
				if (val == K)
					return res;
			}
		}
		return null;
	}

	// A better solution is to keep a list of integers, in which we add all elements
	// of the array
	// If the sum is K, we return the list
	// If the sum is larger than |K|, we remove the first element
	public static ArrayList<Integer> list2(int[] arr, int K) {
		int val = 0;
		ArrayList<Integer> res = new ArrayList<>();
		for (int i = 0; i < arr.length; ++i) {
			val += arr[i];
			res.add(arr[i]);
			if (val == K)
				return res;
			else if (Math.abs(val) > Math.abs(K)) {
				val -= res.get(0);
				res.remove(0);
			}
		}
		return null;
	}
}
