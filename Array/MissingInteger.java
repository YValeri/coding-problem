package Array;

import java.util.Arrays;

/*
 * Given an array of integers, find the first missing positive integer in linear time and constant space. 
 * In other words, find the lowest positive integer that does not exist in the array. 
 * The array can contain duplicates and negative numbers as well.
 * 
 * For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
 * 
 * You can modify the input array in-place.
 */

public class MissingInteger {
	public static void main(String[] args) {
		int[] arr = {3,4,-1,1};
		findMissingInteger(arr);
		int[] arr2 = {1,2,0};
		findMissingInteger(arr2);
		int[] arr3 = {2,3,4,5,6,7,8,1};
		findMissingInteger(arr3);
		int[] arr4 = {-1};
		findMissingInteger(arr4);
	}

	public static void findMissingInteger(int[] arr) {
		System.out.println(Arrays.toString(arr));
		int end = movePositive(arr);
		swapSigns(arr, end);
		int missing = -1;

		// Search for the first positive integer
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] >= 0) {
				missing = i + 1;
				break;
			}
		}

		// If we found it, it's index+1 is the missing integer
		System.out.println("The first missing integer is : " + ((missing == -1) ? (arr.length + 1) : missing));
	}

	// Function to swap all negative integers of the array at the end
	// Returns the index of the last positive integer
	public static int movePositive(int[] arr) {
		int j = arr.length - 1;
		for (int i = 0; i < arr.length && i < j; i++) {
			if (arr[i] <= 0) {
				int temp = arr[j];
				arr[j] = arr[i];
				arr[i] = temp;
				j--;
				i--;
			}
		}
		return j;
	}

	public static void swapSigns(int[] arr, int end) {
		// We go through the positive part of the array
		for (int i = 0; i <= end; i++) {
			// For each element, we get val = |arr[i]| - 1
			int val = Math.abs(arr[i]) - 1;
			// If val is between 0 and end
			if (val <= end && val >= 0)
				// We swap the sign of the corresponding index in the array
				arr[val] *= -1;
		}
		// After that function, all indexes n between 1 and end are negative IF there is
		// An i such that arr[i] = n
		// That way, the index of the first positive integer of the array is the missing
		// integer
	}
}
