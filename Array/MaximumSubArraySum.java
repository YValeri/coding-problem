package Array;

/*
 * Given an array of numbers, find the maximum sum of any contiguous subarray of the array.
 * 
 * For example, given the array [34, -50, 42, 14, -5, 86], the maximum sum would be 137, 
 * since we would take elements 42, 14, -5, and 86.
 * 
 * Given the array [-5, -1, -8, -9], the maximum sum would be 0, 
 * since we would not take any elements.
 * 
 * Do this in O(N) time.
 */

public class MaximumSubArraySum {
	public static void main(String[] args) {
		int[] tab = { 34, -50, 42, 14, -5, 86 };
		long lStartTime;
		long lEndTime;
		long output;
		int res;

		lStartTime = System.nanoTime();
		res = maxSum(tab);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(res);
	}

	public static int maxSum(int[] tab) {
		int currentMax = 0, Totalmax = 0;
		// We go through the array
		for (int i = 0; i < tab.length; ++i) {
			// Add the current element to the current max
			currentMax += tab[i];
			// If we went in the negative, we just forget the sum, as we're sure we can go
			// higher later on
			if (currentMax < 0) {
				currentMax = 0;
			// If the current max is greater than the global max, we update it
			} else if (currentMax > Totalmax)
				Totalmax = currentMax;

		}
		return Totalmax;
	}
}
