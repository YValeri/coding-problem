package Array;

import java.util.Arrays;

/*
 * Given a number represented by a list of digits, find the next greater permutation of a number, 
 * in terms of lexicographic ordering. 
 * If there is not greater permutation possible, return the permutation with the lowest value/ordering.
 * 
 * For example, the list [1,2,3] should return [1,3,2]. 
 * The list [1,3,2] should return [2,1,3]. 
 * The list [3,2,1] should return [1,2,3].
 * 
 * Can you perform the operation without allocating extra memory (disregarding the input memory)?
 */

public class NextGreaterPermutation {
	public static void main(String[] args) {
		int[] arr = { 0, 1, 2, 5, 3, 3, 6 };
		next(arr);
		System.out.println(Arrays.toString(arr));
		int[] arr2 = { 1, 2, 3 };
		next(arr2);
		System.out.println(Arrays.toString(arr2));
		int[] arr3 = { 1, 3, 2 };
		next(arr3);
		System.out.println(Arrays.toString(arr3));
		int[] arr4 = { 3, 2, 1 };
		next(arr4);
		System.out.println(Arrays.toString(arr4));
		int[] arr5 = { 0, 1, 2, 5, 3, 3, 8 };
		next(arr5);
		System.out.println(Arrays.toString(arr5));
	}

	public static void next(int[] arr) {
		int i;

		// Get the first value greater than the next one, in decreasing order
		for (i = arr.length - 1; i > 0; i--) {
			if (arr[i] > arr[i - 1]) {
				break;
			}
		}

		// If i = 0, that means the array in sorted in decreasing order
		// So we sort it in increasing order
		if (i == 0)
			Arrays.sort(arr);
		else {
			int x = arr[i - 1], min = i;

			// We search the first index after i with a value between arr[i-1] and arr[i]
			for (int j = i + 1; j < arr.length; j++)
				if (x < arr[j] && arr[j] < arr[min])
					min = j;

			// Swap the value at the found index and the value at i-1
			int temp = arr[i - 1];
			arr[i - 1] = arr[min];
			arr[min] = temp;

			// Then we sort the array from i to the end
			Arrays.sort(arr, i, arr.length);
		}
	}
}
