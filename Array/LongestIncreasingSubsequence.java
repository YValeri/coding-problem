package Array;

/*
 * Given an array of numbers, find the length of the longest increasing subsequence in the array. 
 * The subsequence does not necessarily have to be contiguous.
 * 
 * For example, given the array [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15], 
 * the longest increasing subsequence has length 6: it is 0, 2, 6, 9, 11, 15.
 */

public class LongestIncreasingSubsequence {
	public static void main(String[] args) {
		int[] arr = { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };
		System.out.println(measure(arr));
	}

	public static int measure(int[] arr) {
		// We create a new array of the same length, initialized at 1
		int[] temp = new int[arr.length];
		for (int i = 0; i < temp.length; ++i)
			temp[i] = 1;

		// For each value in arr
		for (int i = 1; i < arr.length; ++i)
			// we go through all the elements at a lower index than i
			for (int j = 0; j < i; ++j)
				// if the current value is greater than the a previous one
				if (arr[i] > arr[j])
					// we update the temp array by taking the max between
					// the temp value at the current position
					// the temp value at that previous position + 1
					temp[i] = Math.max(temp[i], temp[j] + 1);

		// return the last index, as it contains the longest increasing subsequence
		return temp[temp.length - 1];
	}
}