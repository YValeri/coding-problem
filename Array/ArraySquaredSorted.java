package Array;

import java.util.Arrays;

/*
 * Given a sorted list of integers, square the elements and give the output in sorted order.
 * 
 * For example, given [-9, -2, 0, 2, 3], return [0, 4, 4, 9, 81].
 */

public class ArraySquaredSorted {
	public static void main(String[] args) {
		int[] arr = { -9, -6, 0, 3, 6 };
		System.out.println(Arrays.toString(squareAndSort(arr)));
	}

	public static int[] squareAndSort(int[] arr) {
		// We create the array we'll sent back
		int[] ret = new int[arr.length];

		// i will go up and j down to keep track of the sorted integers in arr
		// retj will be the index where we put the next squared integer
		int i = 0, j = arr.length - 1, retj = arr.length - 1;

		while (i < j) {
			// if |arr[i]| > |arr[j]|, then arr[i]^2 will be greater than arr[j]^2
			if (Math.abs(arr[i]) > Math.abs(arr[j]))
				ret[retj] = arr[i] * arr[i++];
			else // we change ret[retj] to the squared integer from ret and
					// increment/decrement the according integers
				ret[retj] = arr[j] * arr[j--];

			retj--;
		}
		return ret;
	}
}
