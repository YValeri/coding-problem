package Array;

/*
 * Given an array, print all it's subsets.
 */

public class SubsetOfSet {

	public static void main(String[] args) {
		int[] arr = {0, 1, 2, 3, 3, 5, 8};
		for(int i = 0; i < arr.length; i++) {
			System.out.println("Set of length "+i+" :");
			loop(i, arr, 0, "{");
			System.out.println();
		}
		System.out.print("Set of length "+arr.length+" :\n{");
		for(int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]);
			if( i < arr.length-1) System.out.print(",");
		}
		System.out.println("}");
	}
	
	static void loop(int taille, int[] arr, int start, String s) {
		if(taille == 0) 
			System.out.println("{}");
		if(taille == 1) 
			for(int i = start; i < arr.length; i++)
				System.out.println(s+arr[i]+"}");
		else 
			for(int i = start; i < arr.length-1; i++) 
				loop(taille-1, arr, i+1, s+arr[i]+",");
	}
}
