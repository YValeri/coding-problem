package Array;

/*
 * Given an array of integers, return a new array such that each element at index i of the new array 
 * is the product of all the numbers in the original array except the one at i.
 * 
 * For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24].
 * If our input was [3, 2, 1], the expected output would be [2, 3, 6].
 * 
 * Follow-up: what if you can't use division?
 */

public class NewArrayTimes {
	public static void main(String[] args) {
		int[] arr = { 1, 2, 3, 4, 5 };

		long lStartTime = System.nanoTime();
		timesArray(arr);
		long lEndTime = System.nanoTime();
		long output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);

		lStartTime = System.nanoTime();
		timesArrayWithoutDiv(arr);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
	}

	// Simple solution where we multiply all integers in the array to get n
	// and set each value of the array as n / arr[i]
	public static void timesArray(int[] arr) {
		int[] nArr = new int[arr.length];
		int total = 1;

		for (int i = 0; i < arr.length; i++)
			total = total * arr[i];

		for (int i = 0; i < arr.length; i++) {
			nArr[i] = total / arr[i];
		}
	}

	public static void timesArrayWithoutDiv(int[] arr) {
		// Create a new array and set all values at 1
		int[] nArr = new int[arr.length];
		for (int i = 0; i < arr.length; i++)
			nArr[i] = 1;

		// Traverse the first array with a double for loop
		for (int i = 0; i < arr.length; i++)
			for (int j = 0; j < arr.length; j++)
				// if the current index is different from the "global" one
				if (i != j)
					// multiply the value of the new array at the "global" index by the value of the
					// array at the current index
					nArr[i] = nArr[i] * arr[j];
	}
}
