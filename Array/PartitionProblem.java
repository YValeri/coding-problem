package Array;

import java.util.Arrays;

/*
 * Given a multiset of integers, return whether it can be partitioned into two subsets whose sums are the same.
 * 
 * For example, given the multiset {15, 5, 20, 10, 35, 15, 10}, it would return true, 
 * since we can split it up into {15, 5, 10, 15, 10} and {20, 35}, which both add up to 55.
 * 
 * Given the multiset {15, 5, 20, 10, 35}, it would return false, 
 * since we can't split it up into two subsets that add up to the same sum.
 */

public class PartitionProblem {
	public static void main(String[] args) {
		int[] arr = { 15, 5, 20, 10, 35, 15, 10 };

		long lStartTime, lEndTime, output;
		boolean res;
		lStartTime = System.nanoTime();
		res = partition(arr);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(res);

		int[] arr2 = { 15, 5, 20, 10, 35 };
		lStartTime = System.nanoTime();
		res = partition(arr2);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(res);

		int[] arr3 = { 15, 5, 20, 10, 35, 15, 10, -10, -10 };
		lStartTime = System.nanoTime();
		res = partition(arr3);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(res);
	}

	public static boolean partition(int[] arr) {
		// Create two new sums
		int sum1 = 0;
		int sum2 = 0;

		// Sort and reverse the array, necessary for this method as negative number
		// could destroy the whole process
		Arrays.sort(arr);
		for (int i = 0; i < arr.length / 2; ++i) {
			int temp = arr[i];
			arr[i] = arr[arr.length - 1 - i];
			arr[arr.length - 1 - i] = temp;
		}

		// Traverse the array
		for (int i = 0; i < arr.length; ++i) {
			// If the first sum is less than the second, we increase it by the value of the
			// current cell, else increase the second sum
			if (sum1 < sum2) 
				sum1 += Math.abs(arr[i]);
			else 
				sum2 += Math.abs(arr[i]);
			
			// The goal here is to even the two sums
		}

		// If we could create a partition with this array, than the sums are the same
		return sum1 == sum2;
	}
}
