package Array;

import java.util.Arrays;

/*
 * Given an array of strictly the characters 'R', 'G', and 'B', segregate the values of the array so that all the Rs come first, 
 * the Gs come second, and the Bs come last. You can only swap elements of the array.
 * 
 * Do this in linear time and in-place.
 * 
 * For example, given the array ['G', 'B', 'R', 'R', 'B', 'R', 'G'], 
 * it should become ['R', 'R', 'R', 'G', 'G', 'B', 'B'].
 */

public class SegregateRGB {
	public static void main(String[] args) {
		char[] arr = { 'R', 'R', 'B', 'R', 'B', 'R', 'B' };

		segreg(arr);
		System.out.println(Arrays.toString(arr));
	}

	public static void segreg(char[] arr) {
		// Two integers used to keep track of where the 'R' and 'B' should go
		int x = 0, y = arr.length - 1;

		// We traverse the array
		for (int i = 0; i <= y; ++i) {
			// if the element is 'R', we "swap" it with the element at position x
			if (arr[i] == 'R') {
				arr[i] = arr[x];
				arr[x++] = 'R';
			}

			// if the element is 'B', we "swap" it with the element at position y
			if (arr[i] == 'B') {
				arr[i] = arr[y];
				arr[y--] = 'B';
				// we reduce i because we know that the position y, which starts at the end,
				// is now a 'B', where it should be, so there is no use checking that position
				// again
				i--;
			}
		}
	}
}
