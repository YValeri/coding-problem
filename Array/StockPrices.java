package Array;

/*
 * Given a array of numbers representing the stock prices of a company in chronological order, 
 * write a function that calculates the maximum profit you could have made from buying and selling that stock once. 
 * You must buy before you can sell it.
 * 
 * For example, given [9, 11, 8, 5, 7, 10], you should return 5, 
 * since you could buy the stock at 5 dollars and sell it at 10 dollars.
 */

public class StockPrices {
	public static void main(String[] args) {
		int[] tab = { 9, 11, 8, 5, 7, 10, 8, 7, 9, 7, 81, 4, 46, 148, 16, 114, 48, 96 };
		long lStartTime;
		long lEndTime;
		long output;
		int res;

		lStartTime = System.nanoTime();
		res = bestDifference(tab);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(res);

		lStartTime = System.nanoTime();
		res = maxDiff(tab);
		lEndTime = System.nanoTime();
		output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(res);
	}

	// Simple O(n^2) version, check each result and update a max
	public static int bestDifference(int[] tab) {
		int max = 0;
		for (int i = 0; i < tab.length; ++i) {
			for (int j = i + 1; j < tab.length; ++j) {
				if ((tab[j] - tab[i]) > max)
					max = tab[j] - tab[i];
			}
		}
		return max;
	}

	// A O(n) version, keeping track of the minimum element encountered and maximum
	// difference
	public static int maxDiff(int arr[]) {
		// Setup
		int max_diff = arr[1] - arr[0];
		int min_element = arr[0];
		int i;

		// Traverse the array
		for (i = 1; i < arr.length; i++) {
			// If the current element minus the minimum is higher than the current maximum
			// difference
			if (arr[i] - min_element > max_diff)
				// update it
				max_diff = arr[i] - min_element;

			// Update the minimum element encountered if possible
			if (arr[i] < min_element)
				min_element = arr[i];
		}

		return max_diff;
	}
}
