package Array;

/*
 * Given a list of numbers L, implement a method sum(i, j) 
 * which returns the sum from the sublist L[i:j] (including i, excluding j).
 * 
 * For example, given L = [1, 2, 3, 4, 5], sum(1, 3) should return sum([2, 3]), which is 5.
 * 
 * You can assume that you can do some pre-processing. 
 * sum() should be optimized over the pre-processing step.
 */

public class SubListSum {
	public static void main(String[] args) {
		int[] arr = {1, 2, 3, 4, 5};
		System.out.println(sum(arr, 1, 6));
	}
	
	public static int sum(int[] arr, int start, int end) {
		//If start >= end, swap the two index
		if(start >= end) {
			int temp = end;
			end = start;
			start = temp;
		}
		
		//Preprocess step, where we optimize our array to contain only the element we want to sum
		int[] temp = preprocess(arr, start, end);
		int res = 0;
		
		//We just traverse the preprocessed array, as it contains all the elements we want to sum
		for(int i = 0; i < temp.length; ++i)
			res += temp[i];
		return res;
	}
	
	//Preprocess function, where we create an array containing only the elements we want to sum, and return it
	public static int[] preprocess(int[] arr, int start, int end) {
		int[] temp = new int[end-start];
		if(end > arr.length)
			end = arr.length;
		for(int i = start; i < end; ++i)
			temp[i-start] = arr[i];
		return temp;
	}
}
