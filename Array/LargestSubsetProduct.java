package Array;

/*
 * Given a list of integers, return the largest product that can be made by multiplying any three integers.
 * 
 * For example, if the list is [-10, -10, 5, 2], we should return 500, since that's -10 * -10 * 5.
 * 
 * You can assume the list has at least three integers.
 */

public class LargestSubsetProduct {
	public static void main(String[] args) {
		Integer[] arr = { 1, -4, 3, -6, 7, 0 };
		int res = largestProduct(arr);
		System.out.println(res);
	}

	public static int largestProduct(Integer[] arr) {
		int max1 = Integer.MIN_VALUE, max2 = Integer.MIN_VALUE, max3 = Integer.MIN_VALUE, min1 = Integer.MAX_VALUE,
				min2 = Integer.MAX_VALUE;

		// We just go through the array, while keeping the 3 highest integers and the 2
		// lowest
		for (int i = 0; i < arr.length; ++i) {
			if (arr[i] > max1) {
				max3 = max2;
				max2 = max1;
				max1 = arr[i];
			} else if (arr[i] > max2) {
				max3 = max2;
				max2 = arr[i];
			} else if (arr[i] > max3) {
				max3 = arr[i];
			}

			if (arr[i] < min1) {
				min2 = min1;
				min1 = arr[i];
			} else if (arr[i] < min2) {
				min2 = arr[i];
			}
		}

		// We return the max between the multiplication of the 3 highest integers
		// And the 2 lowest * highest
		return Math.max(max1 * max2 * max3, min1 * min2 * max1);
	}
}
