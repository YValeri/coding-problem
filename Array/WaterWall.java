package Array;

/*
 * You are given an array of non-negative integers that represents a two-dimensional elevation map 
 * where each element is unit-width wall and the integer is the height. 
 * Suppose it will rain and all spots between two walls get filled up.
 * 
 * Compute how many units of water remain trapped on the map in O(N) time and O(1) space.
 * 
 * For example, given the input [2, 1, 2], we can hold 1 unit of water in the middle.
 * 
 * Given the input [3, 0, 1, 3, 0, 5], we can hold 3 units in the first index, 2 in the second, 
 * and 3 in the fourth index (we cannot hold 5 since it would run off to the left), so we can trap 8 units of water.
 */

public class WaterWall {
	public static void main(String[] args) {
		int[] tab = {3,0,1,3,0,5};
		
		long lStartTime = System.nanoTime();
		int res = trapWater(tab);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
	}
	
	public static int trapWater(int[] tab) {
		int left[] = new int[tab.length]; 
        int right[] = new int[tab.length]; 
        int res = 0; 
        left[0] = tab[0]; 
        for (int i = 1; i < tab.length; i++) 
           left[i] = Math.max(left[i-1], tab[i]); 
        right[tab.length-1] = tab[tab.length-1]; 
        for (int i = tab.length-2; i >= 0; i--) 
           right[i] = Math.max(right[i+1], tab[i]); 
        for (int i = 0; i < tab.length; i++) 
           res += Math.min(left[i],right[i]) - tab[i]; 
       
        return res; 
	}
}
