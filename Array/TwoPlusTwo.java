package Array;
import java.util.HashSet;

/*
 * Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
 * 
 * For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
 * 
 * Bonus: Can you do this in one pass?
 * 
 */

public class TwoPlusTwo {
	
	public static void main(String[] args) {
		int[] arr = {-18,15,35,7};
		int k = 17;
		
		
		//O(n�)
		long lStartTime = System.nanoTime();
		findDuo(arr, k);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        
        
        //O(n)
        lStartTime = System.nanoTime();
        findDuoN(arr, k);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
		
	}
	
	static void findDuo(int[] arr, int k){
		for(int i = 0; i < arr.length; i++) 
			for(int j = i+1; j < arr.length; j++) 
				if(arr[i] + arr[j] == k) 
					System.out.println("Found a combination : "+arr[i]+" + "+arr[j]+" = "+k+".");
		
		
	}
	
	/*
	1) Initialize an empty hash table s.
	2) Do following for each element arr[i] in arr[]
	   (a)    If s[k - A[i]] is set then print the pair (arr[i], k - arr[i])
	   (b)    Insert arr[i] into s.
	*/
	
	static void findDuoN(int arr[],int k){       
        HashSet<Integer> s = new HashSet<Integer>();
        for (int i = 0; i < arr.length; i++){
            int temp = k-arr[i];
 
            if (s.contains(temp))
                System.out.println("Pair with given sum " + k + " is (" + arr[i] + ", "+temp+")");
            
            s.add(arr[i]);
        }
    }
	
	
}
