package Array;

import java.util.Hashtable;

/*
 * Given a list of elements, find the majority element, which appears more than half the time (> floor(len(lst) / 2.0)).
 * 
 * You can assume that such element exists.
 * 
 * For example, given [1, 2, 1, 1, 3, 4, 0], return 1.
 */

public class MajorityElement {
	public static void main(String[] args) {
		int[] arr = {1, 2, 1, 1, 3, 4, 0,1,2};
		System.out.println(majority(arr));
	}
	
	public static int majority(int[] arr) {
		//Create an HashTable to keep the track of the number of occurrences of a special integer
		Hashtable<Integer, Integer> map = new Hashtable<>();
		
		//Traverse the array
		for(int i = 0; i < arr.length; ++i) {
			//Get the number of occurences of arr[i]
			Integer temp = map.get(arr[i]);
			//If it is not in the table, add it
			if(temp == null)
				map.put(arr[i], 1);
			else {
				//If the number of occurrences of arr[i]+1 is larger than floor(len(lst) / 2.0), return that number
				if((temp+1) >= (arr.length/2))
					return arr[i];
				else
					//Else increment of the number of occurrences of arr[i]
					map.replace(arr[i], temp+1);
			}
		}
		return -1;
	}
}
