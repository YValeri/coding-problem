package Array;

/*
 * Given an array of integers where every integer occurs three times 
 * except for one integer, which only occurs once, 
 * find and return the non-duplicated integer.
 * 
 * For example, given [6, 1, 3, 3, 3, 6, 6], return 1.
 * Given [13, 19, 13, 13], return 19.
 * 
 * Do this in O(N) time and O(1) space.
 */

public class TripletInArray {
	public static void main(String[] args) {
		int[] tab = {6,1,3,3,3,6,6};
		
		long lStartTime = System.nanoTime();
		int res = singleValue(tab, tab.length);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
	}
	
	static final int INT_SIZE = 32; 
    
    // Method to find the element that occur only once 
    static int singleValue(int arr[], int n) 
    { 
        int result = 0; 
        int x, sum; 
          
        // Iterate through every bit 
        for(int i=0; i<INT_SIZE; i++) 
        { 
            // Find sum of set bits at ith position in all  
            // array elements 
            sum = 0; 
            x = (1 << i); 
            for(int j=0; j<n; j++) 
                if((arr[j] & x) == 0) 
                	sum++; 
            // The bits with sum not multiple of 3, are the 
            // bits of element with single occurrence. 
            if ((sum % 3) == 0) 
            result |= x; 
        } 
        return result; 
    } 
}
