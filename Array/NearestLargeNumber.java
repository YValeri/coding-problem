package Array;

/*
 * Given an array of numbers and an index i, return the index of the nearest 
 * larger number of the number at index i, where distance is measured in array indices.
 * 
 * For example, given [4, 1, 3, 5, 6] and index 0, you should return 3.
 * 
 * If two distances to larger numbers are the equal, then return any one of them. 
 * If the array at i doesn't have a nearest larger integer, then return null.
 * 
 * Follow-up: If you can preprocess the array, can you do this in constant time?
 */

public class NearestLargeNumber {
	public static void main(String[] args) {
		int[] arr = { 4, 1, 3, 5, 6 };
		int n = 0;
		System.out.println(nearest(arr, n));
		System.out.println(preprocess(arr, n));
	}

	public static int nearest(int[] arr, int n) {
		// We keep the index and value of the current nearest largest number
		int max_index = 0, max_val = 0;

		// Traverse the array
		for (int i = 0; i < arr.length; ++i) {
			// Skip over n
			if (i == n)
				continue;

			// If the distance n to i is larger than the current max, update the two
			// integers
			if ((arr[i] - Math.abs(n - i)) > max_val) {
				max_index = i;
				max_val = arr[i] - Math.abs(n - i);
			}
		}
		// Return the max_index we got
		return max_index;
	}

	/*
	 * Since we can preprocess, we'll calculate the distances at each index and keep
	 * track of the max
	 */
	public static int preprocess(int[] arr, int n) {
		int max_index = 0;
		for (int i = 0; i < arr.length; ++i) {
			if (i == n)
				arr[i] = 0;
			else
				arr[i] = arr[i] - Math.abs(n - i);
			if (arr[max_index] > arr[i])
				max_index = i;
		}
		return nearest2(arr, max_index);
	}

	/*
	 * We preprocessed and changed the value of n, therefore we just return n, as it
	 * is our answer
	 */
	public static int nearest2(int[] arr, int n) {
		return n;
	}
}
