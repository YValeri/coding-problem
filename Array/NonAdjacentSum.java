package Array;

/* Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. 
 * Numbers can be 0 or negative.
 * 
 * For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. 
 * [5, 1, 1, 5] should return 10, since we pick 5 and 5.
 * 
 * Follow-up: Can you do this in O(N) time and constant space?
 */

public class NonAdjacentSum {
	public static void main(String[] args) {
		int[] arr = { -1, -1, 5, 10 };
		int n;

		long lStartTime = System.nanoTime();
		n = doubleForSum(arr);
		long lEndTime = System.nanoTime();
		long output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(n);
	}

	static int doubleForSum(int[] arr) {
		int mInclud = arr[0], mExclud = 0, temp = 0;

		// We traverse the whole array
		for (int i = 1; i < arr.length; i++) {
			temp = mInclud; // temp = previous max including arr[i]
			mInclud = mExclud + arr[i]; // mInclud = (previous max excluding arr[i-1]) + arr[i]
			mExclud = Math.max(mExclud, temp);
			// mExclud = max between the previous max excluding arr[i-1] and the previous
			// max including arr[i]
		}
		// Return the max between the max including and excluding arr[arr.length-1]
		return Math.max(mExclud, mInclud);
	}
}
