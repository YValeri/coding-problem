package Array;

import java.util.HashMap;

/*
 * Given an unsorted array of integers, find the length of the longest consecutive elements sequence.
 * 
 * For example, given [100, 4, 200, 1, 3, 2], 
 * the longest consecutive element sequence is [1, 2, 3, 4]. 
 * Return its length: 4.
 * 
 * Your algorithm should run in O(n) complexity.
 */

public class ConsecutiveInUnsorted {
	public static void main(String[] args) {
		int[] arr = { 100, 4, 200, 1, 3, 2 };
		int res = sequence(arr);
		System.out.println(res);
		int[] arr2 = { 1, 9, 3, 10, 4, 20, 2 };
		res = sequence(arr2);
		System.out.println(res);
		int[] arr3 = { 36, 41, 56, 35, 44, 33, 34, 92, 43, 32, 42 };
		res = sequence(arr3);
		System.out.println(res);
		int[] arr4 = { 1, 3, 5, 7, 9, 8, 6, 5, 4, 2 };
		res = sequence(arr4);
		System.out.println(res);
	}

	public static int sequence(int[] arr) {
		// We create a hashmap of integers
		HashMap<Integer, Integer> map = new HashMap<>();

		// We traverse the whole array, putting each values in the map
		for (int i = 0; i < arr.length; ++i)
			map.put(arr[i], arr[i]);
		int maxLength = 0, length = 0;

		// For each entries in the hashmap
		for (Integer i : map.keySet()) {

			// We check if a sequence exists by checking if an integer i is present the map
			while (map.get(i) != null) {
				// If it is, we check the next integer of the sequence
				i += 1;
				length += 1;
			}

			// length is used to keep track of the current length of the sequence checked
			// maxLength is the global maximum sequence length
			if (length > maxLength)
				maxLength = length;
			length = 0;
		}
		return maxLength;
	}
}
