package Array;

/*
 * Given a list of integers S and a target number k, 
 * write a function that returns a subset of S that adds up to k. 
 * If such a subset cannot be made, then return null.
 * 
 * Integers can appear more than once in the list. 
 * You may assume all numbers in the list are positive.
 * 
 * For example, given S = [12, 1, 61, 5, 9, 2] and k = 24, 
 * return [12, 9, 2, 1] since it sums up to 24.
 */

public class SubsetSumsUp {
	public static void main(String[] args) {
		int[] tab = { 12, 1, 61, 5, 9, 2 };

		long lStartTime = System.nanoTime();
		subValues(tab, 24, 0);
		long lEndTime = System.nanoTime();
		long output = lEndTime - lStartTime;
		System.out.println("\nElapsed time in milliseconds: " + output);

		// for(int i = 0; i < res.length; ++i)
		// System.out.print(res+",");
	}

	public static boolean subValues(int[] tab, int k, int start) {
		for (int i = start; i < tab.length; ++i) {
			if (tab[i] == k) {
				System.out.print(tab[i] + ",");
				return true;
			} else {
				if (subValues(tab, k - tab[i], i + 1)) {
					System.out.print(tab[i] + ",");
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isSubsetSum(int set[], int n, int sum) {
		// The value of subset[i][j] will be
		// true if there is a subset of
		// set[0..j-1] with sum equal to i
		boolean subset[][] = new boolean[sum + 1][n + 1];

		// If sum is 0, then answer is true
		for (int i = 0; i <= n; i++)
			subset[0][i] = true;

		// If sum is not 0 and set is empty,
		// then answer is false
		for (int i = 1; i <= sum; i++)
			subset[i][0] = false;

		// Fill the subset table in botton
		// up manner
		for (int i = 1; i <= sum; i++) {
			for (int j = 1; j <= n; j++) {
				subset[i][j] = subset[i][j - 1];
				if (i >= set[j - 1])
					subset[i][j] = subset[i][j] || subset[i - set[j - 1]][j - 1];
			}
		}

		// uncomment this code to print table
		/*
		 * for (int i = 0; i <= sum; i++) { for (int j = 0; j <= n; j++)
		 * System.out.print(subset[i][j]?1:0); System.out.println(); }
		 */

		return subset[sum][n];
	}
}
