package Array;

import java.util.ArrayList;
import java.util.Arrays;

public class PermutationLister {
	public static void main(String[] args) {
		int[] arr = { 0, 1, 2, 3, 3, 5, 6 };
		for (int[] t : allPermut(arr)) {
			System.out.println(Arrays.toString(t));
		}
		int[] arr2 = { 1, 2, 3 };
		for (int[] t : allPermut(arr2)) {
			System.out.println(Arrays.toString(t));
		}

		int[] arr3 = { 1, 3, 2 };
		for (int[] t : allPermut(arr3)) {
			System.out.println(Arrays.toString(t));
		}

		int[] arr4 = { 3, 2, 1 };
		for (int[] t : allPermut(arr4)) {
			System.out.println(Arrays.toString(t));
		}

		/*int[] arr5 = { 0, 1, 2, 5, 3, 3, 8 };
		for (int[] t : allPermut(arr5)) {
			System.out.println(Arrays.toString(t));
		}*/

	}

	/*
	 * Intermediary method that returns a list of the permutations possible
	 */
	public static ArrayList<int[]> allPermut(int[] arr) {
		ArrayList<int[]> res = new ArrayList<>();
		allPermut(arr, arr.length, arr.length, res);
		return res;

	}

	/*
	 * Recursive function that reduces the array to it's last element with each call
	 * and swap all the elements as we go up
	 */
	public static void allPermut(int[] arr, int size, int n, ArrayList<int[]> res) {
		if (size == 1)
			res.add(Arrays.copyOf(arr, arr.length));

		for (int i = 0; i < size; i++) {
			allPermut(arr, size - 1, n, res);

			if (size % 2 == 1) {
				int temp = arr[0];
				arr[0] = arr[size - 1];
				arr[size - 1] = temp;
			}

			else {
				int temp = arr[i];
				arr[i] = arr[size - 1];
				arr[size - 1] = temp;
			}
		}
	}
}
