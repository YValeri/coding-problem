package Array;

/*
 * An sorted array of integers was rotated an unknown number of times.
 * 
 * Given such an array, find the index of the element in the array in faster than linear time. 
 * If the element doesn't exist in the array, return null.
 * 
 * For example, given the array [13, 18, 25, 2, 8, 10] and the element 8, 
 * return 4 (the index of 8 in the array).
 * 
 * You can assume all the integers in the array are unique.
 */

public class FindElemRotatedArray {
	public static void main(String[] args) {
		int[] arr = { 13, 18, 25, 2, 8, 10 };

		long lStartTime = System.nanoTime();
		int n = search(arr, 0, arr.length - 1, 12);
		long lEndTime = System.nanoTime();
		long output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(n);
	}

	// We will use a dichotomic search in the the part of the array where the
	// integer should be
	public static int search(int arr[], int start, int end, int key) {
		// If start > end, we haven't find the integer, so it is not in the array
		if (start > end)
			return -1;

		// Get the middle element and return it if it is the key
		int mid = (start + end) / 2;
		if (arr[mid] == key)
			return mid;

		// If the array between start and mid is sorted
		if (arr[start] <= arr[mid]) {
			// And key is between arr[start] and start[mid]
			if (arr[start] <= key && key <= arr[mid])
				// We know the key is in this part of the array
				// So we continue the search between start and mid-1
				return search(arr, start, mid - 1, key);

			// If we get here, the integer if between arr[mid] and arr[end]
			// So we continue the search between mid+1 and end
			return search(arr, mid + 1, end, key);
		}

		// At this point, the array between start and mid isn't sorted
		// So if the part between mid and end contains the key
		// We continue the search in there
		if (arr[mid] <= key && key <= arr[end])
			return search(arr, mid + 1, end, key);

		// Else we know the key is in the lower part of the array,
		// So we continue the search there
		return search(arr, start, mid - 1, key);
	}
}
