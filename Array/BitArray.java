package Array;

/*
 * Implement a bit array.
 * 
 * A bit array is a space efficient array that holds a value of 1 or 0 at each index.

    init(size): initialize the array with size
    set(i, val): updates index at i with val where val is either 1 or 0.
    get(i): gets the value at index i.

 */

public class BitArray {
	public static void main(String[] args) {
		BitArray b = new BitArray(85);
		System.out.println(b.get(4));
		b.set(4, 1);
		System.out.println(b.get(4));
	}

	// Array of boolean values, with true corresponding to 1, and false to 0
	public boolean bits[];

	public BitArray(int size) {
		size = Math.abs(size);
		// Initialize the array with specific size
		bits = new boolean[size];
	}

	// Set a bit to val
	public void set(int i, int val) {
		bits[i] = (val == 1) ? true : false;
	}

	// Get a bit in the bitSet
	public long get(int i) {
		return bits[i] ? 1 : 0;
	}
}
