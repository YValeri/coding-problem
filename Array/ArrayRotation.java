package Array;

import java.util.Arrays;

/*
 * Write a function that rotates a list by k elements. 
 * For example, [1, 2, 3, 4, 5, 6] rotated by two becomes [3, 4, 5, 6, 1, 2]. 
 * Try solving this without creating a copy of the list. 
 * How many swap or move operations do you need?
 */

/*
 * We need exactly a * r rotations 
 * with a being arr.length and r being the number of rotations we want to do.
 */

public class ArrayRotation {
	public static void main(String[] args) {
		int[] arr = { 1, 2, 3, 4, 5, 6 };
		int rotation = 1;
		rotate(arr, rotation);
		System.out.println(Arrays.toString(arr));
	}

	public static void rotate(int[] arr, int rotation) {

		// We assure the rotation integer is valid, i.e is between 0 and arr.length
		rotation %= arr.length;

		// If there is no rotation to do, we just return
		if (rotation == 0)
			return;

		// To ensure all elements were rotated, we do GCD(arr.length, rotation) round of
		// rotation
		for (int i = 0; i < gcd(arr.length, rotation); ++i) {

			// We keep the first element of the rotation and start at index i
			int start = arr[i];
			int j = i;

			// We do all the necessary rotation in a round
			do {
				arr[j] = arr[(j + rotation) % arr.length];
				j = (j + rotation) % arr.length;
			} while ((j + rotation) % arr.length != i);

			// Set the former first element at the last rotated index
			arr[j] = start;
		}
	}

	public static int gcd(int a, int b) {
		if (b == 0)
			return a;
		else
			return gcd(b, a % b);
	}
}
