package Array;

/*
 * Given an array of integers, write a function to determine whether 
 * the array could become non-decreasing by modifying at most 1 element.
 * 
 * For example, given the array [10, 5, 7], you should return true, 
 * since we can modify the 10 into a 1 to make the array non-decreasing.
 * 
 * Given the array [10, 5, 1], you should return false, 
 * since we can't modify any one element to get a non-decreasing array.
 */

public class NonDecreasingArray {
	public static void main(String[] args) {
		int[] arr = { 10, 5, 7 };
		System.out.println(change(arr));
		int[] arr2 = { 10, 5, 1 };
		System.out.println(change(arr2));
		int[] arr3 = { 1, 12, 7 };
		System.out.println(change(arr3));
		int[] arr4 = { 10, 12, 1 };
		System.out.println(change(arr4));
	}

	public static boolean change(int[] arr) {
		// changed is a variable to know if we use our only chance to change a value
		boolean changed = true;

		// Traverse the array
		for (int i = 0; i < arr.length - 1; ++i) {
			// if the current value is greater than the following one
			if (arr[i] > arr[i + 1]) {
				// if we haven't changed an integer yet
				if (changed) {
					// change it to the following one
					arr[i] = arr[i + 1];
					// we go back to the previous value, to see if arr[i] > arr[i+1]
					i = (i == 0) ? i : i - 2;
					// we used our only chance of changing a value
					changed = false;
				} else
					// we couldn't change the array by modifying a single value, so return false
					return false;
			}
		}
		return true;
	}
}
