package Array;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

/*
 * Compute the running median of a sequence of numbers. 
 * That is, given a stream of numbers, print out the median of the list so far on each new element.
 * 
 * Recall that the median of an even-numbered list is the average of the two middle numbers.
 * 
 * For example, given the sequence [2, 1, 5, 7, 2, 0, 5], your algorithm should print out:

2
1.5
2
3.5
2
2
2

 */

public class StreamMedian {
	public static void main(String[] args) {
		int[] tab = { 2, 1, 5, 7, 2, 0, 5 };

		long lStartTime = System.nanoTime();
		double[] res = median(tab);
		long lEndTime = System.nanoTime();
		long output = lEndTime - lStartTime;
		System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(Arrays.toString(res));
	}

	static int cur = 1;

	public static double[] median(int[] tab) {
		// Setup the max length of the array
		int length = 100;

		// We create two arrays : sort will contain the sorted stream, and res the
		// medians
		int[] sort = new int[length + 1];
		double[] res = new double[length + 1];

		// Initialize the random
		Random r = new Random();

		// Initialize the two arrays with a random integer
		sort[0] = r.nextInt(length);
		res[0] = sort[0];

		// Create the random integer stream, where each new element is random
		Stream<Integer> integers = Stream.iterate(r.nextInt(length), i -> r.nextInt(length));
		integers.limit(length).forEach(i -> {
			System.out.print("i = " + i + ", " + Arrays.toString(sort));
			System.out.println();
			// insert the current element at the right place in the sorted array
			insert(sort, i);

			// l is actual length of the stream, as we just simulate one with an array
			int l = cur + 1;

			// Depending on of the parity of l, the median is either the middle element of
			// the sorted array, or the sum of the two middle values divided by 2
			if (l % 2 == 0)
				res[cur] = (sort[l / 2] + sort[l / 2 - 1]) / 2.0;
			else
				res[cur] = sort[l / 2];
			cur++;
		});

		return res;
	}

	/*
	 * Utility function to insert an element in a sorted array with contains mostly
	 * 0s at the start
	 */
	public static void insert(int[] tab, int value) {
		for (int i = 0; i < tab.length - 2; ++i) {
			if (tab[0] > value) {
				for (int j = tab.length - 2; j >= 0; j--)
					tab[j + 1] = tab[j];
				tab[0] = value;
				break;
			} else if (tab[i] <= value && tab[i + 1] == 0) {
				tab[i + 1] = value;
				break;
			} else if (tab[i] <= value && value < tab[i + 1]) {
				for (int j = tab.length - 2; j > i; j--)
					tab[j + 1] = tab[j];
				tab[i + 1] = value;
				break;
			}
		}
	}
}
