package Array;

import java.util.Arrays;

/*
 * Given an array of numbers representing the stock prices of a company in chronological order and an integer k, 
 * return the maximum profit you can make from k buys and sells. 
 * You must buy the stock before you can sell it, and you must sell the stock before you can buy it again.
 * 
 * For example, given k = 2 and the array [5, 2, 4, 0, 1], you should return 3.
 */

//Generalization of "StockPrices" problem, which is this problem with k fixed at 1

public class StockPricesKTimes {
	public static void main(String[] args) {
		int[] arr = { 5, 2, 4, 0, 1 };
		int k = 2;
		System.out.println(maxProfit(arr, k));
	}

	// Solution using dynamic programming
	public static int maxProfit(int[] price, int k) {
		// Create a matrix of the number of buy/sells possible * size of the array
		int profit[][] = new int[k + 1][price.length + 1];

		// Set the left column and top row at 0
		for (int i = 0; i <= k; i++)
			profit[i][0] = 0;
		for (int j = 0; j <= price.length; j++)
			profit[0][j] = 0;

		// We traverse the matrix using a double nested loop
		for (int i = 1; i <= k; i++) {
			// Set a minimum value
			int prevDiff = Integer.MIN_VALUE;
			for (int j = 1; j < price.length; j++) {
				// We change prevDiff to be the max between the previous one and (top left cell
				// of the matrix - price of the previous day)
				prevDiff = Math.max(prevDiff, profit[i - 1][j - 1] - price[j - 1]);

				// We change the current cell to be the math between the left cell and the
				// current price + previous difference
				profit[i][j] = Math.max(profit[i][j - 1], price[j] + prevDiff);
			}
		}
		for (int i = 0; i < k + 1; ++i)
			System.out.println(Arrays.toString(profit[i]));
		
		//The final profit we can make is located at the last row, penultimate column
		return profit[k][price.length - 1];
	}
}
