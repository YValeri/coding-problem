package Array;

/*
 * Given an array of integers in which two elements appear exactly once 
 * and all other elements appear exactly twice, find the two elements that appear only once.
 * 
 * For example, given the array [2, 4, 6, 8, 10, 2, 6, 10], return 4 and 8. 
 * The order does not matter.
 * 
 * Follow-up: Can you do this in linear time and constant space?
 */

public class AllTwiceButTwo {
	public static void main(String[] args) {
		int[] arr = { 2, 4, 6, 8, 10, 2, 6, 10 };
		twice(arr);
	}

	public static void twice(int[] arr) {
		int xor = 0;

		/*
		 * We do a XOR of all elements in arr Using properties of XOR, only the two
		 * elements appearing once remain in xor
		 */
		for (int i = 0; i < arr.length; ++i)
			xor = xor ^ arr[i];

		int x = 0, y = 0;
		int xor2 = 0, one = 1;

		/*
		 * We get the first bit in xor that is one, starting from the right Again, using
		 * properties of XOR, this bit is set in of the two elements and not set in the
		 * other
		 */
		for (;;) {
			if ((xor & one) > 0) {
				xor2 = one;
				break;
			}
			one <<= 1;
		}

		/*
		 * Now, we do an AND with each element of the array and the bit we got earlier
		 * If the result is positive, we do a XOR with x and that element Else, with y
		 * and that element In both cases, the elements that appear twice will cancel
		 * out and the two special elements will be in x and y
		 */
		for (int i = 0; i < arr.length; ++i) {
			if ((xor2 & arr[i]) > 0)
				x = x ^ arr[i];
			else
				y = y ^ arr[i];
		}

		System.out.println("x = " + x + ", y = " + y);
	}
}
