package Array;

import java.util.Arrays;

/*
 * Given a pivot x, and a list lst, partition the list into three parts.

    The first part contains all elements in lst that are less than x
    The second part contains all elements in lst that are equal to x
    The third part contains all elements in lst that are larger than x

 * Ordering within a part can be arbitrary.

 * For example, given x = 10 and lst = [9, 12, 3, 5, 14, 10, 10], one partition may be `[9, 3, 5, 10, 10, 12, 14].
 */

/*
 * This problem is very very similar to "SegregateRGB" where each element less to x is R, equal to x is G and the rest is B.
 * So the code doesn't change much, except for few conditions and that we have to swap elements.
 */

public class SegregateX {
	public static void main(String[] args) {
		int[] arr = { 9, 12, 3, 5, 14, 10, 10 };
		int x = 10;

		segreg(arr, x);
		System.out.println(Arrays.toString(arr));
	}

	public static void segreg(int[] arr, int n) {
		// Two integers used to keep track of where the elements less and larger
		// to x should go
		int x = 0, y = arr.length - 1;

		// We traverse the array
		for (int i = 0; i <= y; ++i) {
			int temp = arr[i];

			// if the element is < x, we "swap" it with the element at position x
			if (temp < n) {
				arr[i] = arr[x];
				arr[x++] = temp;
			}

			// if the element is > x, we "swap" it with the element at position y
			if (arr[i] > n) {
				arr[i] = arr[y];

				// we reduce y because we know that the position y, which starts at the end,
				// is now a 'B', where it should be, so there is no use checking that position
				// again
				arr[y--] = temp;

				// we reduce i because we don't which value was at the position y, since we
				// haven't checked that position yet
				i--;
			}
		}
	}
}
