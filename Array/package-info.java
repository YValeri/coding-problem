/**
 * This package is about manipulating arrays or using their properties to 
 * achieve certain goals.
 */
/**
 * @author YValeri
 *
 */
package Array;