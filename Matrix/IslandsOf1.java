package Matrix;

/*
 * Given a matrix of 1s and 0s, return the number of "islands" in the matrix. 
 * A 1 represents land and 0 represents water, 
 * so an island is a group of 1s that are neighboring and their perimeter is surrounded by water.
 * 
 * For example, this matrix has 4 islands.

1 0 0 0 0
0 0 1 1 0
0 1 1 0 0
0 0 0 0 0
1 1 0 0 1
1 1 0 0 1

 */

public class IslandsOf1 {
	public static void main(String[] args) {
		int[][] matrix = {{1, 0, 0, 0, 0, 1},
						{0, 0, 1, 1, 0, 0},
						{0, 1, 1, 0, 0, 1},
						{0, 0, 0, 0, 0, 0},
						{1, 1, 0, 0, 1, 0},
						{1, 1, 0, 0, 1, 0}};
		System.out.println(islands(matrix));
	}
	
	public static void showMatrix(int[][] matrix, boolean[][] marqued) {
		System.out.println("Marqued = ");
		for (int i = 0; i < marqued.length; i++) {
			for (int j = 0; j < marqued[0].length; j++) {
				System.out.print(((marqued[i][j])?1:0)+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public static int islands(int[][] matrix) {
		boolean[][] marqued = new boolean[matrix.length][matrix[0].length];
		int cpt = 0;
		for(int i = 0; i < matrix.length; ++i) {
			for (int j = 0; j < matrix[0].length; j++) {
				if(matrix[i][j] == 1 && marqued[i][j] == false) {
					marqued[i][j] = true;
					cpt++;
					mark(matrix, marqued, i, j);
				}
			}
		}
		return cpt;
	}
	
	public static void mark(int[][] matrix, boolean[][] marqued, int startI, int startJ) {
		if(startJ + 1 < matrix[0].length && matrix[startI][startJ + 1] == 1 && !marqued[startI][startJ + 1]) {
			marqued[startI][startJ + 1] = true;
			mark(matrix, marqued, startI, startJ + 1);
		}
		if(startI+1 < matrix.length) {
			for(int j = startJ-1; j <= startJ+1; ++j) {
				if(j < 0 || j >= matrix[0].length) 
					continue;
				
				if(matrix[startI+1][j] == 1 && !marqued[startI+1][j]) {
					marqued[startI+1][j] = true;
					mark(matrix, marqued, startI+1, j);
				}
			}
		}
	}
}
