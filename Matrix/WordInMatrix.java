package Matrix;

/*
 * Given a 2D matrix of characters and a target word, 
 * write a function that returns whether the word can be found in the matrix by going left-to-right, or up-to-down.
 * 
 * For example, given the following matrix:

[['F', 'A', 'C', 'I'],
 ['O', 'B', 'Q', 'P'],
 ['A', 'N', 'O', 'B'],
 ['M', 'A', 'S', 'S']]

 * and the target word 'FOAM', you should return true, since it's the leftmost column. 
 * Similarly, given the target word 'MASS', you should return true, since it's the last row.
 */

public class WordInMatrix {
	public static void main(String[] args) {
		char[][] matrix = {{'F', 'A', 'C', 'I'},
						{'O', 'B', 'Q', 'P'},
						{'A', 'N', 'O', 'B'},
						{'M', 'A', 'S', 'S'}};
		String s = "OBQP";
		System.out.println("Is the word \""+s+"\" in the matrix ? "+((find(matrix, s))?"Yes":"No"));
	}
	
	public static boolean find(char[][] matrix, String s) {
		for (int i = 0; i < matrix.length; i++) {
			boolean line = true, column = true;
			for (int j = 0; j < matrix[0].length; j++) {
				line = line && (s.charAt(j) == matrix[i][j]);
				column = column && (s.charAt(j) == matrix[j][i]);
				if(!(line || column))
					break;
			}
			if(line || column)
				return true;
		}
		return false;
	}
}
