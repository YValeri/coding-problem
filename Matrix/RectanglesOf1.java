package Matrix;

/*
 * Given an N by M matrix consisting only of 1's and 0's, 
 * find the largest rectangle containing only 1's and return its area.
 * 
 * For example, given the following matrix:

[[1, 0, 0, 0],
 [1, 0, 1, 1],
 [1, 0, 1, 1],
 [0, 1, 0, 0]]

 * Return 4.
 */

//This problem is very similar to the one I solved in "IslandsOf1"

public class RectanglesOf1 {
	public static void main(String[] args) {
		int[][] matrix = { { 0, 0, 0, 0, 1, 1 }, 
							{ 0, 1, 1, 1, 0, 0 }, 
							{ 0, 1, 1, 1, 0, 1 }, 
							{ 0, 0, 0, 0, 0, 0 },
							{ 1, 1, 0, 0, 1, 0 }, 
							{ 1, 1, 0, 0, 1, 0 } };
		System.out.println(rectangles(matrix));
	}

	public static void showMatrix(int[][] matrix, boolean[][] marqued) {
		System.out.println("Marqued = ");
		for (int i = 0; i < marqued.length; i++) {
			for (int j = 0; j < marqued[0].length; j++) {
				System.out.print(((marqued[i][j]) ? 1 : 0) + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	public static int rectangles(int[][] matrix) {
		// We create a matrix of boolean of the same length to know which cell have been
		// checked
		boolean[][] marqued = new boolean[matrix.length][matrix[0].length];
		int res = 0;

		// For all cell in matrix
		for (int i = 0; i < matrix.length; ++i) {
			for (int j = 0; j < matrix[0].length; j++) {
				// if the cell is 1 and isn't marqued
				if (matrix[i][j] == 1 && marqued[i][j] == false) {
					// mark it
					marqued[i][j] = true;

					// update the max encountered with the result of the mark method
					int temp = mark(matrix, marqued, i, j, 1);
					if (temp > res)
						res = temp;
				}
			}
		}
		return res;
	}

	// Recursive function that check the 4 surrounding cells of one specific cell
	// If one cell around a specific one is 1 and not marked, invoke the function
	// again with increased counter and the found cell
	public static int mark(int[][] matrix, boolean[][] marqued, int startI, int startJ, int cpt) {
		if (startJ + 1 < matrix[0].length && matrix[startI][startJ + 1] == 1 && !marqued[startI][startJ + 1]) {
			marqued[startI][startJ + 1] = true;
			return mark(matrix, marqued, startI, startJ + 1, cpt + 1);
		}
		if (startJ - 1 >= 0 && matrix[startI][startJ - 1] == 1 && !marqued[startI][startJ - 1]) {
			marqued[startI][startJ - 1] = true;
			return mark(matrix, marqued, startI, startJ - 1, cpt + 1);
		}
		if (startI + 1 < matrix.length && matrix[startI + 1][startJ] == 1 && !marqued[startI + 1][startJ]) {
			marqued[startI + 1][startJ] = true;
			return mark(matrix, marqued, startI + 1, startJ, cpt + 1);
		}
		if (startI - 1 >= 0 && matrix[startI - 1][startJ] == 1 && !marqued[startI - 1][startJ]) {
			marqued[startI - 1][startJ] = true;
			return mark(matrix, marqued, startI - 1, startJ, cpt + 1);
		}
		return cpt;
	}
}
