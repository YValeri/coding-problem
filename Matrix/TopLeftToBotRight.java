package Matrix;

/*
 * There is an N by M matrix of zeroes. 
 * Given N and M, write a function to count the number of ways of starting at the top-left corner 
 * and getting to the bottom-right corner. You can only move right or down.
 * 
 * For example, given a 2 by 2 matrix, you should return 2, since there are two ways to get to the bottom-right:

    Right, then down
    Down, then right

 * Given a 5 by 5 matrix, there are 70 ways to get to the bottom-right.
 */

public class TopLeftToBotRight {
	public static void main(String[] args) {
		int[][] matrix = new int[12][12];
		for(int i = 0; i < matrix.length; ++i) {
			for(int j = 0; j < matrix[0].length; ++j) {
				matrix[i][j] = 0;
			}
		}
		
		long lStartTime, lEndTime, output;
		int res;
		lStartTime = System.nanoTime();
		res = countWays(matrix);
	    lEndTime = System.nanoTime();
	    output = lEndTime - lStartTime;
	    System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println("To can go from top-left to bottom right in a "+matrix.length+"*"+matrix[0].length+" matrix in "+res+" moves.");
	}
	
	public static int countWays(int[][] matrix) {
		for(int i = 1; i < matrix.length; ++i) 
			matrix[0][i] = 1;
		
		for(int i = 1; i < matrix[0].length; ++i) 
			matrix[i][0] = 1;
		
		for(int i = 1; i < matrix.length; ++i) 
			for(int j = 1; j < matrix[0].length; ++j) 
				matrix[i][j] = matrix[i-1][j] + matrix[i][j-1];
			
		
		return matrix[matrix.length-1][matrix[0].length-1];
	}
}
