package Matrix;

/*
 * See "ClassroomInterval" problem
 */

public class Pair<K, V> {
	K first;
	V second;
	
	Pair(K first, V second){
		this.first = first;
		this.second = second;
	}
	
	public String toString() {
		return "("+first+","+second+")";
	}
}
