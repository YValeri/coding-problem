package Matrix;

/*
 * You are given an N by M matrix of 0s and 1s. 
 * Starting from the top left corner, how many ways are there to reach the bottom right corner?
 * 
 * You can only move right and down. 0 represents an empty space while 1 represents a wall you cannot walk through.
 * 
 * For example, given the following matrix:

[[0, 0, 1],
 [0, 0, 1],
 [1, 0, 0]]

 * Return two, as there are only two ways to get to the bottom right:

    Right, down, down, right
    Down, right, down, right

 * The top left corner and bottom right corner will always be 0.
 */

public class TopLeftToBotRightWithWalls {
	public static void main(String[] args) {
		int[][] matrix = {{0, 0, 1},
						  {0, 0, 1},
						  {1, 0, 0}};
		System.out.println(determine(matrix));
	}
	
	/*
	 * Similar to the "TopLeftToBotRight" problem, we create another matrix of the same length.
	 * For the first line and column, we put everything at 1 if the corresponding cell in matrix is 0,
	 * else, we leave it at 0. Then we traverse the matrix, and every cell is becomes the sum of the one 1 line up and 1 column left,
	 * only if the corresponding cell in matrix is 0.
	 * Then we return the bottom right cell value.
	 */
	public static int determine(int[][] matrix) {
		int[][] temp = new int[matrix.length][matrix[0].length];
		for(int i = 1; i < matrix.length; ++i) 
			if(matrix[0][i] == 0)
				temp[0][i] = 1;
		
		for(int i = 1; i < matrix[0].length; ++i) 
			if(matrix[i][0] == 0)
				temp[i][0] = 1;
		
		for(int i = 1; i < matrix.length; ++i) 
			for(int j = 1; j < matrix[0].length; ++j) 
				if(matrix[i][j] == 0)
					temp[i][j] = temp[i-1][j] + temp[i][j-1];
		
		return temp[temp.length-1][temp[0].length-1];
	}
}
