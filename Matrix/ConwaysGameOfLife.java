package Matrix;

/*
 * Conway's Game of Life takes place on an infinite two-dimensional board of square cells. 
 * Each cell is either dead or alive, and at each tick, the following rules apply:

    Any live cell with less than two live neighbours dies.
    Any live cell with two or three live neighbours remains living.
    Any live cell with more than three live neighbours dies.
    Any dead cell with exactly three live neighbours becomes a live cell.

 *A cell neighbours another cell if it is horizontally, vertically, or diagonally adjacent.
 *
 *Implement Conway's Game of Life. 
 *It should be able to be initialized with a starting list of live cell 
 *coordinates and the number of steps it should run for. 
 *Once initialized, it should print out the board state at each step.
 *
 *You can represent a live cell with an asterisk (*) and a dead cell with a dot (.).
 */

public class ConwaysGameOfLife {
	public static void main(String[] args) throws InterruptedException {
		boolean[][] tab = new boolean[15][15];
		tab[8][7] = true;
		tab[7][8] = true;
		tab[6][7] = true;
		tab[5][6] = true;
		tab[7][6] = true;
		print(tab);
		Thread.sleep(3000);
		
		while(true) {
			System.out.println();
			System.out.println();
			tab = step(tab);
			print(tab);
			Thread.sleep(3000);
		}
	}

	public static void print(boolean[][] tab) {
		for(int i = 0; i < tab.length; ++i) {
			for(int j = 0; j < tab[i].length; ++j)
				if(tab[i][j])
					System.out.print("*");
				else
					System.out.print(".");
			System.out.println();
		}
	}
	
	public static boolean[][] step(boolean[][] tab) {
		boolean[][] temp = new boolean[tab.length][tab[0].length];
		for(int i = 0; i < tab.length; ++i) 
			for(int j = 0; j < tab[i].length; ++j)
				temp[i][j] = determine(tab, i, j);
		
		return temp;
	}
	
	public static boolean determine(boolean[][] tab, int x, int y) {
		int livingCells = 0;
		for(int i = x-1; i <= x+1; ++i) {
			for(int j = y-1; j <= y+1; ++j) {
				if(i >= 0 && i < tab.length && j >= 0 && j < tab[0].length && i != x && j != y) {
					if(tab[i][j])
						livingCells++;
				}
			}
		}
		if(tab[x][y] == false && livingCells == 3)
			return true;
		else if(livingCells == 2 || livingCells == 3)
			return true;
		else
			return false;
	}
}
