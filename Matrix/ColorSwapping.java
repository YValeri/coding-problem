package Matrix;

import java.util.Arrays;

/*
 * Given a 2-D matrix representing an image, a location of a pixel in the screen and a color C, 
 * replace the color of the given pixel and all adjacent same colored pixels with C.
 * 
 * For example, given the following matrix, and location pixel of (2, 2), and 'G' for green:

B B W
W W W
W W W
B B B

 * Becomes

B B G
G G G
G G G
B B B

 */

public class ColorSwapping {
	public static void main(String[] args) {
		char[][] matrix = {{'B', 'B', 'W'},
							{'W', 'W', 'W'},
							{'W', 'W', 'W'}, 
							{'B', 'B', 'B'}};
		
		Pair<Integer, Integer> p = new Pair<>(4, 3);
		char c = 'C';
		swap(matrix, p, c);
		for(int i = 0; i < matrix.length; ++i)
			System.out.println(Arrays.toString(matrix[i]));
	}
	
	public static void swap(char[][] matrix, Pair<Integer, Integer> p, char c) {
		//The point's coordinates start a 1, so we just change them to fit the array model
		p.first--;
		p.second--;
		
		//We keep the color we want to change
		char toChange = matrix[p.first][p.second];
		
		//We check the 9 positions around the point given
		for(int i = p.first-1; i < p.first+2; ++i) 
			//if we are in an valid row
			if(0 <= i && i < matrix.length) 
				for(int j = p.second-1; j < p.second+2; ++j) 
					//if we are in an valid column
					if(0 <= j && j < matrix[0].length) 
						//if the pixel we check is of the color we want to change, do it
						if(toChange == matrix[i][j])
							matrix[i][j] = c;
					
				
			
		
	}
}
