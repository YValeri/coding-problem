package Matrix;

/*
 * Given a 2D board of characters and a word, find if the word exists in the grid.
 * 
 * The word can be constructed from letters of sequentially adjacent cell, 
 * where "adjacent" cells are those horizontally or vertically neighboring. 
 * The same letter cell may not be used more than once.
 * 
 * For example, given the following board:

[
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]

 * exists(board, "ABCCED") returns true, exists(board, "SEE") returns true, exists(board, "ABCB") returns false.
 */

public class WordMap {
	public static void main(String[] args) {
		char[][] board = {{'A','B','C','E'},
						{'S','F','C','S'},
						{'A','D','E','E'}};
		System.out.println(exists(board, "ABCCED"));
		System.out.println(exists(board, "SEE"));
		System.out.println(exists(board, "ABCB"));
		System.out.println(exists(board, "BFCEDASA"));
		System.out.println(exists(board, "BFCEDASF"));
	}
	
	public static boolean exists(char[][] board, String s) {
		for(int i = 0; i < board.length; ++i) {
			for(int j = 0; j < board[i].length; ++j) {
				if(board[i][j] == s.charAt(0)) {
					boolean[][] map = new boolean[board.length][board[i].length];
					map[i][j] = true;
					if(path(board, i, j, s, 1, map))
						return true;
				}
			}
		}
		return false;
	}

	public static boolean path(char[][] board, int i, int j, String s, int k, boolean[][] map) {
		System.out.println("i = "+i+", j = "+j+", k = "+k+", charAt = "+((k < s.length())?s.charAt(k):"fin"));
		if(k == s.length())
			return true;
		if(i-1 >= 0 && !map[i-1][j] && board[i-1][j] == s.charAt(k)){
			map[i-1][j] = true;
			if(path(board, i-1, j, s, k+1, map))
				return true;
		}
		if(i+1 < board.length && !map[i+1][j] && board[i+1][j] == s.charAt(k)){
			map[i+1][j] = true;
			if(path(board, i+1, j, s, k+1, map))
				return true;
		}
		if(j-1 >= 0 && !map[i][j-1] && board[i][j-1] == s.charAt(k)){
			map[i][j-1] = true;
			if(path(board, i, j-1, s, k+1, map))
				return true;
		}
		if(j+1 < board[i].length && !map[i][j+1] && board[i][j+1] == s.charAt(k)){
			map[i][j+1] = true;
			if(path(board, i, j+1, s, k+1, map))
				return true;
		}
		return false;
	}
}
