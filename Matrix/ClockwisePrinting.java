package Matrix;

/*
 * Given a N by M matrix of numbers, print out the matrix in a clockwise spiral.
 * 
 * For example, given the following matrix:

[[1,  2,  3,  4,  5],
 [6,  7,  8,  9,  10],
 [11, 12, 13, 14, 15],
 [16, 17, 18, 19, 20]]

 * You should print out the following:

1
2
3
4
5
10
15
20
19
18
17
16
11
6
7
8
9
14
13
12

 */

public class ClockwisePrinting {
	static int N, M, startN, startM;
	public static void main(String[] args) {
		int[][] matrix = {{1,  2,  3,  4,  5},
						{6,  7,  8,  9,  10},
						{11, 12, 13, 14, 15},
						{21, 22, 23, 24, 25},
						{16, 17, 18, 19, 20}};
		N = matrix.length;
		M = matrix[0].length;
		startN = 0;
		startM = 0;
		clockPrint(matrix);
	}
	
	public static void clockPrint(int[][] matrix) {
		lToR(matrix);
	}
	
	public static void lToR(int[][] matrix) {
		//System.out.println("lToR : ");
		//print();
		for(int i = startM; i < M; ++i) 
			System.out.println(matrix[startN][i]);
		startN++;
		if(startN >= N || startM >= M)
			return;
		TToB(matrix);
	}
	
	public static void TToB(int[][] matrix) {
		//System.out.println("TToB : ");
		//print();
		for(int i = startN; i < N; ++i) 
			System.out.println(matrix[i][M-1]);
		M--;
		if(startN >= N || startM >= M)
			return;
		RToL(matrix);
	}
	
	public static void RToL(int[][] matrix) {
		//System.out.println("RToL : ");
		//print();
		for(int i = M-1; i >= startM; --i) 
			System.out.println(matrix[N-1][i]);
		N--;
		if(startN >= N || startM >= M)
			return;
		BToT(matrix);
	}
	
	public static void BToT(int[][] matrix) {
		//System.out.println("BToT : ");
		//print();
		for(int i = N-1; i >= startN; --i) 
			System.out.println(matrix[i][startM]);
		startM++;
		if(startN >= N || startM >= M)
			return;
		lToR(matrix);
	}
	
	public static void print() {
		System.out.println("startM = "+startM);
		System.out.println("startN = "+startN);
		System.out.println("M = "+M);
		System.out.println("N = "+N);
	}
}
