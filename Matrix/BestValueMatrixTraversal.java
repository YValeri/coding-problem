package Matrix;

/*
 * You are given a 2-d matrix where each cell represents number of coins in that cell. 
 * Assuming we start at matrix[0][0], and can only move right or down, 
 * find the maximum number of coins you can collect by the bottom right corner.
 * 
 * For example, in this matrix

0 3 1 1
2 0 0 4
1 5 3 1

 * The most we can collect is 0 + 2 + 1 + 5 + 3 + 1 = 12 coins.
 */

public class BestValueMatrixTraversal {
	public static void main(String[] args) {
		int[][] matrix = { { 0, 3, 1, 1 }, { 2, 0, 5, 4 }, { 1, 5, 3, 1 } };
		System.out.println(bestValue(matrix));
	}

	public static int bestValue(int[][] matrix) {
		for(int i = 1; i < matrix.length; ++i)
			matrix[i][0] += matrix[i-1][0];
		for(int i = 1; i < matrix[0].length; ++i)
			matrix[0][i] += matrix[0][i-1];
		for(int i = 1; i < matrix.length; ++i) {
			for(int j = 1; j < matrix[0].length; ++j) {
				matrix[i][j] += Math.max(matrix[i-1][j], matrix[i][j-1]);
			}
		}
		return matrix[matrix.length-1][matrix[0].length-1];
	}
}
