package Tree;

/*
 * See "LockingTree" problem
 */

public class LockingNode {
	LockingNode left;
	LockingNode right;
	LockingNode parent;
	boolean lock;
	
	LockingNode(LockingNode left, LockingNode right){
		this.left = left;
		if(left != null) left.parent = this;
		this.right = right;
		if(right != null) right.parent = this;
		this.parent = null;
		lock = false;
	}
	
	LockingNode(LockingNode left, LockingNode right, LockingNode parent){
		this.left = left;
		if(left != null) left.parent = this;
		this.right = right;
		if(right != null) right.parent = this;
		this.parent = parent;
		lock = false;
	}
	
	boolean is_locked() {
		return lock;
	}
	
	boolean checkChange() {
		LockingNode n = this.parent;
		while(n != null) {
			if(n.lock)
				return false;
			n = n.parent;
		}
		if(left != null && right != null)
			return left.explore() & right.explore();
		if(right != null)
			return right.explore();
		if(left != null)
			return left.explore();
		return true;
	}
	
	boolean lock() {
		if(checkChange()) {
			lock = true;
			return true;
		}
		return false;
	}
	
	boolean explore() {
		if(lock)
			return false;
		if(left != null && right != null)
			return left.explore() & right.explore();
		if(right != null)
			return right.explore();
		if(left != null)
			return left.explore();
		return true;
	}
	
	boolean unlock() {
		if(checkChange() && lock) {
			lock = false;
			return true;
		}
		return false;
	}
}
