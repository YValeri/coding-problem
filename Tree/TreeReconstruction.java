package Tree;

/*
 * Given pre-order and in-order traversals of a binary tree, write a function to reconstruct the tree.
 * 
 * For example, given the following preorder traversal:
 * [a, b, d, e, c, f, g]
 * 
 * And the following inorder traversal:
 * [d, b, e, a, f, c, g]
 * 
 * You should return the following tree:

    a
   / \
  b   c
 / \ / \
d  e f  g

 */

public class TreeReconstruction {
	public static void main(String[] args) {
		String[] preorder = {"a", "b", "d", "e", "f"};
		String[] inorder = {"d", "b", "e", "a", "f"};

		print(reconstruct(preorder, inorder, 0));
	}
	
	public static Node reconstruct(String[] preorder, String[] inorder, int start) {
		if(inorder.length == 1)
			return new Node(inorder[0], null, null);
		else if(inorder.length == 0)
			return null;
		String root = preorder[start];
		int end = 0;
		for(int i = 0; i < inorder.length; ++i) {
			if(inorder[i].equals(root)) {
				end = i;
				break;
			}
		}
		if(end == 0)
			return new Node(inorder[0], null, null);
		String[] left = new String[end];
		String[] right = new String[inorder.length - end - 1];
		for(int i = 0; i < left.length; ++i) {
			left[i] = inorder[i];
		}
		for(int i = 0; i < right.length; ++i) {
			right[i] = inorder[i+end+1];
		}
		return new Node(inorder[end], reconstruct(preorder, left, start+1), reconstruct(preorder, right, end+1));
	}
	
	public static void print(Node n) {
		System.out.println(n.sValue);
		if(n.left != null)
			print(n.left);
		if(n.right != null)
			print(n.right);
	}
}
