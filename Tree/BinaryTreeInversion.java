package Tree;

/*
 * Invert a binary tree.
 * 
 * For example, given the following tree:

    a
   / \
  b   c
 / \  /
d   e f

 * should become:

  a
 / \
 c  b
 \  / \
  f e  d

 */

public class BinaryTreeInversion {
	public static void main(String[] args) {
		Node A = new Node("a", new Node("b", new Node("d", null, null), null), new Node("c", null, new Node("e", new Node("f", null, null), new Node("g", null, null))));
		A.print();
		inversion(A);
		System.out.println();
		A.print();
		
	}
	
	public static void inversion(Node root) {
		if(root == null)
			return;
		Node temp = root.left;
		root.left = root.right;
		root.right = temp;
		inversion(root.left);
		inversion(root.right);
	}
}
