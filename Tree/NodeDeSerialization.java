package Tree;

/* Given the root to a binary tree, implement serialize(root), which serializes the tree into a string,
 * and deserialize(s), which deserializes the string back into the tree.
 * 
 * For example, given the following Node class

class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
        
 * The following test should pass:
 * node = Node('root', Node('left', Node('left.left')), Node('right'))
 * assert deserialize(serialize(node)).left.left.val == 'left.left'
 */

public class NodeDeSerialization {
	public static void main(String[] args) {
		Node n = new Node("root", new Node("left", new Node("left.left", null, null), null), new Node("right", null, null));
		
		
		long lStartTime = System.nanoTime();
		String s = n.serialize(n);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(s);
        
        
		lStartTime = System.nanoTime();
		Node n2 = n.deserialize(s);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
		System.out.println(n.serialize(n2));
	}
}
