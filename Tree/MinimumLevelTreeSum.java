package Tree;

import java.util.LinkedList;
import java.util.Queue;

/*
 * Given a binary tree, return the level of the tree with minimum sum.
 */

public class MinimumLevelTreeSum {
	public static void main(String[] args) {
		Node root = new Node(15);

		root.right = new Node(2);
		root.left = new Node(3);

		root.right.right = new Node(4);
		root.right.left = new Node(5);
		root.left.right = new Node(6);
		root.left.left = new Node(7);

		root.left.left.right = new Node(8);
		root.left.left.left = new Node(9);
		
		System.out.println(minSum(root));
	}

	public static int minSum(Node root) {
		Queue<Node> q = new LinkedList<Node>();
		q.add(root);
		int sum, level = 0, minSum = Integer.MAX_VALUE, minLevel = -1;
		while (!q.isEmpty()) {
			int count = q.size();
			sum = 0;
			while (count-- != 0) {
				Node temp = q.remove();
				sum += temp.iValue;
				if (temp.left != null)
					q.add(temp.left);
				if (temp.right != null)
					q.add(temp.right);
			}
			if (sum < minSum) {
				minSum = sum;
				minLevel = level;
			}
			level++;
		}
		return minLevel;
	}
}
