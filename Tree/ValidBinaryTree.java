package Tree;

/*
 * Determine whether a tree is a valid binary search tree.
 * 
 * A binary search tree is a tree with two children, left and right, 
 * and satisfies the constraint that the key in the left child must be less 
 * than or equal to the root and the key in the right child must be greater than or equal to the root.
 */

public class ValidBinaryTree {
	public static void main(String[] args) {
		Node A = new Node(6, new Node(4, new Node(2, null, null), null), new Node(8, new Node(5, null, null), new Node(11, new Node(9, null, null), new Node(13, null, null))));
		System.out.println(valid(A));
	}
	
	public static boolean valid(Node root) {
		if(root.left != null && root.right != null) {
			if(root.left.iValue > root.iValue || root.right.iValue < root.iValue)
				return false;
			return validL(root.left, root.iValue) && validR(root.right, root.iValue);
		}else if(root.right != null) {
			if(root.right.iValue < root.iValue)
				return false;
			return validR(root.right, root.iValue);
		}else if(root.left != null) {
			if(root.left.iValue > root.iValue)
				return false;
			return validL(root.left, root.iValue);
		}else
			return true;
	}
	
	public static boolean validL(Node root, int max) {
		if(root.left != null && root.right != null) {
			if(root.left.iValue > root.iValue || root.right.iValue < root.iValue || root.right.iValue > max)
				return false;
			return validL(root.left, root.iValue) && validR(root.right, root.iValue);
		}else if(root.right != null) {
			if(root.right.iValue < root.iValue || root.right.iValue > max)
				return false;
			return validR(root.right, root.iValue);
		}else if(root.left != null) {
			if(root.left.iValue > root.iValue)
				return false;
			return validL(root.left, root.iValue);
		}else
			return true;
	}
	
	public static boolean validR(Node root, int min) {
		if(root.left != null && root.right != null) {
			if(root.left.iValue > root.iValue || root.right.iValue < root.iValue || root.left.iValue < min)
				return false;
			return validL(root.left, root.iValue) && validR(root.right, root.iValue);
		}else if(root.right != null) {
			if(root.right.iValue < root.iValue)
				return false;
			return validR(root.right, root.iValue);
		}else if(root.left != null) {
			if(root.left.iValue > root.iValue || root.left.iValue < min)
				return false;
			return validL(root.left, root.iValue);
		}else
			return true;
	}
}
