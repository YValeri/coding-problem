package Tree;

/*
 * Implement a Locking Tree class.
 * You can only (un)lock a node only if all of it's parent or child are not locked
 */

public class LockingTree {
	public static void main(String[] args) {
		LockingNode n = new LockingNode(null, null);
		n.left = new LockingNode(null, null, n);
		n.right = new LockingNode(null, null, n);
		n.left.left = new LockingNode(null, null, n.left);
		n.left.right = new LockingNode(null, null, n.left);
		n.right.right = new LockingNode(null, null, n.right);
		
		long lStartTime = System.nanoTime();
		boolean locked = n.left.is_locked();
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("first is locked = " + locked);
        
        lStartTime = System.nanoTime();
		locked = n.left.lock();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("first lock = "+locked);
        
        lStartTime = System.nanoTime();
		locked = n.left.is_locked();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("second is locked = " + locked);
        
        lStartTime = System.nanoTime();
		locked = n.lock();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("second lock = "+locked);
        
        lStartTime = System.nanoTime();
		locked = n.left.unlock();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("first unlock = "+locked);
        
        lStartTime = System.nanoTime();
		locked = n.left.is_locked();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("third is locked = " + locked);
        
        lStartTime = System.nanoTime();
		locked = n.lock();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("third lock = "+locked);
        
        lStartTime = System.nanoTime();
		locked = n.left.lock();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("fourth lock = "+locked);
        
        lStartTime = System.nanoTime();
		locked = n.unlock();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("second unlock = "+locked);
	}
}
