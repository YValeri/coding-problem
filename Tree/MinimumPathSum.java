package Tree;

import java.util.ArrayList;

/*
 * Given a binary tree, find a minimum path sum from root to a leaf.
 * 
 * For example, the minimum path in this tree is [10, 5, 1, -1], which has sum 15.

	  10
	 /  \
	5    5
	 \     \
	   2    1
	       /
	     -1

 */

public class MinimumPathSum {
	public static void main(String[] args) {
		Node root = new Node(10);
		
		root.left = new Node(5);
		root.left.right = new Node(-3);
		root.left.left = new Node(6);
		
		root.right = new Node(5);
		root.right.right = new Node(1);
		root.right.right.left = new Node(-1);
		root.right.right.right = new Node(0);
		
		System.out.println(minPath(root));
	}
	
	public static ArrayList<Node> minPath(Node root){
		if(root == null)
			return null;
		return minPathInter(root, new ArrayList<Node>());
	}
	
	public static ArrayList<Node> minPathInter(Node root, ArrayList<Node> l){
		l.add(root);
		if(root.left == null && root.right == null) 
			return l;
		else if(root.left != null && root.right == null) 
			return minPathInter(root.left, l);
		else if(root.left == null && root.right != null)
			return minPathInter(root.right, l);
		else {
			ArrayList<Node> l1 = minPathInter(root.left, new ArrayList<Node>(l));
			ArrayList<Node> l2 = minPathInter(root.right, new ArrayList<Node>(l));
			
			if(sum(l1) <= sum(l2)) 
				return l1;
			
			return l2;
		}
	}
	
	public static int sum(ArrayList<Node> l) {
		int res = 0;
		for(Node n : l)
			res += n.iValue;
		return res;
	}
}
