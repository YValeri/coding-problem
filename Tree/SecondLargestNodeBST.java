package Tree;

/*
 * Given the root to a binary search tree, find the second largest node in the tree.
 */

public class SecondLargestNodeBST {
	public static void main(String[] args) {
		BinarySearchTree tree = new BinarySearchTree(10);
		tree.insert(5);
		tree.print();
		
		long lStartTime = System.nanoTime();
		int res = secondBiggestValue(tree);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
		
		tree.insert(20);
		tree.insert(30);
		
		lStartTime = System.nanoTime();
		res = secondBiggestValue(tree);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
        
		tree.insert(25);
		tree.insert(28);
		tree.insert(27);
		tree.insert(29);
		
		lStartTime = System.nanoTime();
		res = secondBiggestValue(tree);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
	}
	
	public static int secondBiggestValue(BinarySearchTree root) {
		BinarySearchTree temp = root;
		while(temp.right != null) {
			root = temp;
			temp = temp.right;
		}
		if(temp.left != null) {
			temp = temp.left;
			while(temp.right != null) 
				temp = temp.right;
			return temp.value;
		}else 
			return root.value;
		
	}
}
