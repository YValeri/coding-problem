package Tree;

/*
 * See "SecondLargestNodeBST" problem
 */

public class BinarySearchTree {
	int value;
	BinarySearchTree left, right;
	
	public BinarySearchTree(int value) {
		this.value = value;
		left = null;
		right = null;
	}
	
	public void insert(int value) {
		if(value > this.value) {
			if(right == null)
				right = new BinarySearchTree(value);
			else
				right.insert(value);
		}else if(value < this.value){
			if(left == null)
				left = new BinarySearchTree(value);
			else
				left.insert(value);
		}else{
			System.out.println("You cannot add a value already existing in the tree.");
		}
	}
	
	public void print() {
		System.out.println(this.value + ", ");
		if(left != null)
			left.print();
		if(right != null)
			right.print();
	}
}
