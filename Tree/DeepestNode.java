package Tree;

/*
 * Given the root of a binary tree, return a deepest node. 
 * For example, in the following tree, return d.

    a
   / \
  b   c
 /	   \
d		e
	   / \
	  f   g

 */

public class DeepestNode {
	public static void main(String[] args) {
		Node A = new Node("a", new Node("b", new Node("d", null, null), null), new Node("c", null, new Node("e", new Node("f", null, null), new Node("g", null, null))));
		System.out.println(deepest(A).sValue);
	}
	
	public static Node deepest(Node root) {
		return check(root).second;
	}
	
	public static Pair<Integer, Node> check(Node root) {
		if(root == null)
			return new Pair<Integer, Node>(0, null);
		else {
			Pair<Integer, Node> left = check(root.left);
			Pair<Integer, Node> right = check(root.right);
			if(left.first > right.first)
				return new Pair<Integer, Node>(left.first+1, (left.second == null)?root:left.second);
			else
				return new Pair<Integer, Node>(right.first+1, (right.second == null)?root:right.second);
		}
	}
}
