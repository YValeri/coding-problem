package Tree;

/*
 * Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree. 
 * Assume that each node in the tree also has a pointer to its parent.
 * 
 * According to the definition of LCA on Wikipedia: 
 * “The lowest common ancestor is defined between two nodes v and w as the lowest node in T 
 * that has both v and w as descendants (where we allow a node to be a descendant of itself).”
 */

public class LowestCommonAncestor {
	static boolean v1;
	static boolean v2;

	public static void main(String[] args) {
		Node root = new Node(1);
		root.left = new Node(2);
		root.right = new Node(3);
		root.left.left = new Node(4);
		root.left.right = new Node(5);
		root.right.left = new Node(6);
		root.right.right = new Node(7);

		System.out.println(LCA(root, 4, 5));
		System.out.println(LCA(root, 4, 7));
		System.out.println(LCA(root, 5, 2));
		System.out.println(LCA(root, 2, 7));
		System.out.println(LCA(root, 2, 8));
	}

	public static Node LCA(Node root, int i, int j) {
		v1 = false;
		v2 = false;
		Node res = LCA2(root, i, j);
		if (v1 && v2)
			return res;
		return null;
	}

	public static Node LCA2(Node root, int i, int j) {
		if (root == null)
			return null;

		Node temp = null;
		if (root.iValue == i) {
			v1 = true;
			temp = root;
		} else if (root.iValue == j) {
			v2 = true;
			temp = root;
		}

		Node left = LCA2(root.left, i, j);
		Node right = LCA2(root.right, i, j);

		if (temp != null)
			return temp;

		if (left != null && right != null)
			return root;

		return (left != null) ? left : right;
	}
}
