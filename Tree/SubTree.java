package Tree;

/*
 * Given two non-empty binary trees s and t, check whether tree t has exactly the same structure and 
 * node values with a subtree of s. A subtree of s is a tree consists of a node in s and all of this node's descendants. 
 * The tree s could also be considered as a subtree of itself.
 */

public class SubTree {
	public static void main(String[] args) {
		Node root = new Node(1);
		root.left = new Node(2);
		root.right = new Node(3);
		root.right.left = new Node(4);
		root.right.right = new Node(5);
		root.right.left.left = new Node(6);
		root.right.left.right = new Node(7);
		Node t = new Node(4);
		t.left = new Node(6);
		t.right = new Node(9);
		
		System.out.println(subtree(root, root.right.left.left));
	}
	
	public static boolean subtree(Node s, Node t) {
		if(s.iValue == t.iValue)
			if(equals(s, t))
				return true;
		if(s.left == null && s.right == null)
			return false;
		else if(s.left != null && s.right == null)
			return subtree(s.left, t);
		else if(s.left == null && s.right != null)
			return subtree(s.right, t);
		else return subtree(s.left, t) || subtree(s.right, t);
	}
	
	public static boolean equals(Node s, Node t) {
		if(s.iValue != t.iValue)
			return false;
		if((s.left != null && t.left == null) || (s.left == null && t.left != null))
			return false;
		if((s.right != null && t.right == null) || (s.right == null && t.right != null))
			return false;
		else {
			if(s.left == null && s.right == null)
				return true;
			if(s.left != null && s.right != null)
				return equals(s.left, t.left) && equals(s.right, t.right);
			if(s.left != null)
				return equals(s.left, t.left);
			else return equals(s.right, t.right);
		}
		
	}
}
