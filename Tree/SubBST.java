package Tree;

/*
 * Given a tree, find the largest tree/subtree that is a BST.
 * 
 * Given a tree, return the size of the largest tree/subtree that is a BST.
 */

public class SubBST {
	public static void main(String[] args) {
		Node A = new Node(6, new Node(7, new Node(2, new Node(1), new Node(4, new Node(3), new Node(5))), null), new Node(8, new Node(51), new Node(11, new Node(9), new Node(13))));
		if(A != null)
			check(A).print();
	}
	
	public static int size(Node root) {
		if(root.left != null && root.right != null) 
			return Math.max(size(root.left), size(root.right)) + 1;
		else if(root.left != null)
			return size(root.left) +1;
		else if(root.right != null)
			return size(root.right) +1;
		else return 1;
	}
	
	public static Node check(Node root) {
		if(root.left != null && root.right != null) {
			Node tempL = check(root.left);
			Node tempR = check(root.right);
			if(tempL != null && tempR != null)
				if(root.left.iValue <= root.iValue && root.right.iValue >= root.iValue) {
					return root;
				}else {
					int iL = size(tempL);
					int iR = size(tempR);
					if(iL >= iR)
						return tempL;
					return tempR;
				}
			else if(tempL != null)
				return tempL;
			else if(tempR != null)
				return tempR;
			else
				return null;
		}else if(root.left != null) {
			Node tempL = check(root.left);
			if(tempL == null)
				return null;
			else if(root.left.iValue <= root.iValue)
				return root;
		}else if(root.right != null) {
			Node tempR = check(root.right);
			if(tempR == null)
				return null;
			else if(root.right.iValue >= root.iValue)
				return root;
		}
		return root;
	}
}
