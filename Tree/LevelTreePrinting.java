package Tree;

import java.util.LinkedList;
import java.util.Queue;

/*
 * Print the nodes in a binary tree level-wise. For example, the following should print 1, 2, 3, 4, 5.

  1
 / \
2   3
   / \
  4   5

 */

public class LevelTreePrinting {
	public static void main(String[] args) {
		Node root = new Node(1);
		root.right = new Node(2);
		root.left = new Node(3);
		root.right.right = new Node(4);
		root.right.left = new Node(5);
		root.left.right = new Node(6);
		root.left.left = new Node(7);
		root.left.left.right = new Node(8);
		root.left.left.left = new Node(9);
		
		levelPrint(root);
	}
	
	public static void levelPrint(Node root) {
		Queue<Node> q = new LinkedList<Node>(); 
		q.add(root);
		while(!q.isEmpty()) {
			Node n = q.remove();
			System.out.println(n.iValue);
			if(n.left != null)
				q.add(n.left);
			if(n.right != null)
				q.add(n.right);
		}
	}
}
