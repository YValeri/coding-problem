package Tree;

import java.util.ArrayList;

/*
 * Given a binary tree, return all paths from the root to leaves.
 * 
 * For example, given the tree:

   1
  / \
 2   3
    / \
   4   5

 * it should return [[1, 2], [1, 3, 4], [1, 3, 5]].
 */

public class PathToLeaves {
	public static void main(String[] args) {
		Node root = new Node(1);
		root.left = new Node(2);
		root.right = new Node(3);
		root.right.left = new Node(4);
		root.right.right = new Node(5);
		root.right.left.left = new Node(6);
		root.right.left.right = new Node(7);
		
		System.out.println(paths(root));
	}
	
	public static ArrayList<ArrayList<Node>> paths(Node root){
		ArrayList<ArrayList<Node>> res = new ArrayList<>();
		paths(root, res, new ArrayList<>());
		return res;
	}

	public static void paths(Node root, ArrayList<ArrayList<Node>> res, ArrayList<Node> cur){
		cur.add(root);
		if(root.left == null && root.right == null)
			res.add(cur);
		if(root.left != null) {
			ArrayList<Node> l = new ArrayList<>(cur);
			paths(root.left, res, l);
		}
		if(root.right != null) {
			ArrayList<Node> r = new ArrayList<>(cur);
			paths(root.right, res, r);
		}
	}
}
