package Tree;

import java.util.ArrayList;
import java.util.Iterator;

/*
 * Given the root of a binary search tree, and a target K, return two nodes in the tree whose sum equals K.
 * 
 * For example, given the following tree and K of 20 :

    10
   /   \
 5      15
       /  \
     11    15

 * Return the nodes 5 and 15.
 */

public class NodeSum {
	public static void main(String[] args) {
		Node root = new Node(15);

		root.right = new Node(20);
		root.left = new Node(8);

		root.right.right = new Node(22);
		root.right.left = new Node(17);
		root.left.right = new Node(9);
		root.left.left = new Node(5);

		root.left.left.right = new Node(6);
		root.left.left.left = new Node(-2);
		
		Pair<Node, Node> nodes = sum(root, 21);
		
		if(nodes != null) {
			System.out.println(nodes.first);
			System.out.println(nodes.second);
		}else
			System.out.println("No pair of nodes which sum equals k found.");
	}
	
	public static Pair<Node, Node> sum(Node root, int k){
		ArrayList<Node> l1 = new ArrayList<>();
		fill(l1, root, true);
		ArrayList<Node> l2 = new ArrayList<>();
		fill(l2, root, false);
		for (Iterator<Node> iterator1 = l1.iterator(), iterator2 = l2.iterator(); iterator1.hasNext() && iterator2.hasNext();) {
			Node n1 = (Node) iterator1.next();
			Node n2 = (Node) iterator2.next();
			if((n1.iValue + n2.iValue) == k)
				return new Pair<Node, Node>(n1, n2);
			
		}
		return null;
	}

	public static void fill(ArrayList<Node> l, Node root, boolean inorder) {
		if(inorder) {
			if(root.left != null)
				fill(l, root.left, inorder);
		}else if(root.right != null)
				fill(l, root.right, inorder);
		l.add(root);
		if(inorder) {
			if(root.right != null)
				fill(l, root.right, inorder);
		}else if(root.left != null)
			fill(l, root.left, inorder);
	}
	
}
