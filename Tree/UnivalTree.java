package Tree;

/*A unival tree (which stands for "universal value") is a tree where all nodes under it have the same value.
 * 
 * Given the root to a binary tree, count the number of unival subtrees.
 * 
 * For example, the following tree has 5 unival subtrees:

   0
  / \
 1   0
    / \
   1   0
  / \
 1   1

 */

public class UnivalTree {
	public static void main(String[] args) {
		Node n = new Node(5, new Node(4, new Node(4, null, null), new Node(4, null, null)), new Node(5, null, new Node(5, null, null)));
		
		//O(n�)
		long lStartTime = System.nanoTime();
		System.out.println(n.nUnivalSubTrees(n));
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        
        //O(n)
		lStartTime = System.nanoTime();
		System.out.println(n.countSingle(n));
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
	}
}
