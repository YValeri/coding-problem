package Tree;

/*
 * Given a binary tree where all nodes are either 0 or 1, prune the tree so that subtrees containing all 0s are removed.
 * 
 * For example, given the following tree:

   0
  / \
 1   0
    / \
   1   0
  / \
 0   0

 * should be pruned to:

   0
  / \
 1   0
    /
   1

 * We do not remove the tree at the root or its left child because it still has a 1 as a descendant.
 */

public class Pruning0s {
	public static void main(String[] args) {
		Node root = new Node(0);
		root.left = new Node(1);
		root.right = new Node(0);
		root.right.left = new Node(1);
		root.right.right = new Node(0);
		root.right.left.left = new Node(0);
		root.right.left.right = new Node(0);
		root.right.right.right = new Node(1);
		
		root.print();
		prune(root);
		System.out.println();
		root.print();
	}
	
	public static boolean prune(Node root) {
		//If root is null, we can safely prune it's parent if possible
		if(root == null)
			return true;
		
		//If the left branch is prunable, then we cut it
		if(prune(root.left))
			root.left = null;
		
		//If the right branch is prunable, then we cut it
		if(prune(root.right))
			root.right = null;
		
		//Finally, if the current node is 0 and its left and right branches are null, we return true
		//effectively saying that we can cut the branch we're on
		if(root.iValue == 0 && root.left == null && root.right == null)
			return true;
		else //If at least one of these conditions is not met, we can't cut the branch
			return false;
	}
}
