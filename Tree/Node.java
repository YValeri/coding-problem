package Tree;

/*
 * See "NodeDeSerialization", "UnivalTree", "ArithmeticExpressionEvaluation" problems
 */

public class Node {
	String sValue;
	Integer iValue;
	Node left;
	Node right;
	Node rnd;
	int count = 0;

	Node() {
		this.iValue = null;
		this.left = null;
		this.right = null;
		this.rnd = null;
		this.sValue = "";
	}

	Node(int val) {
		this.iValue = val;
		this.left = null;
		this.right = null;
		this.rnd = null;
		this.sValue = "";
	}

	Node(String s) {
		this.iValue = 0;
		this.left = null;
		this.right = null;
		this.rnd = null;
		this.sValue = s;
	}

	Node(String val, Node left, Node right) {
		this.sValue = val;
		this.left = left;
		this.right = right;
		this.iValue = 0;
		this.rnd = null;
	}

	Node(int val, Node left, Node right) {
		this.iValue = val;
		this.left = left;
		this.right = right;
		this.sValue = "";
		this.rnd = null;
	}

	public void print() {
		if (sValue.equals(""))
			System.out.println(iValue);
		else
			System.out.println(sValue);
		if (left != null)
			left.print();
		if (right != null)
			right.print();
	}

	String serialize(Node n) {
		if (n != null)
			return n.sValue + "(" + serialize(n.left) + "," + serialize(n.right) + ")";
		return "";
	}

	Node deserialize(String s) {
		if (s.equals(""))
			return null;
		String name = s.substring(0, s.indexOf('('));
		String next = s.substring(s.indexOf('(') + 1, s.lastIndexOf(')'));
		boolean premierPassage = true;
		int total = 0;
		int middle = -1;
		for (int i = 0; middle == -1 && i < next.length(); i++) {
			// System.out.println(i);
			if (next.charAt(i) == '(') {
				premierPassage = false;
				total++;
			} else if (next.charAt(i) == ')')
				total--;
			if (total == 0 && !premierPassage)
				middle = i + 1;
		}
		String left, right;
		if (middle == -1) {
			left = "";
			right = next.substring(next.indexOf(',') + 1);
		} else {
			left = next.substring(0, middle);
			right = next.substring(middle + 1);
		}
		return new Node(name, deserialize(left), deserialize(right));
	}

	int nUnivalSubTrees(Node n) {
		if (n.left == null && n.right == null)
			return 1;
		else if (n.left == null) {
			if (n.right.iValue == n.iValue)
				return nUnivalSubTrees(n.right) + 1;
			else
				return nUnivalSubTrees(n.right);
		} else if (n.right == null) {
			if (n.left.iValue == n.iValue)
				return nUnivalSubTrees(n.left) + 1;
			else
				return nUnivalSubTrees(n.left);
		} else if (n.right.iValue == n.iValue && n.left.iValue == n.iValue)
			return nUnivalSubTrees(n.left) + nUnivalSubTrees(n.right) + 1;
		else
			return nUnivalSubTrees(n.left) + nUnivalSubTrees(n.right);
	}

	boolean countSingleRec(Node node) {
		// Return false to indicate NULL
		if (node == null)
			return true;

		// Recursively count in left and right subtrees also
		boolean left = countSingleRec(node.left);
		boolean right = countSingleRec(node.right);

		// If any of the subtrees is not singly, then this
		// cannot be singly.
		if (left == false || right == false)
			return false;

		// If left subtree is singly and non-empty, but data
		// doesn't match
		if (node.left != null && node.iValue != node.left.iValue)
			return false;

		// Same for right subtree
		if (node.right != null && node.iValue != node.right.iValue)
			return false;

		// If none of the above conditions is true, then
		// tree rooted under root is single valued, increment
		// count and return true.
		count++;
		return true;
	}

	int countSingle(Node node) {
		// Recursive function to count
		countSingleRec(node);
		return count;
	}

	public float arithEval() throws Exception {
		if (left == null && right == null) {
			System.out.print(iValue + " ");
			return (float) iValue;
		} else {
			float res;
			System.out.print("( ");
			res = left.arithEval();
			switch (sValue) {
			case "+":
				System.out.print("+ ");
				res += right.arithEval();
				break;
			case "-":
				System.out.print("- ");
				res -= right.arithEval();
				break;
			case "/":
				System.out.print("/ ");
				res /= right.arithEval();
				break;
			case "*":
				System.out.print("* ");
				res *= right.arithEval();
				break;
			default:
				throw new Exception("Wrong symbol");
			}
			System.out.print(") ");
			return res;
		}
	}
}
