package Tree;

/*
 * Suppose an arithmetic expression is given as a binary tree. Each leaf is an integer and each internal node is one of '+', '−', '*', or '/'.
 * 
 * Given the root to such a tree, write a function to evaluate it.
 * 
 * For example, given the following tree:

    *
   / \
  +    +
 / \  / \
3  2  4  5

 * You should return 45, as it is (3 + 2) * (4 + 5).
 */

public class ArithmeticExpressionEvaluation {
	public static void main(String[] args) throws Exception {
		Node a = new Node(3, null, null);
		Node b = new Node(2, null, null);
		Node c = new Node(4, null, null);
		Node d = new Node(5, null, null);
		Node e = new Node("+", a, b);
		Node f = new Node("+", c, d);
		Node g = new Node("/", e, f);
		
		System.out.println("= " + g.arithEval());
	}
}
