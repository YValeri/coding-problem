package Tree;

/*
 * Given a node in a binary search tree, return the next bigger element, 
 * also known as the inorder successor.
 * 
 * For example, the inorder successor of 22 is 30.

   10
  /  \
 5    30
     /  \
   22    35

 * You can assume each node has a parent pointer.
 */

public class NodeSuccessor {
	public static void main(String[] args) {
		Node root = new Node(10);
		root.left = new Node(5);
		root.left.left = new Node(2);
		root.left.right = new Node(8);
		root.left.right.left = new Node(7);
		root.left.right.right = new Node(9);
		root.right = new Node(30);
		root.right.left = new Node(22);
		root.right.right = new Node(35);
		
		Node get = root.left;
		
		Node res = successor(root, root);
		if(res != null)
			System.out.println(res.iValue);
		else
			System.out.println("The node "+get.iValue+" has no successor.");
	}
	
	public static Node successor(Node root, Node n) {
		if(root.left == n || root.right == n || root == n) {
			Node cur = null;
			if(root.left == n) {
				cur = root.left;
				if(cur.right == null)
					return root;
			}else if(root.right == n){
				cur = root.right;
				if(cur.right == null)
					return null;
			}else {
				cur = root;
			}
			cur = cur.right;
			while(cur.left != null)
				cur = cur.left;
			return cur;
		}
		Node l = null, r = null;
		if(root.left != null)
			l = successor(root.left, n);
		if(root.right != null)
			r = successor(root.right, n);
		if(l != null)
			return l;
		else if(r != null)
			return r;
		return null;
	}
}
