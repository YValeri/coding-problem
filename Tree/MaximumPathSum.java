package Tree;

/*
 * Given a binary tree of integers, find the maximum path sum between two nodes. 
 * The path must go through at least one node, and does not need to go through the root.
 */

public class MaximumPathSum {

	static int val;
	static int maxPathSumUtil(Node node) {
		
		System.out.println("val1 = "+val);

		if (node == null)
			return 0;
		if (node.left == null && node.right == null)
			return node.iValue;
		
		System.out.println("val2 = "+val);
		int ls = maxPathSumUtil(node.left);
		int rs = maxPathSumUtil(node.right);
		
		System.out.println("val3 = "+val);
		if (node.left != null && node.right != null) {
			val = Math.max(val, ls + rs + node.iValue);
			return Math.max(ls, rs) + node.iValue;
		}
		return (node.left == null) ? rs + node.iValue : ls + node.iValue;
	}

	static void maxPathSum(Node node) {
		val = Integer.MIN_VALUE;
		maxPathSumUtil(node);
	}

	public static void main(String args[]) {
		Node root = new Node();
		root = new Node(-15);
		root.left = new Node(5);
		root.right = new Node(6);
		root.left.left = new Node(-8);
		root.left.right = new Node(1);
		root.left.left.left = new Node(2);
		root.left.left.right = new Node(6);
		root.right.left = new Node(3);
		root.right.right = new Node(9);
		root.right.right.right = new Node(0);
		root.right.right.right.left = new Node(4);
		root.right.right.right.right = new Node(-1);
		root.right.right.right.right.left = new Node(10);
		maxPathSum(root);
		System.out.println("Max pathSum of the given binary tree is " +val);
	}

}
