package Map;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * Given a mapping of digits to letters (as in a phone number), and a digit string, 
 * return all possible letters the number could represent. 
 * You can assume each valid number in the mapping is a single digit.
 * 
 * For example if {“2”: [“a”, “b”, “c”], 3: [“d”, “e”, “f”], …} 
 * then “23” should return [“ad”, “ae”, “af”, “bd”, “be”, “bf”, “cd”, “ce”, “cf"].
 */

public class DigitsToLetters {
	static HashMap<Character, ArrayList<Character>> map = new HashMap<>();
	
	public static void main(String[] args) {
		for(int i = 0; i < 9; ++i) {
			ArrayList<Character> l = new ArrayList<>();
			for(int j = 0; j < 3; ++j) {
				if((char) ('a' + j + i*3) <= 'z')
					l.add((char) ('a' + j + i*3));
			}
			int temp = (i+1)%10;
			char c = (char) ('0' + temp); 
			map.put(c, l); 
		}
		String s = "2379";
		ArrayList<String> res = new ArrayList<>();
		digitsMapping(s, res, 0, "");
		System.out.println(res);
	}
	
	public static void digitsMapping(String s, ArrayList<String> res, int start, String cur){
		if(start == s.length()) {
			res.add(cur);
			return;
		}
			
		for(char c : map.get(s.charAt(start))) {
			String temp = cur + String.valueOf(c);
			digitsMapping(s, res, start+1, temp);
		}
	}
}
