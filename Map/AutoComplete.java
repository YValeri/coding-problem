package Map;

import java.util.TreeSet;

/*
 * Implement an autocomplete system. 
 * That is, given a query string s and a set of all possible query strings, 
 * return all strings in the set that have s as a prefix.
 * 
 * For example, given the query string de and the set of strings [dog, deer, deal],
 * return [deer, deal].
 *  
 * Hint: Try preprocessing the dictionary into a more efficient
 * data structure to speed up queries.
 */

public class AutoComplete {
	static TreeSet<String> dictionnary = new TreeSet<String>();
	static TreeSet<String> currentDictionnary = new TreeSet<String>();

	public static void main(String[] args) {
		dictionnary.add("deer");
		dictionnary.add("dop");
		dictionnary.add("deal");
		System.out.println(dictionnary);
		String s = "de";
		
		long lStartTime = System.nanoTime();
		complete(s);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(currentDictionnary);
	}
	
	static void complete(String s) {
		for(String entry : (currentDictionnary.size() != 0)?currentDictionnary:dictionnary) {
			if(entry.startsWith(s))
				currentDictionnary.add(entry);
		}
	}
}
