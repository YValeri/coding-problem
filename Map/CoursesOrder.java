package Map;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * We're given a hashmap with a key courseId and value a list of courseIds, 
 * which represents that the prerequesite of courseId is courseIds. 
 * Return a sorted ordering of courses such that we can finish all courses.
 * 
 * Return null if there is no such ordering.
 * 
 * For example, given {'CSC300': ['CSC100', 'CSC200'], 'CSC200': ['CSC100'], 'CSC100': []}, 
 * should return ['CSC100', 'CSC200', 'CSCS300'].
 */

public class CoursesOrder {
	static HashMap<String, ArrayList<String>> map = new HashMap<>();

	public static void main(String[] args) throws InterruptedException {
		String s1 = "CSC100";
		String s2 = "CSC200";
		String s3 = "CSC300";
		String s4 = "CSC000";
		ArrayList<String> l1 = new ArrayList<>();
		l1.add(s4);
		ArrayList<String> l2 = new ArrayList<>();
		l2.add(s1);
		ArrayList<String> l3 = new ArrayList<>();
		l2.add(s1);
		l3.add(s2);
		ArrayList<String> l4 = new ArrayList<>();
		map.put(s1, l1);
		map.put(s2, l2);
		map.put(s3, l3);
		map.put(s4, l4);
		ArrayList<String> res = order();
		System.out.println(res);
	}
	
	static int order(String s, ArrayList<String> res, ArrayList<String> fail) {
		//System.out.println("s = "+s);
		//System.out.println("res = "+res);
		if(res.contains(s))
			return 1;
		if(map.get(s).size() == 0) {
			res.add(s);
			//System.out.println("res devient "+res);
		}else {
			if(fail.contains(s))
				return 0;
			ArrayList<String> l = map.get(s);
			//System.out.println("l = "+l);
			if(res.containsAll(l)) {
				if(!res.contains(s))
					res.add(s);
				return 1;
			}else {
				for(String ss : l) {
					//System.out.println("ss = "+ss);
					if(!res.contains(ss)) {
						fail.add(s);
						if(order(ss, res, fail) == 0)
							return 0;
						fail.remove(s);
					}
				}
				//System.out.println("sortie de la boucle");
				if(!res.contains(s))
					res.add(s);
				//System.out.println("res devient après la boucle "+res);
			}
		}
		return 1;	
	}

	static ArrayList<String> order() throws InterruptedException {
		ArrayList<String> res = new ArrayList<>();
		ArrayList<String> fail = new ArrayList<>();
		for(String s : map.keySet()) 
			if(order(s, res, fail) == 0)
				return null;
		
		return res;
	}
}
