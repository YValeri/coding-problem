/**
 * This package is about using and manipulating map, whether hash maps, dictionaries or others,
 * to achieve certain goals.
 */
/**
 * @author YValeri
 *
 */
package Map;