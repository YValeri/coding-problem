package Map;

/*
 * Write a map implementation with a get function that lets you retrieve the value of a key at a particular time.
 * 
 * It should contain the following methods:

    set(key, value, time): sets key to value for t = time.
    get(key, time): gets the key at t = time.

 * The map should work like this. If we set a key at a particular time, 
 * it will maintain that value forever or until it gets set at a later time. 
 * In other words, when we get a key at a time, it should return the value that was set for that key set at the most recent time.
 * 
 * Consider the following examples:

d.set(1, 1, 0) # set key 1 to value 1 at time 0
d.set(1, 2, 2) # set key 1 to value 2 at time 2
d.get(1, 1) # get key 1 at time 1 should be 1
d.get(1, 3) # get key 1 at time 3 should be 2

d.set(1, 1, 5) # set key 1 to value 1 at time 5
d.get(1, 0) # get key 1 at time 0 should be null
d.get(1, 10) # get key 1 at time 10 should be 1

d.set(1, 1, 0) # set key 1 to value 1 at time 0
d.set(1, 2, 0) # set key 1 to value 2 at time 0
d.get(1, 0) # get key 1 at time 0 should be 2

 */

import java.util.HashMap;

public class HashMapTimed {
	static HashMap<Pair<Integer, Integer>, Integer> map = new HashMap<>();
	public static void main(String[] args) {
		/*set(1, 1, 0);
		set(1, 2, 2);
		System.out.println(get(1, 1));
		System.out.println(get(1, 3));*/

		/*set(1, 1, 5);
		System.out.println(get(1, 0));
		System.out.println(get(1, 10));*/

		set(1, 1, 0);
		set(1, 2, 0);
		System.out.println(get(1, 0));

	}
	
	public static void set(int key, int value, int time) {
		map.put(new Pair<Integer, Integer>(key, time), value);
	}
	
	public static Integer get(int key, int time) {
		Pair<Integer, Integer> p = new Pair<Integer, Integer>(key, -1);
		for(Pair<Integer, Integer> temp : map.keySet()) {
			if(temp.first == key) {
				if(temp.second > p.second && temp.second <= time)
					p = temp;
			}
		}
		return map.get(p);
	}
}
