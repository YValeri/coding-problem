package Map;

/*
 * Implement a URL shortener with the following methods:
 * 
 * shorten(url), which shortens the url into a six-character alphanumeric string, such as zLg6wl.
 * restore(short), which expands the shortened string into the original url. If no such shortened string exists, return null.
 * 
 * Hint: What if we enter the same URL twice?
 */

import java.util.HashMap;

public class URLShortener {
	static HashMap<String, String> map = new HashMap<>();
	static char[] tab = new char[26 + 26 + 10];
	public static void main(String[] args) {
		for(int i = 0; i < 26; ++i) {
			tab[i] = (char) ('a' + i);
			tab[i+26] = (char) ('A' + i);
		}
		for(int i = 0; i < 10; ++i)
			tab[i+52] = (char) ('0' + i);
		
		String res = shorten("https://codereview.stackexchange.com/questions/111409/hash-function-for-strings");
		System.out.println(restore(res));
		res = shorten("https://stackoverflow.com/questions/34595/what-is-a-good-hash-function");
		System.out.println(restore(res));
		res = shorten("https://www-5.netcourrier.com/netc/mail/msg.php?s=iUo4AsG0k7dtLKKrZgQy0kej&uid=14721.newsletter&mailbox=newsletter&ea_encode=0");
		System.out.println(restore(res));
		res = shorten("https://www.youtube.com");
		System.out.println(restore(res));
		
	}
	
	static String shorten(String url) {
		String hash = hash(url);
		map.put(hash, url);
		return hash;
	}
	
	static String restore(String shortURL) {
		return map.get(shortURL);
	}
	
	static String hash(String url) {
		int hash = 7;
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < url.length(); i++) {
		    hash = hash*31 + url.charAt(i);
		    res.replace(i%6, i%6+1, String.valueOf(tab[Math.abs(hash%62)]));
		}
		System.out.println(res);
		return res.toString();
	}
}
