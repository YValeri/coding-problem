package Stack_Queue;

/*
 * Implement 3 stacks using a single list:

class Stack:
    def __init__(self):
        self.list = []

    def pop(self, stack_number):
        pass

    def push(self, item, stack_number):
        pass

 */

public class Stack3List1 {
	public static void main(String[] args) {
		ListNode l = new ListNode();
		push(1, 1, l);
		push(2, 2, l);
		push(3, 3, l);
		push(1, 4, l);
		push(2, 5, l);
		push(3, 6, l);

		print(l);
		pop(2, l);
		print(l);
		pop(2, l);
		print(l);
		pop(3, l);
		print(l);
		pop(3, l);
		print(l);
		pop(3, l);

	}

	/*
	 * Intermediate function to push onto the list l
	 */
	public static void push(int stack_number, int value, ListNode l) {
		System.out.println("We push " + value + " on the number " + stack_number + " stack.");
		l.push(stack_number, value);
	}

	/*
	 * Intermediate function to pop the first element of the list l with stack
	 * number stack_number
	 */
	public static void pop(int stack_number, ListNode l) {
		System.out.println("We pop the number " + stack_number + " stack.");
		Node n = l.pop(stack_number);
		if (n == null)
			System.out.println("There were no value in that specific stack.");
		else
			System.out.println("The poped value is " + n.value);
	}

	// Function to print the list l
	public static void print(ListNode l) {
		Node n = l.head;
		System.out.println("This is the list :");
		while (n != null) {
			System.out.println("Stack number = " + n.number + ", value = " + n.value);
			n = n.next;
		}
		System.out.println();
	}
}
