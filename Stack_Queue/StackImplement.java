package Stack_Queue;

/*
 * Implement a stack that has the following methods:
 * 
 * push(val), which pushes an element onto the stack
 * pop(), which pops off and returns the topmost element of the stack. If there are no elements in the stack, then it should throw an error or return null.
 * max(), which returns the maximum value in the stack currently. If there are no elements in the stack, then it should throw an error or return null.
 * 
 * Each method should run in constant time.
 */

public class StackImplement {
	public static void main(String[] args) {
		Stack<Integer> list = new Stack<>();
		long lStartTime;
        long lEndTime;
        long output;
        Integer res;
        
        lStartTime = System.nanoTime();
		list.push(7);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après push");
		list.print();
		
		lStartTime = System.nanoTime();
		list.push(5);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après push");
		list.print();
		
		lStartTime = System.nanoTime();
		list.push(-3);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après push");
		list.print();
		
		lStartTime = System.nanoTime();
		list.push(12);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après push");
		list.print();
        
        lStartTime = System.nanoTime();
		list.push(11);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après push");
		list.print();
		
		lStartTime = System.nanoTime();
		res = list.max();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après max = "+res);
		list.print();
		
		lStartTime = System.nanoTime();
		res = list.pop();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après pop = "+res);
		list.print();
		
		lStartTime = System.nanoTime();
		res = list.pop();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après pop = "+res);
		list.print();
		
		lStartTime = System.nanoTime();
		res = list.pop();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après pop = "+res);
		list.print();
		
		lStartTime = System.nanoTime();
		res = list.max();
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println("après max = "+res);
		list.print();
	}
}
