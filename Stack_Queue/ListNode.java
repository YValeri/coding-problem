package Stack_Queue;

/*
 * See the "Stack3List1" problem
 */

public class ListNode {
	Node head;

	public ListNode() {
		this.head = null;
	}

	/*
	 * Push replaces the head of the list, and the next of the new head is the old
	 * one
	 */
	public void push(int number, int item) {
		head = new Node(number, item, head);

	}

	/*
	 * We just go through the list, while keeping the current and previous node. If
	 * we see the right stack number, we take out the corresponding Node, and link
	 * prev and cur.next, effectively removing cur from the list
	 */
	public Node pop(int number) {
		Node prev = null;
		Node cur = head;

		while (cur != null && cur.number != number) {
			prev = cur;
			cur = cur.next;
		}

		if (cur == null)
			return null;
		if (prev == null)
			head = cur.next;
		else
			prev.next = cur.next;
		return cur;
	}
}
