package Stack_Queue;

/*
 * See the "Stack3List1" problem
 */

public class Node {
	int number, value;
	Node next;

	public Node(int n, int v) {
		this.number = n;
		this.value = v;
		this.next = null;
	}

	public Node(int n, int v, Node next) {
		this.number = n;
		this.value = v;
		this.next = next;
	}
}
