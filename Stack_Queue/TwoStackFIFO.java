package Stack_Queue;

import java.util.Stack;

/*
 * See "TwoStacksQueue" problem.
 */

public class TwoStackFIFO<E> {
	Stack<E> FS;
	Stack<E> SS;
	
	public void enqueue(E elem) {
		if(FS == null) {
			FS = new Stack<E>();
			SS = new Stack<E>();
		}
		FS.push(elem);
	}
	
	public E dequeue() {
		E res = null;
		while(!FS.isEmpty()) {
			E elem = FS.pop();
			SS.push(elem);
		}
		res = SS.pop();
		while(!SS.isEmpty()) {
			E elem = SS.pop();
			FS.push(elem);
		}
		return res;
	}
	
	public void print() {
		System.out.println(FS);
	}
}
