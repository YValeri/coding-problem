package Stack_Queue;

/*
 * Implement a queue using two stacks. 
 * Recall that a queue is a FIFO (first-in, first-out) data structure with the following methods: 
 * enqueue, which inserts an element into the queue
 * dequeue, which removes it.
 */

public class TwoStacksQueue {
	public static void main(String[] args) {
		TwoStackFIFO<Integer> queue = new TwoStackFIFO<>();
		
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		queue.enqueue(6);
		queue.enqueue(7);
		queue.enqueue(8);
		queue.print();
		
		System.out.println(queue.dequeue());
		queue.print();
		
		System.out.println(queue.dequeue());
		queue.print();
		

		queue.enqueue(55);
		queue.enqueue(98);
		queue.print();
		
		System.out.println(queue.dequeue());
		queue.print();
		
		System.out.println(queue.dequeue());
		queue.print();
		
		System.out.println(queue.dequeue());
		queue.print();
		
		System.out.println(queue.dequeue());
		queue.print();
	}
}
