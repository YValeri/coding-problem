package Stack_Queue;

import java.util.LinkedList;

/*
 * See problem "StackImplement"
 */

public class Stack<T extends Comparable<T>> {
	LinkedList<T> list;
	LinkedList<T> max;
	
	public Stack() {
		list = new LinkedList<T>();
		max = new LinkedList<T>();
	}
	
	private boolean empty() {
		return list.isEmpty();
	}

	public void push(T val) {
		list.add(val);
		if(max.isEmpty())
			max.add(val);
		else {
			T currentMax;
			if(max.peekLast().compareTo(val) < 0) 
				currentMax = val;
			else
				currentMax = max.peekLast();
			max.add(currentMax);
		}
	}
	
	public T max() {
		if(empty())
			return null;
		return max.peekLast();
	}
	
	public void print() {
		System.out.println("List = ");
		System.out.println(list);
		System.out.println("Max = ");
		System.out.println(max);
	}
	
	public T pop() {
		if(empty())
			return null;
		max.removeLast();
		return list.removeLast();
	}
}
