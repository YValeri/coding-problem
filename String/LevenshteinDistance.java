package String;

/*
 * The edit distance between two strings refers to the minimum number of character insertions, deletions, 
 * and substitutions required to change one string to the other. 
 * For example, the edit distance between “kitten” and “sitting” is three: substitute the “k” for “s”, substitute the “e” for “i”, and append a “g”.
 * 
 * Given two strings, compute the edit distance between them.
 */

public class LevenshteinDistance {
	public static void main(String[] args) {
		String a = "kitten";
		String b = "sitting";
		
		long lStartTime = System.nanoTime();
		int res = levenshtein(a, b);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
	}
	
	static int levenshtein(String x, String y) {
	    int[][] dp = new int[x.length() + 1][y.length() + 1];
	 
	    for (int i = 0; i <= x.length(); i++) 
	        for (int j = 0; j <= y.length(); j++) 
	            if (i == 0) 
	                dp[i][j] = j;
	            else if (j == 0)
	                dp[i][j] = i;
	            else 
	                dp[i][j] = Math.min(dp[i - 1][j - 1] + ((x.charAt(i - 1) == y.charAt(j - 1))?0:1), Math.min(dp[i - 1][j] + 1, dp[i][j - 1] + 1));
	 
	    return dp[x.length()][y.length()];
	}
}
