package String;

import java.util.regex.Pattern;

/*
 * You're given a string consisting solely of (, ), and *. * can represent either a (, ), or an empty string. 
 * Determine whether the parentheses are balanced.

 * For example, (()* and (*) are balanced. )*( is not balanced.
 */

public class BalancedParentheses2 {
	public static void main(String[] args) {
		String s = "*)*(";
		System.out.println(balanced(s));
		s = "(()*";
		System.out.println(balanced(s));
		s = "(*)";
		System.out.println(balanced(s));
		s = ")*(";
		System.out.println(balanced(s));
		s = "()*";
		System.out.println(balanced(s));
	}

	public static boolean balanced(String s) {
		// Since "()" is a right sub-string no matter what, we just replace them with
		// empty strings
		while (s.contains("()"))
			s = s.replace("()", "");

		// Now we have a string without "()", so we just check if s is composed of (*),
		// (*, *) or *
		return Pattern.matches("(([(][*][)])|([(][*])|([*][)])|([*]))*", s);
	}
}
