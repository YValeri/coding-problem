package String;

/*
 * Given a string which we can delete at most k, return whether you can make a palindrome.
 * 
 * For example, given 'waterrfetawx' and a k of 2, you could delete f and x to get 'waterretaw'.
 */

public class DeleteIntoPalindrome {
	public static void main(String[] args) {
		StringBuilder s = new StringBuilder("waterrfetawx");
		int k = 2;
		
		System.out.println(delete(s, k));
	}

	public static boolean delete(StringBuilder s, int k) {
		if(isPalindrome(s)) {
			System.out.println("\""+s+"\" is a palindrome.");
			return true;
		}else if(k == 0) 
			return false;
		for(int i = 0; i < s.length(); ++i) {
			StringBuilder temp = new StringBuilder(s);
			temp.deleteCharAt(i);
			if(delete(s, k-1))
				return true;
		}
		return false;
	}

	public static boolean isPalindrome(StringBuilder s) {
		int n = s.length();
		for (int i = 0; i < (n / 2); ++i)
			if (s.charAt(i) != s.charAt(n - i - 1))
				return false;

		return true;
	}
}
