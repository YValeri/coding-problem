package String;

/*
 * Given a string s and an integer k, 
 * break up the string into multiple texts such that each text has a length of k or less. 
 * You must break it up so that words don't break across lines. If there's no way to break the text up, then return null.
 * 
 * You can assume that there are no spaces at the ends of the string and that there is exactly one space between each word.
 * 
 * For example, given the string 
 * "the quick brown fox jumps over the lazy dog" and k = 10, 
 * you should return: ["the quick", "brown fox", "jumps over", "the lazy", "dog"]. 
 * No string in the list has a length of more than 10.
 */

public class StringExplosion {
	public static void main(String[] args) {
		String s = "the quick brown fox jumps over the lazy dog";
		
		String[] res = explode(s, 10);
		for(int i = 0; i < res.length; ++i)
			System.out.println(res[i]);
	}
	
	static String[] explode(String s, int k) {
		String[] res = new String[s.length()/k + 1];
		int cpt = 0;
		while(s.length() > k) {
			int ret = k;
			if(s.charAt(k) != ' ') {
				while(s.charAt(ret) != ' ')
					--ret;
			}
			res[cpt++] = s.substring(0, ret);
			s = s.substring(ret+1);
		}
		if(s.length() != 0)
			res[cpt] = s;
		return res;
	}
}
