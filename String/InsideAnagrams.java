package String;

import java.util.ArrayList;

/*
 * Given a word W and a string S, find all starting indices in S which are anagrams of W.
 * 
 * For example, given that W is "ab", and S is "abxaba", return 0, 3, and 4.
 */

public class InsideAnagrams {
	final static int CHARACTERS = 26;

	public static void main(String[] args) {
		String W = "yb";
		String S = "abxabyuhdfgbyidqufvbiyusgrfuvbqosydbcuboysdvcmplokijuhygtvbnwaysvctynnzuienppmwpla";

		System.out.println(anagrams(W, S));
	}

	public static ArrayList<Integer> anagrams(String W, String S) {
		ArrayList<Integer> res = new ArrayList<>();
		for (int i = 0; i < S.length() - W.length() + 1; ++i) {
			if(W.contains(String.valueOf(S.charAt(i))))
				if (isAnagram(W, S.substring(i, i + W.length())))
					res.add(i);
		}
		return res;
	}

	public static boolean isAnagram(String a, String b) {
		System.out.println("Comparing \"" + a + "\" and \"" + b + "\".");
		char[] count = new char[CHARACTERS];
		for (int i = 0; i < a.length(); ++i) {
			count[a.charAt(i) - 'a']++;
			count[b.charAt(i) - 'a']--;
		}
		for (int i = 0; i < CHARACTERS; ++i)
			if (count[i] != 0)
				return false;
		return true;
	}
}
