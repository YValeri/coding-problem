package String;

/*
 * Given a string, find the palindrome that can be made by inserting the fewest number of characters as possible anywhere in the word.
 *  If there is more than one palindrome of minimum length that can be made, return the lexicographically earliest one (the first one alphabetically).
 *  
 *  For example, given the string "race", you should return "ecarace", 
 *  since we can add three letters to it (which is the smallest amount to make a palindrome). 
 *  There are seven other palindromes that can be made from "race" by adding three letters, but "ecarace" comes first alphabetically.
 *  
 *  As another example, given the string "google", you should return "elgoogle".
 */

public class PalindromeCreation {
	public static void main(String[] args) {
		String s = "google";
		
		long lStartTime = System.nanoTime();
		String res = palindrome(s);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
	}
	
	public static String palindrome(String s) {
		int i;
		for(i = 0; s.charAt(i) == s.charAt(s.length()-1-i); ++i);
		String t = s.substring(i, s.length()-i);
		t = new StringBuilder(t).reverse().toString();
		System.out.println(t);
		String temp = t.toString();
		System.out.println("temp = "+temp);
		System.out.println("s = "+s);
		int j = t.length()-1;
		for(int l = j; j >= 0 && t.charAt(l) == s.charAt(t.length()-1-j+i); --l) {
			System.out.println(temp);
			temp = new StringBuilder(temp).deleteCharAt(l).toString();
			System.out.println(temp);
		}
		String res = s.substring(0, i) + temp + s.substring(i);
		return res;
	}
}
