package String;

/*
 * Given a string, find the longest palindromic contiguous substring. 
 * If there are more than one with the maximum length, return any one.
 * 
 * For example, the longest palindromic substring of "aabcdcb" is "bcdcb". 
 * The longest palindromic substring of "bananas" is "anana".
 */

public class LongestContiguousPalindrom {
	public static void main(String[] args) {
		String s = "bananas";
		long lStartTime;
        long lEndTime;
        long output;
        String res;
		
		lStartTime = System.nanoTime();
		res = LPCS(s);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
	}
	
	public static String LPCS(String s) {
		String a = "", b = "";
		int start = 0, end = 0;
		for(int i = 0; i < s.length(); ++i) {
			a = "";
			b = "";
			for(int j = i; j < s.length(); ++j) {
				a = a + s.charAt(j);
				b = s.charAt(j) + b;
				if(a.equals(b) && a.length() > end) {
					start = i;
					end = a.length();
				}
			}
		}
		return s.substring(start, start+end);
	}
}
