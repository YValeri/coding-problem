package String;

/*
 * Run-length encoding is a fast and simple method of encoding strings. 
 * The basic idea is to represent repeated successive characters as a single count and character. 
 * For example, the string "AAAABBBCCDAA" would be encoded as "4A3B2C1D2A".
 * 
 * Implement run-length encoding and decoding. 
 * You can assume the string to be encoded have no digits and consists solely of alphabetic characters. 
 * You can assume the string to be decoded is valid.
 */

public class RunLengthEncoding {
	public static void main(String[] args) {
		String s = "AAAABBBCCDAA";
		System.out.println(s);
		
		long lStartTime = System.nanoTime();
		s = encode(s);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(s);
        
        lStartTime = System.nanoTime();
		s = decode(s);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(s);
	}
	
	public static String encode(String s) {
		String res = "";
		for (int i = 0; i < s.length();) {
			char c = s.charAt(i);
			int count = 1;
			for (int j = i+1; j < s.length() && s.charAt(j) == c; j++)
				count++;
			i += count;
			res += count + "" + c;
		}
		return res;
	}
	
	public static String decode(String s) {
		String res = "";
		for (int i = 0; i < s.length(); i = i + 2) {
			int count = Integer.parseInt(s.substring(i, i+1));
			String c = String.valueOf(s.charAt(i+1));
			for(int j = 0; j < count; j++)
				res += c;
		}
		return res;
	}
}
