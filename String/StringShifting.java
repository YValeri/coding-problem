package String;

/*
 * Given two strings A and B, return whether or not A can be shifted some number of times to get B.
 * 
 * For example, if A is "abcde" and B is "cdeab", return true. If A is "abc" and B is "acb", return false.
 */

public class StringShifting {
	public static void main(String[] args) {
		String A = "abcde";
		String B = "cdeab";
		
		boolean res;
		long lStartTime = System.nanoTime();
		res = shift(A, B);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
		
		A = "abc";
		B = "acb";
		
		lStartTime = System.nanoTime();
		res = shift(A, B);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(res);
	}
	
	public static boolean shift(String a, String b) {
		if(a.length() != b.length())
			return false;
		String str = a;
		String r = "";
		int i = 0;
		while(i != a.length()) {
			if(b.equals(str.substring(i)+r))
				return true;
			r += str.charAt(i);
			i++;
		}
		return false;
	}
}
