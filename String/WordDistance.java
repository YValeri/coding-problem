package String;

import java.util.ArrayList;

/*
 * Find an efficient algorithm to find the smallest distance (measured in number of words) 
 * between any two given words in a string.
 * 
 * For example, given words "hello", and "world" and a text content of "dog cat hello cat dog dog hello cat world", 
 * return 1 because there's only one word "cat" in between the two words.
 */

public class WordDistance {
	public static void main(String[] args) {
		String s = "dog cat hello cat dog hello cat world";
		System.out.println(distance(s, "hello", "world"));
	}
	
	public static int distance(String s, String w1, String w2) {
		//Create a stringBuilder with the string s, necessary for some methods
		StringBuilder st = new StringBuilder(s);
		
		/*
		 * Create two lists which will contain the locations of the indexes of w1 and w2,
		 * each containing the first letter of w1 and w2 and the first letter of the next word
		 */
		ArrayList<Integer> w1s = new ArrayList<>();
		ArrayList<Integer> w2s = new ArrayList<>();
		
		//Traverse the string s, with i and j used to search the words
		for(int i = 0, j = 0; i < st.length() || j < st.length(); ++i, ++j) {
			//Get the indexes of the first letter of the two words, starting from index i and j
			int temp1 = st.indexOf(w1, i);
			int temp2 = st.indexOf(w2, j);
			
			//If the word is contained in the string
			if(temp1 != -1){
				//add the index of the word in the list, and the index of the first letter of the the following word
				w1s.add(temp1);
				w1s.add(temp1 + w1.length() + 1);
				//modify i, since we found w1, we can just skip over the start of the next word
				i += temp1;
			}
			//We do the exact same for w2 and j
			if(temp2 != -1) {
				w2s.add(temp2);
				w2s.add(temp2 + w2.length() + 1);
				j += temp2;
			}
			//If there is no more w1 or w2 in the string starting from i and j, finish the loop
			if(temp1 == -1 && temp2 == -1)
				break;
		}
		//If there is no w1 or w2, just return
		if(w1s.isEmpty() || w2s.isEmpty())
			return -1;
		
		//We use a min variable to keep tracks of the number of words
		int min = Integer.MAX_VALUE;
		
		//We traverse the two lists
		for(Integer i : w1s) {
			for(Integer j : w2s) {
				if(j >= s.length())
					continue;
				
				//If the indexes are the same, we know the string as the substring "w1 w2" in it, so return 0
				if(i == j)
					return 0;
				
				//Else, we get the substring from the two indexes
				String temp = s.substring(Math.min(i, j), Math.max(i, j)-1);
				//Replace all spaces with nothing, in order to compute the number of " " in the substring
				int count = temp.length() - temp.replace(" ", "").length() + 1;
				//Update the min if possible
				if(min > count)
					min = count;
			}
		}
		return min;
	}
}
