package String;

/*
 * Implement regular expression matching with the following special characters:

    . (period) which matches any single character
    * (asterisk) which matches zero or more of the preceding element

That is, implement a function that takes in a string and a valid regular expression and returns whether or not the string matches the regular expression.

For example, given the regular expression "ra." and the string "ray", your function should return true. The same regular expression on the string "raymond" should return false.

Given the regular expression ".*at" and the string "chat", your function should return true. The same regular expression on the string "chats" should return false.
 */

public class RegExpDotsAsterisks {
	public static void main(String[] args) {
		String s = "ray";
		String reg = "ra.";
		
		long lStartTime = System.nanoTime();
		boolean match = matches(s, reg, 0, 0);
        long lEndTime = System.nanoTime();
        long output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(match);
        
        s = "rays";
		reg = "r.*";
		
		lStartTime = System.nanoTime();
		match = matches(s, reg, 0, 0);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(match);
        
        s = "chat";
		reg = ".*at";
		
		lStartTime = System.nanoTime();
		match = matches(s, reg, 0, 0);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(match);
        
        s = "chats";
		reg = "c.*ats";
		
		lStartTime = System.nanoTime();
		match = matches(s, reg, 0, 0);
        lEndTime = System.nanoTime();
        output = lEndTime - lStartTime;
        System.out.println("Elapsed time in milliseconds: " + output);
        System.out.println(match);
	}
	
	static boolean matches(String s, String reg, int i, int j) {
		if(s.length() < reg.length())
			return false;
		
		if(s.length() <= i || reg.length() <= j)
			return true;
		
		if(j+1 < reg.length())
			if(reg.charAt(j) == '.' && reg.charAt(j+1) == '*')
				return matches(s, reg, j+2, j+2);
			
		if(reg.charAt(j) == '.' || s.charAt(i) == reg.charAt(j))
			return matches(s, reg, i+1, j+1);
			
		return false;
	}
}
