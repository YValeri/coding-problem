package String;

/*
 * Given a string of parentheses, write a function to compute the minimum 
 * number of parentheses to be removed to make the string valid 
 * (i.e. each open parenthesis is eventually closed).
 * 
 * For example, given the string "()())()", you should return 1. 
 * Given the string ")(", you should return 2, since we must remove all of them.
 */

public class BalancedParentheses {
	public static void main(String[] args) {
		String s = ")()()())()((())()()(()())())()(()()()()()())()()()()()())())(())";
		System.out.println(remove(s));
		
		System.out.println(s);
	}
	
	public static int remove(String s) {
		while(s.contains("()"))
				s = s.replace("()", "");
		System.out.println(s);
		return s.length();
	}
}
