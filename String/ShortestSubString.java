package String;

import java.util.HashSet;

/*
 * Given a string and a set of characters, return the shortest substring containing all the characters in the set.
 * 
 * For example, given the string "figehaeci" and the set of characters {a, e, i}, you should return "aeci".
 * 
 * If there is no substring containing all the characters in the set, return null.
 */

public class ShortestSubString {
	public static void main(String[] args) {
		String s = "figehoooooooooooooooooooiazefzaedqscrgrthbieufbivsnoeqp,iuvybienrfzoubyvenzo,ifbuhefgvyihbudfniqusdygvbqci";
		HashSet<Character> map = new HashSet<>();
		map.add('a');
		map.add('e');
		map.add('i');
		
		System.out.println(substring(s, map));
		System.out.println(s);
	}
	
	public static String substring(String s, HashSet<Character> map) {
		int start = 0, length = Integer.MAX_VALUE;
		for(int i = 0; i < s.length(); ++i) {
			if(map.contains(s.charAt(i))) {
				HashSet<Character> map2 = new HashSet<>();
				map2.addAll(map);
				map2.remove(s.charAt(i));
				int j = i+1;
				for(; j < s.length() && !map2.isEmpty(); ++j) 
					if(map2.contains(s.charAt(j)))
						map2.remove(s.charAt(j));
				
				if(map2.isEmpty() && j-i < length) {
					start = i;
					length = j-i;
				}
			}
		}
		System.out.println(s);
		return s.substring(start, start+length);
	}
}
