package String;

/*
 * Given a string, determine whether any permutation of it is a palindrome.
 * 
 * For example, "carrace" should return true, since it can be rearranged to form "racecar", 
 * which is a palindrome. "daily" should return false, since there's no rearrangement that can form a palindrome.
 */

public class PossiblePalindrome {
	public static void main(String[] args) {
		String s = "carracee";
		System.out.println(determine(s));
	}
	
	/*
	 * Simply put, a palindrome is a sucession of symbols xi with 0 <= i <= t and t in N, 
	 * and with the condition that xi = x(t-i).
	 * Therefore, if we want to determine if any permutation of a string is a palindrome, that means we
	 * get rid of of the condition. Henceforth, we now only have to check if for each symbol in the string, 
	 * there is another of the same symbol.
	 */
	public static boolean determine(String s) {
		//We create a boolean array for each letter of the alphabet
		boolean[] arr = new boolean[26];
		
		//Traverse the string : for each letter, swap the boolean value in the array
		for(int i = 0; i < s.length(); ++i)
			arr[s.charAt(i) - 'a'] = !arr[s.charAt(i) - 'a'];
		
		//If the string is of odd length, there will be a letter alone, so we need another boolean
		boolean odd = false;
		
		/*
		 * Traverse the array. For each cell, if it is true, that means the corresponding letter at
		 * position i in the alphabet appears 2*n+1 times, with n in N. If the string is of length even, that is not possible
		 * so we return false. If its length is odd, we check if the odd boolean is activated. If it is, that means the only 
		 * chance for a character appearing 2*n+1 times was used, so we can't have another letter appearing an odd number of times,
		 * thereforce, we return false. If that chance haven't been used yet, we use it, and continue checking.
		 */
		for(int i = 0; i < arr.length; ++i)
			if(arr[i])
				if((s.length()%2 == 1 && odd) || s.length()%2 == 0)
					return false;
				else 
					odd = true;
		
		//No two letters are appearing an odd number of times, so there is a permutation that is a palindrome, so return true
		return true;
	}
}
