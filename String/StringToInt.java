package String;

import java.util.regex.*;

/*
 * Given a string, return whether it represents a number. Here are the different kinds of numbers:

    "10", a positive integer
    "-10", a negative integer
    "10.1", a positive real number
    "-10.1", a negative real number
    "1e5", a number in scientific notation

 * And here are examples of non-numbers:

    "a"
    "x 1"
    "a -2"
    "-"

 */

public class StringToInt {
	public static void main(String[] args) {
		String s = "-18.7e7";
		System.out.println(isInt(s));
	}

	public static boolean isInt(String s) {
		return Pattern.matches("-?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?", s);
	}
}
