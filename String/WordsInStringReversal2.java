package String;

import java.util.HashSet;

/*
 * Given a string and a set of delimiters, reverse the words in the string while maintaining the relative order of the delimiters. 
 * For example, given "hello/world:here", return "here/world:hello"
 * 
 * Follow-up: Does your solution work for the following cases: "hello/world:here/", "hello//world:here"
 */

public class WordsInStringReversal2 {
	public static void main(String[] args) {
		HashSet<Character> h = new HashSet<>();
		h.add('/');
		h.add(':');
		h.add(';');
		h.add('\\');
		h.add('.');
		h.add(',');
		StringBuilder s = new StringBuilder("/:/hello/world:here//");
		System.out.println(reversal(s, h));
	}

	public static StringBuilder reversal(StringBuilder s, HashSet<Character> h) {
		StringBuilder first = new StringBuilder("");
		StringBuilder second = new StringBuilder("");
		System.out.println(s);
		while(h.contains(s.charAt(0))) {
			first.append(s.charAt(0));
			s.delete(0, 1);
		}
		while(h.contains(s.charAt(s.length()-1))) {
			second.append(s.charAt(s.length()-1));
			s.delete(s.length()-1, s.length());
		}
		int i = 0;
		while (i < s.length()) {
			int temp = minFrom(s, h, i);
			while(temp == i+1)
				temp = minFrom(s, h, ++i);
			wordReverse(s, h, i, (temp == -1) ? s.length() - 1 : temp - 1);
			i = ((temp == -1) ? s.length() : temp) + 1;
		}
		return first.append(wordSwap(s, h).append(second));
	}
	
	public static int minFrom(StringBuilder s, HashSet<Character> h, int start) {
		for(int i = start; i < s.length(); ++i)
			if(h.contains(s.charAt(i)))
				return i;
		return -1;
	}
	
	public static StringBuilder wordSwap(StringBuilder s, HashSet<Character> h) {
		StringBuilder res = new StringBuilder("");
		int start = 0;
		for(int i = s.length()-1; i >= 0; --i) {
			if(!h.contains(s.charAt(i)))
				res.append(s.charAt(i));
			else {
				int temp = minFrom(s, h, start);
				if(temp == -1)
					continue;
				while(h.contains(s.charAt(temp))) {
					res.append(s.charAt(temp++));
					if(temp >= s.length())
						return res;
				}
				start += temp;
			}
		}
		return res;
	}

	public static void wordReverse(StringBuilder s, HashSet<Character> h, int start, int end) {
		for (int i = start; i < end; ++i, --end) {
			char c = s.charAt(i);
			s.setCharAt(i, s.charAt(end));
			s.setCharAt(end, c);
		}
	}
}
