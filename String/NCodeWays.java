package String;

/*
 * Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, 
 * count the number of ways it can be decoded.
 * 
 * For example, the message '111' would give 3, 
 * since it could be decoded as 'aaa', 'ka', and 'ak'.
 * 
 * You can assume that the messages are decodable.
 * For example, '001' is not allowed.
 */

public class NCodeWays {
	public static void main(String[] args){
		String s = "1112";
		int n = 0;
		if(s != null && s.length() != 0 && inRange(s))
			n = decodeWays(s);
		System.out.println(n);
	}
	
	static boolean inRange(String s) {
		for(int i = 0; i < s.length(); i++)
			if(!Character.isDigit(s.charAt(i)))
				return false;
		if(s.contains("00") || s.equals("0") || s.charAt(0) == '0')
			return false;
		return true;
	}
	
	 static int decodeWays(String s) {
	        int n = s.length();
	        int[] dp = new int[n+1];
	        dp[0] = 1;
	        dp[1] = s.charAt(0) != '0' ? 1 : 0;
	        for(int i = 2; i <= n; i++) {
	            int first = Integer.valueOf(s.substring(i-1, i));
	            int second = Integer.valueOf(s.substring(i-2, i));
	            if(first >= 1 && first <= 9) {
	               dp[i] += dp[i-1];  
	            }
	            if(second >= 10 && second <= 26) {
	                dp[i] += dp[i-2];
	            }
	        }
	        return dp[n];
	    }
}
