package String;

/*
 * Given a string of words delimited by spaces, reverse the words in string. 
 * For example, given "hello world here", return "here world hello".
 * "a b c d e f g" becomes "g f e d c b a"
 * 
 * Follow-up: given a mutable string representation, can you perform this operation in-place?
 */

public class WordsInStringReversal {
	public static void main(String[] args) {
		StringBuilder s = new StringBuilder("hello world here");
		System.out.println(reversal(s));
	}

	public static StringBuilder reversal(StringBuilder s) {
		int i = 0;
		while (i < s.length()) {
			while(s.charAt(i) == ' ')
				++i;
			int temp = s.indexOf(" ", i);
			wordReverse(s, i, (temp == -1) ? s.length() - 1 : temp - 1);
			i = ((temp == -1) ? s.length() : temp) + 1;
		}
		wordReverse(s, 0, s.length()-1);
		return s;
	}

	public static void wordReverse(StringBuilder s, int start, int end) {
		int mid = (end - start) / 2;
		for (int i = start; i <= start + mid; ++i, end--) {
			char c = s.charAt(i);
			s.setCharAt(i, s.charAt(end));
			s.setCharAt(end, c);
		}
	}
}
